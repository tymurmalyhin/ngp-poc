# Databricks notebook source
# Setup connection to ADLS
storage_account_name = "sanpnextgenadls2"
storage_account_access_key = "jyiXgReQb9CFSZY8HEKNErK1HIPOtZMn6iTzv2NrplzIU9IleAL6wcj/5EliKBlmciG6vSJxxAoRvMW0Vt3kDg=="

# COMMAND ----------

# Define file locations
File_RAW = "abfss://poc-raw-tier@sanpnextgenadls2.dfs.core.windows.net/RAW/qa_Pos_RAW"
File_RAW_checkpoint = "abfss://poc-raw-tier@sanpnextgenadls2.dfs.core.windows.net/RAW/qa_Pos_RAW_cp"

# COMMAND ----------

# Setup spark
spark.conf.set("fs.azure.account.key."+storage_account_name+".dfs.core.windows.net",storage_account_access_key)

# COMMAND ----------

# Create view
file_type = "Delta"

df1 = spark.readStream.format(file_type).load(File_RAW)
df2=df1.selectExpr("Key","CAST(value AS string)", "offset", "timestamp")
df2.createOrReplaceTempView("qa_Pos_VIEW")

# COMMAND ----------

# MAGIC %sql
# MAGIC select * from qa_Pos_VIEW
