# Databricks notebook source
#Metadata starts

# COMMAND ----------

storage_account_name = "sanpnextgenadls2"
storage_account_access_key = "jyiXgReQb9CFSZY8HEKNErK1HIPOtZMn6iTzv2NrplzIU9IleAL6wcj/5EliKBlmciG6vSJxxAoRvMW0Vt3kDg=="

# COMMAND ----------

# Define file locations
POC_File_RAW = "abfss://poc-raw-tier@sanpnextgenadls2.dfs.core.windows.net/RAW/qa_Promotions_RAW"
POC_File_RAW_checkpoint = "abfss://poc-raw-tier@sanpnextgenadls2.dfs.core.windows.net/RAW/qa_Promotions_RAW_cp"

# COMMAND ----------

POC_Topic = "promotions"
POC_Confluent_Hosts='104.209.139.140:9092'
POC_eventhubs_connection_string=''
GROUP_ID = 'promotions'

# COMMAND ----------

POC_query = "Promotions_ingestion_query"
micro_batch_fq = "30 seconds"

# COMMAND ----------

#Metadata ends

# COMMAND ----------

# Setup spark
spark.conf.set("fs.azure.account.key."+storage_account_name+".dfs.core.windows.net",storage_account_access_key)

# COMMAND ----------

# Setup connection to Kafka
POC_kafka_DF = spark.readStream.format("kafka").option("kafka.bootstrap.servers", POC_Confluent_Hosts).option("kafka.session.timeout.ms", "30000").option("kafka.group.id", GROUP_ID).option("failOnDataLoss", "false").option("subscribe", POC_Topic ).option("startingOffsets", "earliest").option("fetch.max.bytes","5242880").load()

# COMMAND ----------

# Stream to the Data Lake 
run = POC_kafka_DF.writeStream.format("delta").queryName(POC_query).outputMode("append").trigger(processingTime=micro_batch_fq).start(path=POC_File_RAW, checkpointLocation =POC_File_RAW_checkpoint)

# COMMAND ----------

#run.stop()
