package com.catalina;

import com.catalina.config.TestConfig;
import com.catalina.omni.kafka.router.service.NotificationService;
import com.catalina.omni.kafka.router.service.SlackNotificationServiceImpl;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {MessageSourceAutoConfiguration.class, SlackNotificationServiceImpl.class, TestConfig.class})
@Profile("test")
@TestPropertySource("classpath:application-test.properties")
public class NotificationTest {

    @Autowired
    private NotificationService notificationService;

    @Test
    @Ignore
    public void testNotification() {
        notificationService.sendMessage("invalid.message.detected", 0, "testing123");
    }
}
