package com.catalina;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class LocalConnectivityTest {

    @Test
    @Ignore
    public void testConnectToNewKafka() throws InterruptedException, ExecutionException, TimeoutException {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.serializer","org.apache.kafka.common.serialization.LongSerializer");
        props.put("value.serializer", "com.catalina.omni.kafka.router.impl.MessageSerializer");
        final KafkaProducer producer = new KafkaProducer(props);
        producer.send(new ProducerRecord("test-topic","aa")).get(5000, TimeUnit.MILLISECONDS);
    }

    @Test
    @Ignore
    public void createTopicInOldKafka(){
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.serializer","org.apache.kafka.common.serialization.LongSerializer");
        props.put("value.serializer", "com.catalina.omni.kafka.router.impl.MessageSerializer");
        final AdminClient adminClient = AdminClient.create(props);
        adminClient.createTopics(Collections.singleton(new NewTopic("orders", 10, (short) 1)));
    }
}
