package com.catalina.omni.kafka.router.api.filter;

import com.catalina.platform.api.models.advertiser.Promotion;
import com.catalina.platform.avro.ChannelType;
import com.catalina.platform.avro.Event;
import com.catalina.platform.avro.EventType;
import com.catalina.platform.avro.Order;
import com.google.common.collect.ImmutableSet;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

public interface Filters {

    Set<String> PLATFORM_COMBINATIONS = ImmutableSet.of("platform", "PLATFORM", "Platform");

    default Predicate<Event> isStore() {
        return event -> ChannelType.store.equals(event.getChannelType());
    }

    default Predicate<Event> isImpression() {
        return event -> EventType.impression.equals(event.getType());
    }

    default Predicate<Order> hasValidCids() {
        return order -> !Optional.ofNullable(order.getCids()).orElse(Collections.emptyList()).isEmpty();
    }

    default Predicate<Promotion> isPromotionSegment() {
        return promotion -> Optional.ofNullable(promotion)
                .map(p -> p.metadata)
                .map(Map::keySet)
                .filter(keys -> keys.stream().anyMatch(key -> PLATFORM_COMBINATIONS.stream().anyMatch(key::contains)))
                .isPresent();
    }
}
