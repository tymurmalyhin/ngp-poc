package com.catalina.omni.kafka.router;

import com.catalina.omni.core.configuration.hystrix.HystrixConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@Import(value = {HystrixConfiguration.class})
@EnableHystrix
@EnableHystrixDashboard
@EnableScheduling
public class KafkaRouterApp {

    public static void main(String[] args) {
        SpringApplication.run(KafkaRouterApp.class).registerShutdownHook();
    }
}
