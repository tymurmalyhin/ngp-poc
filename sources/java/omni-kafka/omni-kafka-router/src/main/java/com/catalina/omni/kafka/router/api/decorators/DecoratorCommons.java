package com.catalina.omni.kafka.router.api.decorators;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;

import java.util.Optional;
import java.util.function.Function;

import static java.nio.charset.StandardCharsets.UTF_8;

public interface DecoratorCommons {

    byte[] EMPTY_HEADER = new byte[0];

    String HEADER_TIME = "time";
    String HEADER_COUNTRY_CODE = "country_code";
    String HEADER_NETWORK_ID = "network_id";
    String HEADER_CHANNEL_TYPE = "channel_type";
    String HEADER_AD_ID = "ad_id";
    String HEADER_AD_GROUP_ID = "ad_group_id";
    String HEADER_AD_NAME = "ad_name";
    String HEADER_TARGETING_DESCRIPTION = "targeting_description";
    String HEADER_CAMEOID = "cameoid";
    String HEADER_PLATFORM = "platform";
    String HEADER_REACH = "reach";
    String HEADER_SEQUENTIAL = "sequential";

    static byte[] getUtf8Bytes(Object s) {
        return Optional.ofNullable(s)
                .map(Object::toString)
                .map(s1 -> s1.getBytes(UTF_8))
                .orElse(EMPTY_HEADER);
    }

    static <T> void addHeader(ProducerRecord<Long, T> record, String headerId, byte[] headerValue) {
        record.headers().add(new RecordHeader(headerId, headerValue));
    }

    static <T> ChainedDecorator<T, ProducerRecord<Long, T>> addHeader(Function<T, Object> extractData, String headerName) {
        return (event, record) -> {
            Optional.of(event)
                    .map(extractData)
                    .map(DecoratorCommons::getUtf8Bytes)
                    .ifPresent(bytes -> addHeader(record, headerName, bytes));
            return record;
        };
    }
}
