package com.catalina.omni.kafka.router.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Builder
@Accessors(fluent = true)
public class ProcessorStats {

    @JsonProperty
    private String processorName;

    @JsonProperty
    private String topicName;

    @JsonProperty
    private int nrOfPipelines;

    @JsonProperty
    private boolean paused;
}
