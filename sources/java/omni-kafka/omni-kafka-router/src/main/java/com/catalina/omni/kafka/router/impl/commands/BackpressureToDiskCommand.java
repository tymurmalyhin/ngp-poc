package com.catalina.omni.kafka.router.impl.commands;

import com.catalina.omni.kafka.router.api.serialization.Serializer;
import com.catalina.omni.kafka.router.impl.BackpresureProcessor;
import com.netflix.hystrix.*;
import io.micrometer.core.instrument.Counter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static com.netflix.hystrix.HystrixCommandProperties.ExecutionIsolationStrategy.SEMAPHORE;
import static java.util.UUID.randomUUID;

@Slf4j
public class BackpressureToDiskCommand<OUT> extends HystrixCommand<Void> {

    private final String backpressureFolder;
    private final Serializer<OUT> serializer;
    private final BackpresureProcessor<OUT> resubmitProcessor;

    private final OUT data;
    private final Counter successCounter;
    private final Counter failedCounter;

    public BackpressureToDiskCommand(String backpressureFolder,
                                     Serializer<OUT> serializer,
                                     BackpresureProcessor<OUT> resubmitProcessor,
                                     OUT data,
                                     int timeout,
                                     Counter successCounter,
                                     Counter failedCounter) {
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("backpressure"))
                .andThreadPoolKey(HystrixThreadPoolKey.Factory.asKey("backpressure"))
                .andCommandKey(HystrixCommandKey.Factory.asKey("backpressureToDisk"))
                .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
                        .withFallbackEnabled(true)
                        .withExecutionIsolationSemaphoreMaxConcurrentRequests(100000)
                        .withExecutionIsolationStrategy(SEMAPHORE)
                        .withExecutionTimeoutEnabled(true)
                        .withExecutionTimeoutInMilliseconds(timeout)));
        this.backpressureFolder = backpressureFolder;
        this.serializer = serializer;
        this.resubmitProcessor = resubmitProcessor;
        this.data = data;
        this.successCounter = successCounter;
        this.failedCounter = failedCounter;
    }

    @Override
    protected Void run() {
        try {
            Files.write(backpressureFileName(backpressureFolder, randomUUID().toString(),
                    resubmitProcessor.getProcessorName()), serializer.apply(data));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        successCounter.increment();
        return null;
    }

    @Override
    protected Void getFallback() {
        log.error("Error persisting backpressured event, re-submitting", getExecutionException());
        resubmitProcessor.backdoorSubmitMessage(data);
        //this should theoretically not happen, but in case we can't write the message to the disk, just
        //try to re-send it.

        failedCounter.increment();
        return null;
    }

    public static Path backpressureFileName(String backpressureFolder, String uuid, String processorName) {
        return Paths.get(backpressureFolder, processorName, uuid + ".data");
    }
}
