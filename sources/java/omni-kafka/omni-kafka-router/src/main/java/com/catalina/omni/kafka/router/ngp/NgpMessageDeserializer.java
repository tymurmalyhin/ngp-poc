package com.catalina.omni.kafka.router.ngp;

import com.catalina.ngp.kafka.message.ngp.NgpMessage;
import com.catalina.ngp.kafka.serialization.NgpMessageSerializer;
import com.catalina.omni.kafka.router.api.serialization.Deserializer;
import io.micrometer.core.instrument.Timer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component("ngpMessageDeserializer")
@Slf4j
public class NgpMessageDeserializer implements Deserializer<NgpMessage> {

    private final Timer deserializationTimer;

    public NgpMessageDeserializer(@Qualifier("ngpMessageDeserializerTimer") Timer deserializationTimer) {
        this.deserializationTimer = deserializationTimer;
    }

    @Override
    public Optional<NgpMessage> apply(byte[] bytes) {
        return deserializationTimer.record(() -> Optional.of(NgpMessageSerializer.fromBytes(bytes)));
    }
}
