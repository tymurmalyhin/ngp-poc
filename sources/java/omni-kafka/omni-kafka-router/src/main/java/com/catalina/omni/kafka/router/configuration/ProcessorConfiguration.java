package com.catalina.omni.kafka.router.configuration;

import com.catalina.ngp.kafka.message.ngp.NgpMessage;
import com.catalina.omni.core.metrics.MetricsCore;
import com.catalina.omni.kafka.router.api.ForwardingPipeline;
import com.catalina.omni.kafka.router.api.serialization.Deserializer;
import com.catalina.omni.kafka.router.impl.ForwardingProcessor;
import com.catalina.omni.kafka.router.impl.MessageSerializer;
import com.catalina.omni.kafka.router.service.NotificationService;
import com.catalina.platform.api.models.advertiser.Promotion;
import com.catalina.platform.avro.Event;
import com.catalina.platform.avro.Order;
import kafka.javaapi.consumer.ConsumerConnector;
import org.apache.http.impl.client.HttpClients;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Configuration
@EnableConfigurationProperties(ProcessorSettings.class)
public class ProcessorConfiguration {

    @Bean("ngpMessageProcessor")
    public ForwardingProcessor<NgpMessage> ngpMessageProcessor(InputConfiguration inputConfiguration,
                                                               @Value("${spring.profiles.active}") String profile,
                                                               @Qualifier("ngpMessageConsumer") ConsumerConnector consumerConnector,
                                                               MetricsCore metricsCore,
                                                               List<ForwardingPipeline<NgpMessage, ProducerRecord<Long, NgpMessage>>> pipelines,
                                                               @Qualifier("ngpMessageDeserializer") Deserializer<NgpMessage> deserializer,
                                                               ProcessorSettings processorSettings,
                                                               RestTemplate restTemplate,
                                                               @Value("${events.backpressure.timeout}") int backpressureTimeout,
                                                               @Value("${events.backpressure.folder}") String backpressureFolder,
                                                               NotificationService notificationService) {
        final MessageSerializer<NgpMessage> ngpMessageSerializer = new MessageSerializer<>();
        return new ForwardingProcessor<>("ngpMessageProcessor", inputConfiguration.getNgpmessage().getTopic(), profile, consumerConnector,
                metricsCore, pipelines, deserializer, processorSettings,
                backpressureTimeout, restTemplate,
                ngpMessage -> ngpMessageSerializer.serialize(null, ngpMessage), backpressureFolder, notificationService
        );
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        RestTemplate restTemplate = builder.build();
        restTemplate.setRequestFactory(getHttpRequestFactory());
        return restTemplate;
    }

    /**
     * Should prevent premature closing of connection pool
     * <p>
     * https://github.com/spring-cloud/spring-cloud-commons/issues/330
     */
    @Bean
    @RefreshScope
    public HttpComponentsClientHttpRequestFactory getHttpRequestFactory() {
        return new HttpComponentsClientHttpRequestFactory(HttpClients.createDefault());
    }
}
