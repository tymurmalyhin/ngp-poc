package com.catalina.omni.kafka.router.controller;

import com.catalina.omni.kafka.router.dto.ProcessorStats;
import com.catalina.omni.kafka.router.impl.ForwardingProcessor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.lang.String.format;

@RestController
@RequestMapping("/perf")
public class PerformanceController {

    private final List<ForwardingProcessor> processors;

    public PerformanceController(List<ForwardingProcessor> processors) {
        this.processors = processors;
    }

    /**
     * Enables/disables forwarding across all processors
     */
    @PostMapping("/forwarding/{enable}")
    public void enableDisableForwarding(@PathVariable("enable") boolean enable) {
        if (enable) {
            processors.forEach(forwardingProcessor -> forwardingProcessor.resume(true, false));
        } else {
            processors.forEach(forwardingProcessor -> forwardingProcessor.pause(true, false));
        }
    }

    @PostMapping("/forwarding/limit/{limit}")
    public void rateLimitForwarding(@PathVariable("limit") int limit) {
        processors.forEach(forwardingProcessor -> forwardingProcessor.rateLimitProcessing(limit));
    }

    /**
     * Enables/disables bcakpressure across all processors
     */
    @PostMapping("/backpressure/{enable}")
    public void enableDisableBackpressure(@PathVariable("enable") boolean enable) {
        if (enable) {
            processors.forEach(forwardingProcessor -> forwardingProcessor.resume(false, true));
        } else {
            processors.forEach(forwardingProcessor -> forwardingProcessor.pause(false, true));
        }
    }

    @PostMapping("/backpressure/limit/{limit}")
    public void rateLimitBackpressure(@PathVariable("limit") int limit) {
        processors.forEach(forwardingProcessor -> forwardingProcessor.rateLimitBackpressure(limit));
    }

    @PostMapping("/processor/{name}/forwarding/limit/{limit}")
    public ResponseEntity<String> rateLimitForwardingForSingleFlow(@PathVariable("name") String name,
                                                                   @PathVariable("limit") int limit) {
        final Optional<ForwardingProcessor> processor = processors.stream()
                .filter(forwardingProcessor -> name.equals(forwardingProcessor.getProcessorName()))
                .findFirst();

        processor.ifPresent(forwardingProcessor -> forwardingProcessor.rateLimitProcessing(limit));

        return respondOkOrNotFound(name, processor);
    }

    @PostMapping("/processor/{name}/backpressure/limit/{limit}")
    public ResponseEntity<String> rateLimitBackpressureForSingleFlow(@PathVariable("name") String name,
                                                                     @PathVariable("limit") int limit) {
        final Optional<ForwardingProcessor> processor = processors.stream()
                .filter(forwardingProcessor -> name.equals(forwardingProcessor.getProcessorName()))
                .findFirst();

        processor.ifPresent(forwardingProcessor -> forwardingProcessor.rateLimitBackpressure(limit));

        return respondOkOrNotFound(name, processor);
    }

    /**
     * Provides fine-grained control over separate processors.
     *
     * @param name         of the processor we want to enable/disable
     * @param enable       whether we want to resume(true) or pause(false)
     * @param forwarding   whether forwarding should be paused/resumed
     * @param backpressure whether backpressure should be paused/resumed
     */
    @PostMapping("/processor/{name}/{enable}/forwarding/{forwarding}/backpressure/{backpressure}")
    public ResponseEntity<String> onOffSingleFlow(@PathVariable("name") String name,
                                                  @PathVariable("enable") boolean enable,
                                                  @PathVariable("forwarding") boolean forwarding,
                                                  @PathVariable("backpressure") boolean backpressure) {
        final Optional<ForwardingProcessor> processor = processors.stream()
                .filter(forwardingProcessor -> name.equals(forwardingProcessor.getProcessorName()))
                .findFirst();

        processor.ifPresent(forwardingProcessor -> {
            if (enable) {
                forwardingProcessor.resume(forwarding, backpressure);
            } else {
                forwardingProcessor.pause(forwarding, backpressure);
            }
        });

        return respondOkOrNotFound(name, processor);
    }

    @GetMapping("/processors")
    public List<ProcessorStats> listProcessors() {
        return processors.stream()
                .map(forwardingProcessor -> ProcessorStats.builder()
                        .processorName(forwardingProcessor.getProcessorName())
                        .topicName(forwardingProcessor.getTopicName())
                        .nrOfPipelines(forwardingProcessor.getPipelines().size())
                        .paused(forwardingProcessor.isPaused())
                        .build())
                .collect(Collectors.toList());
    }

    private static ResponseEntity<String> respondOkOrNotFound(@PathVariable("name") String name, Optional<ForwardingProcessor> processor) {
        return processor.map(forwardingProcessor -> ResponseEntity.ok("OK"))
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).body(format("Processor %s was not found", name)));
    }
}
