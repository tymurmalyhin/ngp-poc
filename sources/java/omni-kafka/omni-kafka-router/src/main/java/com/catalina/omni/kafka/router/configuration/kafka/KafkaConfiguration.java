package com.catalina.omni.kafka.router.configuration.kafka;

import com.catalina.omni.kafka.router.configuration.InputConfiguration;
import kafka.consumer.ConsumerConfig;
import kafka.javaapi.consumer.ConsumerConnector;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

import static kafka.consumer.Consumer.createJavaConsumerConnector;

@Configuration
public class KafkaConfiguration {

    /**
     * This consumer connects to old Vault Kafka
     */

    @Bean(name = "ngpMessageConsumer", destroyMethod = "shutdown")
    public ConsumerConnector consumerConnectorNgpMessage(InputConfiguration inputConfiguration) {
        return createConsumer(inputConfiguration.getNgpmessage());
    }

    /**
     * This producer connects to our new Kubernetes Kafka
     */
    @Bean(destroyMethod = "close")
    public Producer producer(@Value("${events.output.server}") String server
            , @Value("${events.output.security-protocol}") String securityProtocol
            , @Value("${events.output.sasl-mechanism}") String saslMechanism
            , @Value("${events.output.sasl-jaas-config}") String saslJaasConfig
    ) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, server);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.LongSerializer");
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "com.catalina.omni.kafka.router.impl.MessageSerializer");
        props.put(ProducerConfig.MAX_REQUEST_SIZE_CONFIG, String.valueOf(4 * 1024 * 1024));
        if (securityProtocol != null && !securityProtocol.isEmpty()) {
            props.put( "security.protocol", securityProtocol);
            if (securityProtocol.equals("SASL_SSL")) {
                props.put("sasl.mechanism", saslMechanism);
                props.put("sasl.jaas.config", saslJaasConfig);
            }
        }
        return new KafkaProducer(props);
    }

    private static ConsumerConnector createConsumer(InputConfiguration.InputFlowConfiguration cfg) {
        Properties props = new Properties();
        props.put("zk.connect", cfg.getServer());
        props.put("groupid", cfg.getGroup());
        props.put("enable.auto.commit", "true");

        props.put("auto.offset.reset", cfg.getAutoOffsetReset());
        props.put("auto.commit.interval.ms", cfg.getAutoCommitInterval());
        props.put("backoff.increment.ms", cfg.getBackoffIntervalMs());
        props.put("rebalance.retries.max", cfg.getRebalanceRetriesMax());
        props.put("socket.timeout.ms", cfg.getSocketTimeoutMs());


        props.put("zk.sessiontimeout.ms", cfg.getSessionTimeout());
        props.put("zk.connectiontimeout.ms", cfg.getConnectionTimeout());
        return createJavaConsumerConnector(new ConsumerConfig(props));
    }


}
