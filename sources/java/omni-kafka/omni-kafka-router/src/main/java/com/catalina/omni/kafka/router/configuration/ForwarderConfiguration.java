package com.catalina.omni.kafka.router.configuration;

import com.catalina.ngp.kafka.message.ngp.NgpMessage;
import io.micrometer.core.instrument.Timer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;

@Configuration
public class ForwarderConfiguration {

    @Bean("ngpMessageForwarder")
    public Consumer<ProducerRecord<Long, NgpMessage>> ngpMessageForwarder(Producer producer,
                                                                          @Qualifier("ngpMessageForwarderTimer") Timer ngpMessageForwarderTimer) {
        return record -> ngpMessageForwarderTimer.record(() -> {
            try {
                producer.send(record).get(5000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                throw new IllegalStateException(e);
            }
        });
    }

}
