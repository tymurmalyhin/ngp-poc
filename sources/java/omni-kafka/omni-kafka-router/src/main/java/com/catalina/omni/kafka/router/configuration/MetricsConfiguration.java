package com.catalina.omni.kafka.router.configuration;

import com.catalina.omni.core.metrics.MetricsCore;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Timer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.lang.String.format;

@Configuration
public class MetricsConfiguration {

    @Value("${spring.profiles.active}")
    private String activeProfile;

    @Autowired
    private MetricsCore metricsCore;

    @Bean("ngpMessageCounter")
    public Counter ngpMessageCounter() {
        return metricsCore.counterWithDescription(format("metrics.%s.ngpmessage.events", activeProfile),
                "Total number of ngpMessage events");
    }

    @Bean("ngpMessageForwarderTimer")
    public Timer ngpMessageForwarderTimer(MetricsCore metricsCore) {
        return metricsCore.timerWithDescription(format("metrics.%s.ngpmessage.forwarder.timer", activeProfile),
                "Timer for how much time ngpMessage events forwarding takes");
    }


    @Bean("ngpMessageDecoratorTimer")
    public Timer ngpMessageDecoratorTimer(MetricsCore metricsCore) {
        return metricsCore.timerWithDescription(format("metrics.%s.ngpmessage.events.decorator.timer", activeProfile),
                "Timer for how much time ngpMessage events decoration takes");
    }


    @Bean("ngpMessageDeserializerTimer")
    public Timer ngpMessageDeserializerTimer(MetricsCore metricsCore) {
        return metricsCore.timerWithDescription(format("metrics.%s.ngpmessge.deserializer.timer", activeProfile),
                "Timer for how much time deserializing ngpMessage takes");
    }

}
