package com.catalina.omni.kafka.router.api.serialization;

import java.util.Optional;
import java.util.function.Function;

public interface Deserializer<OUT> extends Function<byte[], Optional<OUT>> {

}
