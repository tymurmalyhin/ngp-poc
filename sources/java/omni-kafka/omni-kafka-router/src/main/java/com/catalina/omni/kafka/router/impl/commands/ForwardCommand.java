package com.catalina.omni.kafka.router.impl.commands;

import com.catalina.omni.kafka.router.impl.BackpresureProcessor;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixCommandProperties;
import io.micrometer.core.instrument.Counter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.function.Consumer;

import static com.netflix.hystrix.HystrixCommandProperties.ExecutionIsolationStrategy.SEMAPHORE;
import static java.lang.String.format;

@Slf4j
public class ForwardCommand<OUT> extends HystrixCommand<Void> {

    private final BackpresureProcessor<OUT> backpresureTarget;
    private final Consumer<ProducerRecord<Long, OUT>> forwardFunction;
    private final ProducerRecord<Long, OUT> data;
    private final Counter success;
    private final Counter failed;

    public ForwardCommand(BackpresureProcessor<OUT> backpresureTo,
                          String topicName,
                          int timeout,
                          Consumer<ProducerRecord<Long, OUT>> forwardFunction,
                          ProducerRecord<Long, OUT> data,
                          Counter success,
                          Counter failed,
                          String groupTag) {
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey(backpresureTo.getProcessorName() + "-" + groupTag))
                .andCommandKey(HystrixCommandKey.Factory.asKey(format("%s_%s_%s", backpresureTo.getProcessorName(), topicName, groupTag)))
                .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
                        .withFallbackEnabled(true)
                        .withExecutionIsolationSemaphoreMaxConcurrentRequests(100000)
                        .withExecutionIsolationStrategy(SEMAPHORE)
                        .withExecutionTimeoutEnabled(true)
                        .withExecutionTimeoutInMilliseconds(timeout)));
        this.backpresureTarget = backpresureTo;
        this.forwardFunction = forwardFunction;
        this.data = data;
        this.success = success;
        this.failed = failed;
    }

    @Override
    protected Void run() throws Exception {
        forwardFunction.accept(data);
        success.increment();
        log.trace("Event forwarded :{}", data);
        return null;
    }

    @Override
    protected Void getFallback() {
        failed.increment();
        log.error("Event forward failed: {}", data, getExecutionException());
        backpresureTarget.backpressureMessage(data.value());
        return null;
    }
}
