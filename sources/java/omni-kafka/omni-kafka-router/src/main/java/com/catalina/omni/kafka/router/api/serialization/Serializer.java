package com.catalina.omni.kafka.router.api.serialization;

import java.util.function.Function;

public interface Serializer<IN> extends Function<IN, byte[]> {
}
