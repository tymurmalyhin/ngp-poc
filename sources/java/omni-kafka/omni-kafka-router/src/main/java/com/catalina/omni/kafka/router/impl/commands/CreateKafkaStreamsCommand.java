package com.catalina.omni.kafka.router.impl.commands;

import com.netflix.hystrix.*;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CreateKafkaStreamsCommand extends HystrixCommand<Map<String, List<KafkaStream<Message>>>> {

    private static final Logger LOG = LoggerFactory.getLogger(CreateKafkaStreamsCommand.class);

    private final ConsumerConnector consumerConnector;
    private final List<String> topics;

    public CreateKafkaStreamsCommand(ConsumerConnector consumerConnector,
                                     List<String> topics, int timeout) {
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("kafka"))
                .andThreadPoolKey(HystrixThreadPoolKey.Factory.asKey("kafka"))
                .andCommandKey(HystrixCommandKey.Factory.asKey("createKafkaStreams"))
                .andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
                        .withFallbackEnabled(true)
                        .withExecutionTimeoutEnabled(true)
                        .withExecutionTimeoutInMilliseconds(timeout)));
        this.consumerConnector = consumerConnector;
        this.topics = topics;
    }

    @Override
    protected Map<String, List<KafkaStream<Message>>> run() {
        final Map<String, Integer> topicMap = topics.stream().collect(Collectors.toMap(o -> o, o -> 1));
        Map<String, List<KafkaStream<Message>>> topicMessageStreams = consumerConnector.createMessageStreams(topicMap);
        return topicMessageStreams;
    }

    @Override
    protected Map<String, List<KafkaStream<Message>>> getFallback() {
        LOG.error("Error retrieving kafka streams", getFailedExecutionException());
        return Collections.emptyMap();
    }
}
