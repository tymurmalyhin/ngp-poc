package com.catalina.omni.kafka.router.api;

import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * filter - condition on what events should go trough this pipeline
 * decorator - function to add any metadata to message
 * forwarder - consumer to forward event to destination topic
 */
@Builder
@Getter
@Accessors(fluent = true)
public class ForwardingPipeline<IN, OUT> {

    private String outTopic;
    private Predicate<IN> filter;
    private Function<IN, OUT> decorator;
    private Consumer<OUT> forwarder;
}
