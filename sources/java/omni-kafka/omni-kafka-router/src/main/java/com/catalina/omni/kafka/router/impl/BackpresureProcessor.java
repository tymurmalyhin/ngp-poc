package com.catalina.omni.kafka.router.impl;

import com.catalina.omni.kafka.router.api.serialization.Serializer;
import com.catalina.omni.kafka.router.impl.commands.BackpressureToDiskCommand;
import io.micrometer.core.instrument.Counter;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

/**
 * If connection to new kafka goes away, we need to queue up/pause processing automatically so we don't loose data.
 * The processor itself can recover if connection goes back up
 */
@Slf4j
@Getter
public abstract class BackpresureProcessor<OUT> {

    private final String processorName;
    private final int backpressureTimeout;
    private final RestTemplate restTemplate;
    private final Serializer<OUT> serializer;
    private final int pauseThreshold;
    private final int resumeAfter;
    private final Object pauseLock = new Object();

    private final Counter successBackpressure;
    private final Counter failedBackpressure;
    private final String backpressureFolder;

    public BackpresureProcessor(String processorName,
                                int backpressureTimeout,
                                RestTemplate restTemplate, Serializer<OUT> serializer,
                                int pauseThreshold,
                                int resumeAfter,
                                Counter successBackpressure,
                                Counter failedBackpressure,
                                String backpressureFolder) {
        this.processorName = processorName;
        this.backpressureTimeout = backpressureTimeout;
        this.restTemplate = restTemplate;
        this.serializer = serializer;
        this.pauseThreshold = pauseThreshold;
        this.resumeAfter = resumeAfter;
        this.successBackpressure = successBackpressure;
        this.failedBackpressure = failedBackpressure;
        this.backpressureFolder = backpressureFolder;
    }

    public void backpressureMessage(OUT message) {
        new BackpressureToDiskCommand<>(backpressureFolder, serializer, this, message, 5000,
                successBackpressure, failedBackpressure).execute();
    }

    /**
     * Pauses processing of this processor
     *
     */
    public abstract void pause(boolean forwarding, boolean backpressure);

    /**
     * Resumes processing of this processor
     *
     */
    public abstract void resume(boolean forwarding, boolean backpressure);

    /**
     * Sets the rate limit for processing
     * */
    public abstract void rateLimitProcessing(final int newValue);

    /**
     * Sets the rate limit for backpressure
     * */
    public abstract void rateLimitBackpressure(final int newValue);

    /**
     * Re-forwards message
     */
    public abstract CompletableFuture<Void> backdoorSubmitMessage(byte[] message);

    /**
     * Re-forwards message
     */
    public abstract CompletableFuture<Void> backdoorSubmitMessage(OUT message);

    public Path getInvalidPath(){
        return Paths.get(getBackpressureFolder()).resolve("invalid").resolve(getProcessorName());
    }

    public Path getBackpressurePath(){
        return Paths.get(getBackpressureFolder()).resolve(getProcessorName());
    }
}
