package com.catalina.omni.kafka.router.configuration;

import com.catalina.ngp.kafka.message.ngp.NgpMessage;
import com.catalina.omni.core.metrics.MetricsCore;
import com.catalina.omni.kafka.router.api.ForwardingPipeline;
import com.catalina.omni.kafka.router.api.filter.Filters;
import com.catalina.platform.api.models.advertiser.Promotion;
import com.catalina.platform.avro.Event;
import com.catalina.platform.avro.Order;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Timer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Consumer;
import java.util.function.Function;

import static com.catalina.omni.kafka.router.api.filter.AllMeasuredPredicate.measureAll;
import static com.catalina.omni.kafka.router.api.filter.SuccessMeasuredPredicate.measure;
import static java.lang.String.format;

@Configuration
public class PipelineConfiguration implements Filters {

    @Value("${spring.profiles.active}")
    private String activeProfile;

    @Autowired
    private MetricsCore metricsCore;

    @Bean("ngpMessagePipeline")
    public ForwardingPipeline<NgpMessage, ProducerRecord<Long, NgpMessage>> ngpMessagePipeline(
            @Value("${events.output.topic.ngpmessage}") String outTopic,
            @Qualifier("ngpMessageCounter") Counter isNgpMessageCounter,
            @Qualifier("ngpMessageDecorator") Function<NgpMessage, ProducerRecord<Long, NgpMessage>> decorator,
            @Qualifier("ngpMessageForwarder") Consumer<ProducerRecord<Long, NgpMessage>> ngpMessageForwarder) {
        return ForwardingPipeline.<NgpMessage, ProducerRecord<Long, NgpMessage>>builder()
                .outTopic(outTopic)
                .filter(measure(isNgpMessageCounter, filterTimer("isNgpMessage"), ngpMessage -> true ))
                .decorator(decorator)
                .forwarder(ngpMessageForwarder)
                .build();
    }

    private Timer filterTimer(String filterName) {
        return metricsCore.timerWithDescription(format("metrics.%s.filter.%s.timer", activeProfile, filterName),
                format("Timer for how much condition %s takes", filterName));
    }

}
