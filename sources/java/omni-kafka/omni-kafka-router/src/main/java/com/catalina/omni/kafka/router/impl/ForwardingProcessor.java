package com.catalina.omni.kafka.router.impl;

import com.catalina.omni.core.metrics.MetricsCore;
import com.catalina.omni.kafka.router.api.ForwardingPipeline;
import com.catalina.omni.kafka.router.api.ReducibleSemaphore;
import com.catalina.omni.kafka.router.api.serialization.Deserializer;
import com.catalina.omni.kafka.router.api.serialization.Serializer;
import com.catalina.omni.kafka.router.configuration.ProcessorSettings;
import com.catalina.omni.kafka.router.configuration.kafka.KafkaStreamsUtils;
import com.catalina.omni.kafka.router.impl.commands.ForwardCommand;
import com.catalina.omni.kafka.router.service.NotificationService;
import com.google.common.util.concurrent.RateLimiter;
import io.micrometer.core.instrument.Counter;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.message.Message;
import kafka.message.MessageAndMetadata;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.client.RestTemplate;

import java.io.Closeable;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.StreamSupport;

import static java.lang.String.format;

@Slf4j
@Getter
public class ForwardingProcessor<OUT> extends BackpresureProcessor<OUT> implements InitializingBean, Closeable {

    public static final int PERMITS = 30000;
    public static final int RATE_LIMIT = 15000;

    private final String topicName;
    private final ConsumerConnector consumerConnector;
    private final Deserializer<OUT> eventDeserializer;
    private final List<ForwardingPipeline<OUT, ProducerRecord<Long, OUT>>> pipelines;
    private final ExecutorService executorService;
    private final ExecutorService backpressureExecutorService;
    private final NotificationService notificationService;

    private final ReducibleSemaphore backpressureSemaphore = new ReducibleSemaphore(PERMITS);
    private final RateLimiter backpressureRateLimiter = RateLimiter.create(RATE_LIMIT);
    private final ReducibleSemaphore processingSemaphore = new ReducibleSemaphore(PERMITS);
    private final RateLimiter processingRateLimiter = RateLimiter.create(RATE_LIMIT);

    private final Counter inputCounter;
    private final Counter relevantCounter;
    private final Counter forwardedCounter;
    private final Counter forwardFailedCounter;
    private final AtomicInteger messageSizeGauge;

    private CompletableFuture<Void> runFuture;

    private boolean paused;

    public ForwardingProcessor(String processorName, String topicName, String profileName,
                               ConsumerConnector consumerConnector,
                               MetricsCore metricsCore,
                               List<ForwardingPipeline<OUT, ProducerRecord<Long, OUT>>> pipelines,
                               Deserializer<OUT> eventDeserializer,
                               ProcessorSettings processorSettings,
                               int backpressureTimeout,
                               RestTemplate restTemplate,
                               Serializer<OUT> serializer,
                               String backpressureFolder,
                               NotificationService notificationService) {
        super(processorName, backpressureTimeout, restTemplate, serializer, 500, PERMITS,
                metricsCore.counterWithDescription(
                        metricsBase(processorName, topicName, profileName) + ".backpressure.success",
                        "Total number of successfully backpressured messages"),
                metricsCore.counterWithDescription(
                        metricsBase(processorName, topicName, profileName) + ".backpressure.failed",
                        "Total number of failed backpressured messages"),
                backpressureFolder);
        this.topicName = topicName;
        this.consumerConnector = consumerConnector;
        this.pipelines = pipelines;
        this.eventDeserializer = eventDeserializer;
        this.notificationService = notificationService;
        this.executorService = Executors.newWorkStealingPool(processorSettings.threadPoolSize(getProcessorName()));
        this.backpressureExecutorService = Executors.newWorkStealingPool(processorSettings.backpressureThreadPoolSize(getProcessorName()));

        final String metricsBase = metricsBase(processorName, topicName, profileName);
        inputCounter = metricsCore.counterWithDescription(metricsBase + ".input",
                "Total number of read messages for this processor");
        relevantCounter = metricsCore.counterWithDescription(metricsBase + ".relevant",
                "Total number of relevant messages for this processor");
        forwardedCounter = metricsCore.counterWithDescription(metricsBase + ".forwarded",
                "Total number of forwarded messages for this processor");
        forwardFailedCounter = metricsCore.counterWithDescription(metricsBase + ".forward.failed",
                "Total number of messages for this processor where forwarding failed");
        messageSizeGauge = metricsCore.gaugeWithDescription(metricsBase + ".message.size",
                "Size of the messages that this processor is processing");
    }

    @Override
    public void afterPropertiesSet() {
        log.info("Starting processor {} for topic {}", getProcessorName(), topicName);
        submitStart();
    }

    @Override
    public void close() {
        this.pause(true, true);

        awaitPermitsWithTimeout(processingSemaphore, 0, "processing");
        awaitPermitsWithTimeout(backpressureSemaphore, 0, "backpressure");

        runFuture.cancel(true);
        executorService.shutdown();
        backpressureExecutorService.shutdown();
        log.info("Processor {} for topic {} stopped", getProcessorName(), topicName);
    }

    private static void awaitPermitsWithTimeout(ReducibleSemaphore semaphore, int upperBound, final String tag) {
        int counter = 0;
        while (semaphore.availablePermits() < upperBound) {
            log.info("Waiting till all {} permits are returned[Currently available {}]", tag, semaphore.availablePermits());
            counter++;
            try {
                TimeUnit.SECONDS.sleep(6);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            if (counter == 10) {
                log.info("Flushing of {} permits took over 1 minute,not waiting anymore", tag);
            }
        }
    }

    @Override
    public CompletableFuture<Void> backdoorSubmitMessage(OUT message) {
        backpressureSemaphore.acquireUninterruptibly();
        backpressureRateLimiter.acquire();
        return forwardAsync(message, backpressureExecutorService, "backpressure")
                .whenComplete((aVoid, throwable) -> backpressureSemaphore.release());
    }

    @Override
    public CompletableFuture<Void> backdoorSubmitMessage(byte[] message) {
        return CompletableFuture.supplyAsync(() -> eventDeserializer.apply(message))
                .thenCompose(optData -> {
                    if (optData.isPresent()) {
                        return this.backdoorSubmitMessage(optData.get());
                    } else {
                        //if deserialization failed, complete exceptionally, it will be moved to invalids one level up
                        final CompletableFuture<Void> fut = new CompletableFuture<>();
                        fut.completeExceptionally(new IllegalStateException("Unable to deserialize " + Arrays.toString(message)));
                        return fut;
                    }
                });
    }

    private void submitStart() {
        runFuture = CompletableFuture.runAsync(() -> {
            final Optional<KafkaStream<Message>> optStream = KafkaStreamsUtils.retrieveKafkaStream(topicName, consumerConnector);
            if (!optStream.isPresent()) {
                log.info("No non-empty stream found for topic");
                return;
            }

            final KafkaStream<Message> stream = optStream.get();

            log.info("Processor {} for topic {} connected, starting streaming ...", getProcessorName(), topicName);
            StreamSupport.stream(stream.spliterator(), false)
                    .peek(messageMessageAndMetadata -> processingSemaphore.acquireUninterruptibly())
                    .peek(messageMessageAndMetadata -> processingRateLimiter.acquire())
                    .peek(messageMessageAndMetadata -> inputCounter.increment())
                    .map(MessageAndMetadata::message)
                    .map(Message::payload)
                    .map(ForwardingProcessor::byteBufferToBytes)
                    .peek(bytes -> messageSizeGauge.set(bytes.length))
                    .map(eventDeserializer)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(in -> forwardAsync(in, executorService, "forwarding")
                            .whenComplete((aVoid, throwable) -> processingSemaphore.release())
                    );
        }, executorService)
                //the stream here is endless, so if this finishes(for example connection failure, just resubmit)
                .whenComplete((aVoid, throwable) -> {
                    if (throwable != null) {
                        log.error("Error streaming incoming messages", throwable);
                    } else {
                        log.info("Stream ended, retrieving again ...");
                    }
                    checkedWait(10000);
                    submitStart();
                });
    }

    public void pause(boolean forwarding, boolean backpressure) {
        if (forwarding) {
            processingSemaphore.pause();
            notificationService.sendMessage("processor.forwarding.paused", getProcessorName());
        }

        if (backpressure) {
            backpressureSemaphore.pause();
            notificationService.sendMessage("processor.backpressure.paused", getProcessorName());
        }

        paused = true;
    }

    public void resume(boolean forwarding, boolean backpressure) {
        if (forwarding) {
            processingSemaphore.resume();
            notificationService.sendMessage("processor.forwarding.resumed", getProcessorName());
        }

        if (backpressure) {
            backpressureSemaphore.resume();
            notificationService.sendMessage("processor.backpressure.resumed", getProcessorName());
        }
        paused = false;
    }

    @Override
    public void rateLimitProcessing(int newValue) {
        log.info("Setting rate limit for processing to {}[current value {}]", newValue, processingRateLimiter.getRate());
        processingRateLimiter.setRate(newValue);
    }

    @Override
    public void rateLimitBackpressure(int newValue) {
        log.info("Setting rate limit for backpressure to {}[current value {}]", newValue, backpressureRateLimiter.getRate());
        backpressureRateLimiter.setRate(newValue);
    }

    private CompletableFuture<Void> forwardAsync(OUT in, ExecutorService executor, String groupTag) {
        return CompletableFuture.runAsync(() -> pipelines.forEach(pipeline -> Optional.ofNullable(in)
                .filter(pipeline.filter())
                .map(input -> {
                    relevantCounter.increment();
                    return input;
                })
                .map(pipeline.decorator())
                .ifPresent(record -> new ForwardCommand<>(this, pipeline.outTopic(), 10000,
                        pipeline.forwarder(), record,
                        forwardedCounter, forwardFailedCounter, groupTag).execute())), executor);
    }

    private static String metricsBase(String processorName, String topicName, String profileName) {
        return format("metrics.%s.processor.%s.topic.%s", profileName, processorName, topicName);
    }

    private static byte[] byteBufferToBytes(ByteBuffer byteBuffer) {
        byte[] bytes = new byte[byteBuffer.limit()];
        byteBuffer.get(bytes);
        return bytes;
    }

    private static void checkedWait(int timeout) {
        try {
            TimeUnit.MILLISECONDS.sleep(timeout);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

    public ReducibleSemaphore getBackpressureSemaphore() {
        return backpressureSemaphore;
    }

    public RateLimiter getBackpressureRateLimiter() {
        return backpressureRateLimiter;
    }

    public ReducibleSemaphore getProcessingSemaphore() {
        return processingSemaphore;
    }

    public RateLimiter getProcessingRateLimiter() {
        return processingRateLimiter;
    }
}
