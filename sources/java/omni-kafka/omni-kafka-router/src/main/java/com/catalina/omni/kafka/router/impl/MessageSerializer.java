package com.catalina.omni.kafka.router.impl;

import com.catalina.ngp.kafka.message.ngp.NgpMessage;
import com.catalina.ngp.kafka.serialization.NgpMessageSerializer;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class MessageSerializer<T> implements Serializer<T> {

    public MessageSerializer() {
    }

    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public byte[] serialize(String s, T o) {
        if (o instanceof NgpMessage)
            return NgpMessageSerializer.toBytes((NgpMessage) o);
        throw new IllegalArgumentException("No serializer found for " + o.getClass());
    }

    @Override
    public void close() {

    }
}
