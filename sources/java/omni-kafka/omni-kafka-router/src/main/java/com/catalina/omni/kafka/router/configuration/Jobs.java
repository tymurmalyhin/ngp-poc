package com.catalina.omni.kafka.router.configuration;

import com.catalina.omni.core.metrics.MetricsCore;
import com.catalina.omni.kafka.router.impl.BackpresureProcessor;
import com.catalina.omni.kafka.router.impl.ForwardingProcessor;
import io.micrometer.core.instrument.Counter;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Component
@Slf4j
public class Jobs implements Closeable {

    private final List<ForwardingProcessor> processors;
    private final ExecutorService executorService;
    private final Path backpressureFolder;
    private final int pauseThreshold;
    private final int resumeAfter;
    private final Map<String, Counter> metricsInvalidFilesMoved;
    private final Map<String, Counter> metricsInvalidFilesMoveFailed;
    private final Map<String, Counter> metricsFilesDeletedAfterResubmit;
    private final Map<String, Counter> metricsFilesDeletedAfterResubmitFailed;

    public Jobs(List<ForwardingProcessor> processors,
                @Value("${events.backpressure.folder}") String backpressureFolder,
                @Value("${events.backpressure.pause.threshold}") int pauseThreshold,
                @Value("${events.backpressure.resume.after}") int resumeAfter,
                MetricsCore metricsCore,
                @Value("${spring.profiles.active}") String activeProfile) {
        this.processors = processors;
        this.backpressureFolder = Paths.get(backpressureFolder);
        this.pauseThreshold = pauseThreshold;
        this.resumeAfter = resumeAfter;
        this.executorService = Executors.newWorkStealingPool(3);

        metricsInvalidFilesMoved = createMetrics(processors, metricsCore, activeProfile,
                "metrics.%s.%s.invalid.backpressure.files.moved",
                "Files that were backpressured that were moved due to being unable to read");

        metricsInvalidFilesMoveFailed = createMetrics(processors, metricsCore, activeProfile,
                "metrics.%s.%s.invalid.backpressure.files.moved.failed",
                "Files that were backpressured that were moved due to being unable to read, and the move failed");

        metricsFilesDeletedAfterResubmit = createMetrics(processors, metricsCore, activeProfile,
                "metrics.%s.%s.valid.backpressure.files.deleted",
                "Files that were backpressured and were deleted after successful re-submit");

        metricsFilesDeletedAfterResubmitFailed = createMetrics(processors, metricsCore, activeProfile,
                "metrics.%s.%s.valid.backpressure.files.deleted.failed",
                "Files that were backpressured and re-submited, but the remove failed");
    }

    @NotNull
    public Map<String, Counter> createMetrics(List<ForwardingProcessor> processors, MetricsCore metricsCore, @Value("${spring.profiles.active}") String activeProfile, String s, String s2) {
        return processors.stream()
                .collect(Collectors.toMap(BackpresureProcessor::getProcessorName,
                        o -> metricsCore.counterWithDescription(format(s,
                                activeProfile, o.getProcessorName()),
                                s2)));
    }

    @Scheduled(fixedRate = 10000)
    public void checkBackpressure() {
        processors.forEach(processor -> {
            try {
                final long currentCount = Files.list(backpressureFolder.resolve(processor.getProcessorName())).count();
                log.info("Currently backpressured for processors:{} {}", processor.getProcessorName(), currentCount);

                if (!processor.isPaused() && currentCount > pauseThreshold) {
                    log.info("Pausing processor {}", processor.getProcessorName());
                    processor.pause(true, true);

                    executorService.submit(() -> {
                        try {
                            TimeUnit.MILLISECONDS.sleep(resumeAfter);
                            log.info("Processor {} has been resumed after {} milliseconds", processor.getProcessorName(), resumeAfter);
                            processor.resume(true, true);
                        } catch (InterruptedException e) {
                            throw new IllegalStateException(e);
                        }
                    });
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        });
    }

    @Scheduled(fixedRate = 30000)
    public void submitBackpressured() {
        processors.forEach(processor -> {
            final String processorName = processor.getProcessorName();
            final Path backpressureFolder = this.backpressureFolder.resolve(processorName);

            final AtomicLong submittedCounter = new AtomicLong();
            try {
                Files.list(backpressureFolder)
                        .peek(path -> submittedCounter.incrementAndGet())
                        .forEach(path -> {
                            final FileReadResult fileReadResult = checkedReadAllBytes(path);

                            if (fileReadResult.isSuccess()) {
                                final byte[] bytes = fileReadResult.getBytes();

                                if (bytes.length == 0) {
                                    //if we cant read the file that has been backpressured, we need to move it,
                                    //so it doesnt confuses the health check
                                    final FileMoveResult fileMoveResult = checkedMoveInvalid(processor, path);
                                    if (fileMoveResult.isSuccess()) {
                                        metricsInvalidFilesMoved.get(processorName).increment();
                                    } else {
                                        log.trace("Unable to move {}", path, fileMoveResult.getException());
                                        metricsInvalidFilesMoveFailed.get(processorName).increment();
                                    }
                                } else {
                                    processor.backdoorSubmitMessage(bytes)
                                            .whenComplete((voi, ex) -> {
                                                if (ex == null) {
                                                    final FileDeleteResult fileDeleteResult = checkedDelete(path);
                                                    if (fileDeleteResult.isSuccess()) {
                                                        metricsFilesDeletedAfterResubmit.get(processorName).increment();
                                                    } else {
                                                        log.trace("Unable to delete {}", path, fileDeleteResult.getException());
                                                        metricsFilesDeletedAfterResubmitFailed.get(processorName).increment();
                                                    }
                                                } else {
                                                    final FileMoveResult fileMoveResult = checkedMoveInvalid(processor, path);
                                                    if (fileMoveResult.isSuccess()) {
                                                        metricsInvalidFilesMoved.get(processorName).increment();
                                                    } else {
                                                        log.trace("Unable to move {}", path, fileMoveResult.getException());
                                                        metricsInvalidFilesMoveFailed.get(processorName).increment();
                                                    }
                                                }
                                            });
                                }
                            } else if (fileReadResult.getException() instanceof NoSuchFileException) {
                                //we have multiple replicas reading from the same dir - so this will happen
                                log.trace("File {} was not found, most likely it was already processed by other replica", path);
                            } else {
                                log.error("Error reading file {}", path, fileReadResult.getException());
                                //try to move file if possible
                                final FileMoveResult fileMoveResult = checkedMoveInvalid(processor, path);
                                if (fileMoveResult.isSuccess()) {
                                    metricsInvalidFilesMoved.get(processorName).increment();
                                } else {
                                    log.trace("Unable to move {}", path, fileMoveResult.getException());
                                    metricsInvalidFilesMoveFailed.get(processorName).increment();
                                }
                            }
                        });
                if (submittedCounter.get() > 0) {
                    log.info("{} backpressured messages submited for {}", submittedCounter.get(), processorName);
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        });
    }

    private static FileMoveResult checkedMoveInvalid(ForwardingProcessor processor, Path path) {
        try {
            Files.move(path, processor.getInvalidPath().resolve(path.getFileName()));
            return FileMoveResult.builder().build();
        } catch (IOException e) {
            return FileMoveResult.builder().exception(e).build();
        }
    }

    private static FileDeleteResult checkedDelete(Path path) {
        try {
            Files.delete(path);
            return FileDeleteResult.builder().build();
        } catch (IOException e) {
            return FileDeleteResult.builder().exception(e).build();
        }
    }

    private static FileReadResult checkedReadAllBytes(Path path) {
        try {
            return FileReadResult.builder()
                    .bytes(Files.readAllBytes(path))
                    .build();
        } catch (IOException e) {
            return FileReadResult.builder()
                    .bytes(new byte[0])
                    .exception(e)
                    .build();
        }
    }

    @Override
    public void close() throws IOException {
        executorService.shutdown();
    }

    @Getter
    @Builder
    static class FileReadResult {
        private final byte[] bytes;
        private final Exception exception;

        boolean isSuccess() {
            return exception == null;
        }
    }

    @Getter
    @Builder
    static class FileMoveResult {
        private final Exception exception;

        boolean isSuccess() {
            return exception == null;
        }
    }

    @Getter
    @Builder
    static class FileDeleteResult {
        private final Exception exception;

        boolean isSuccess() {
            return exception == null;
        }
    }
}
