package com.catalina.omni.kafka.router.api.filter;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Timer;

import java.util.function.Predicate;

/**
 * Condition with metrics for both true and false
 */
public class AllMeasuredPredicate<T> implements Predicate<T> {

    private final Counter positiveCounter;
    private final Counter negativeCounter;
    private final Timer operationTimer;
    private final Predicate<T> condition;

    private AllMeasuredPredicate(Counter positiveCounter,
                                 Counter negativeCounter,
                                 Timer operationTimer,
                                 Predicate<T> condition) {
        this.positiveCounter = positiveCounter;
        this.negativeCounter = negativeCounter;
        this.operationTimer = operationTimer;
        this.condition = condition;
    }

    public static <T> AllMeasuredPredicate<T> measureAll(Counter positiveCounter,
                                                         Counter negativeCounter,
                                                         Timer operationTimer,
                                                         Predicate<T> condition) {
        return new AllMeasuredPredicate<T>(positiveCounter, negativeCounter, operationTimer, condition);
    }

    @Override
    public boolean test(T t) {
        return operationTimer.record(() -> {
            if (condition.test(t)) {
                positiveCounter.increment();
                return true;
            } else {
                negativeCounter.increment();
                return false;
            }
        });
    }
}
