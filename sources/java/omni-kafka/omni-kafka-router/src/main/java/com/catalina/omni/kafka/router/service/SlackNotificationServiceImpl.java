package com.catalina.omni.kafka.router.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.stream.Stream;

import static java.util.Locale.getDefault;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;

@Service
public class SlackNotificationServiceImpl implements NotificationService {

    private final RestTemplate restTemplate;
    private final MessageSource messageSource;
    private final boolean notificationsOn;
    private final String slackToken;
    private final String slackChannel;
    private final String username;
    private final String env;
    private final String url;

    public SlackNotificationServiceImpl(RestTemplate restTemplate,
                                        MessageSource messageSource,
                                        @Value("${slack.notifications.on}") Boolean notificationsOn,
                                        @Value("${slack.channel}") String slackChannel,
                                        @Value("${slack.token}") String slackToken,
                                        @Value("${slack.username}") String username,
                                        @Value("${slack.env}") String env,
                                        @Value("${slack.url}") String url) {
        this.restTemplate = restTemplate;
        this.messageSource = messageSource;
        this.notificationsOn = notificationsOn;
        this.slackToken = slackToken;
        this.slackChannel = slackChannel;
        this.username = username;
        this.env = env;
        this.url = url;
    }


    @Override
    public void sendMessage(String messageCode, Object... params) {
        if (!notificationsOn) {
            return;
        }

        final Stream.Builder<Object> builder = Stream.builder().add(env.toUpperCase());
        for (Object param : params) {
            builder.add(param);
        }

        final String[] objParams = builder.build().map(Object::toString).toArray(String[]::new);
        final String message = messageSource.getMessage(messageCode, objParams, getDefault());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("token", slackToken);
        map.add("channel", slackChannel);
        map.add("username", username);
        map.add("text", message);

        restTemplate.postForObject(url, new HttpEntity<>(map, headers), HashMap.class);
    }
}
