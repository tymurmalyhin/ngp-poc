package com.catalina.omni.kafka.router.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "events-input")
@Getter
@Setter
public class InputConfiguration {

    private InputFlowConfiguration ngpmessage;

    @Getter
    @Setter
    public static class InputFlowConfiguration {
        private String topic;
        private String server;
        private String group;
        private int rateLimit;
        private String autoCommitInterval;
        private String backoffIntervalMs;
        private String rebalanceRetriesMax;
        private String autoOffsetReset;
        private String socketTimeoutMs;

        //for some stupid reason, zookeeper connector want's this as string
        private String sessionTimeout;
        private String connectionTimeout;
    }
}
