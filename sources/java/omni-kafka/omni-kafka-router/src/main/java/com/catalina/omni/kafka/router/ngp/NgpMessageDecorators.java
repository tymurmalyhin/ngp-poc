package com.catalina.omni.kafka.router.ngp;

import com.catalina.ngp.kafka.message.ngp.NgpMessage;
import com.catalina.omni.kafka.router.api.decorators.ChainedDecorator;
import com.catalina.omni.kafka.router.api.decorators.DecoratorCommons;
import org.apache.kafka.clients.producer.ProducerRecord;

import static com.catalina.omni.kafka.router.api.decorators.DecoratorCommons.addHeader;

public interface NgpMessageDecorators extends DecoratorCommons {

}
