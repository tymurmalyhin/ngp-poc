package com.catalina.omni.kafka.router.api.filter;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Timer;

import java.util.function.Predicate;

/**
 * Condition with metrics for true
 */
public class SuccessMeasuredPredicate<T> implements Predicate<T> {

    private final Counter positiveCounter;
    private final Timer operationTimer;
    private final Predicate<T> condition;

    private SuccessMeasuredPredicate(Counter positiveCounter, Timer operationTimer, Predicate<T> condition) {
        this.positiveCounter = positiveCounter;
        this.operationTimer = operationTimer;
        this.condition = condition;
    }

    public static <T> SuccessMeasuredPredicate<T> measure(Counter positiveCounter, Timer operationTimer, Predicate<T> condition) {
        return new SuccessMeasuredPredicate<T>(positiveCounter, operationTimer, condition);
    }

    @Override
    public boolean test(T t) {
        return operationTimer.record(() -> {
            if (condition.test(t)) {
                positiveCounter.increment();
                return true;
            } else {
                return false;
            }
        });
    }
}
