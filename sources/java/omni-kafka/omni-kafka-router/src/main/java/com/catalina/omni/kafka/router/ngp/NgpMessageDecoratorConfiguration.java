package com.catalina.omni.kafka.router.ngp;

import com.catalina.ngp.kafka.message.ngp.NgpMessage;
import com.catalina.omni.kafka.router.api.decorators.ChainedDecorator;
import io.micrometer.core.instrument.Timer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Function;


@Configuration
public class NgpMessageDecoratorConfiguration implements NgpMessageDecorators {


    @Bean("ngpMessageDecorator")
    public Function<NgpMessage, ProducerRecord<Long, NgpMessage>> ngpMessageDecorator(@Value("${events.output.topic.ngpmessage}") String ngpMessageTopic,
                                                                         @Qualifier("ngpMessageDecoratorTimer") Timer decoratorTimer) {
        return event -> decoratorTimer.record(() ->  new ProducerRecord<>(ngpMessageTopic, event));
    }

}

