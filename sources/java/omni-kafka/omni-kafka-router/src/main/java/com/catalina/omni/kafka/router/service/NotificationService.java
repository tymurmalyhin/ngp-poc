package com.catalina.omni.kafka.router.service;

public interface NotificationService {
    void sendMessage(String messageCode, Object... params);
}
