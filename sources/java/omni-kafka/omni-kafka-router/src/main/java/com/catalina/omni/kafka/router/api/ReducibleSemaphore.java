package com.catalina.omni.kafka.router.api;

import java.util.concurrent.Semaphore;

public class ReducibleSemaphore extends Semaphore {

    private final int originalPermits;

    public ReducibleSemaphore(int permits) {
        super(permits);
        this.originalPermits = permits;
    }

    public void pause() {
        super.reducePermits(originalPermits);
    }

    public void resume() {
        if (this.availablePermits() < 0) {
            super.release(originalPermits + this.availablePermits());
        } else {
            super.release(originalPermits);
        }

    }
}
