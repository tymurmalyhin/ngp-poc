package com.catalina.omni.kafka.router.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@ConfigurationProperties(prefix = "processor")
@Getter
public class ProcessorSettings {

    @NestedConfigurationProperty
    private final Map<String, Processor> config = new HashMap<>();

    public Processor configForProcessor(final String name) {
        return config.computeIfAbsent(name, key -> {
            throw new IllegalArgumentException(String.format("Configuration for processor %s not found", name));
        });
    }

    public int threadPoolSize(final String processorName) {
        return configForProcessor(processorName).getPoolSize();
    }

    public int backpressureThreadPoolSize(final String processorName) {
        return configForProcessor(processorName).getBackpressurePoolSize();
    }

    private static Function<String, String> notFound(String processorName) {
        return key -> {
            throw new IllegalStateException(String.format("Key %s not found for %s", key, processorName));
        };
    }

    @Getter
    @Setter
    public static class Processor {
        private Integer poolSize;
        private Integer backpressurePoolSize;
    }

}
