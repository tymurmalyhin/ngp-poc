package com.catalina.omni.kafka.router.api.decorators;

import java.util.Objects;
import java.util.function.BiFunction;

/**
 * Function that allows chaining decorator functions
 */
@FunctionalInterface
public interface ChainedDecorator<IN, IN_OUT> extends BiFunction<IN, IN_OUT, IN_OUT> {

    default ChainedDecorator<IN, IN_OUT> andThen(BiFunction<IN, IN_OUT, IN_OUT> after) {
        Objects.requireNonNull(after);
        return (t, u) -> after.apply(t, this.apply(t, u));
    }
}
