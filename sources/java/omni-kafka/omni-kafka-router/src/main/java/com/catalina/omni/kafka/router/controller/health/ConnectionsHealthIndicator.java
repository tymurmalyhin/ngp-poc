package com.catalina.omni.kafka.router.controller.health;


import com.catalina.omni.kafka.router.configuration.InputConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.common.PartitionInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ConnectionsHealthIndicator extends AbstractHealthIndicator {

    private final List<String> outputServers;
    private final Producer k8Producer;
    private final InputConfiguration inputConfiguration;
    private final String instoreOutputTopic;
    private final String orderOutputTopic;
    private final String promotionsOutputTopic;
    private final String promotionSegmentsOutputTopic;
    private final String ngpMessageOutputTopic;

    public ConnectionsHealthIndicator(@Value("${events.output.server}") String outputServers,
                                      Producer k8Producer,
                                      InputConfiguration inputConfiguration,
                                      @Value("${events.output.topic.instore}") String instoreOutputTopic,
                                      @Value("${events.output.topic.orders}") String orderOutputTopic,
                                      @Value("${events.output.topic.promotions}") String promotionsOutputTopic,
                                      @Value("${events.output.topic.promotions-segments}") String promotionSegmentsOutputTopic,
                                      @Value("${events.output.topic.ngpmessage}") String ngpMessageOutputTopic
                                    ) {
        this.outputServers = Arrays.stream(outputServers.split(",")).collect(Collectors.toList());
        this.k8Producer = k8Producer;

        this.inputConfiguration = inputConfiguration;
        this.instoreOutputTopic = instoreOutputTopic;
        this.orderOutputTopic = orderOutputTopic;
        this.promotionsOutputTopic = promotionsOutputTopic;
        this.promotionSegmentsOutputTopic = promotionSegmentsOutputTopic;
        this.ngpMessageOutputTopic = ngpMessageOutputTopic;
    }

    @Override
    protected void doHealthCheck(Health.Builder builder) {

        final InputConfiguration.InputFlowConfiguration ngpMessage = inputConfiguration.getNgpmessage();
/*
        final InputConfiguration.InputFlowConfiguration instore = inputConfiguration.getInstore();
        final InputConfiguration.InputFlowConfiguration orders = inputConfiguration.getOrders();
        final InputConfiguration.InputFlowConfiguration promotions = inputConfiguration.getPromotions();
*/

        isHostReachable(builder, ngpMessage.getServer(),
                "Instore - Input zookeper host reachable",
                "Instore - Input zookeper host unreachable",
                "Instore - Unable to ping input zookeeper");

/*
        isHostReachable(builder, instore.getServer(),
                "Instore - Input zookeper host reachable",
                "Instore - Input zookeper host unreachable",
                "Instore - Unable to ping input zookeeper");

        isHostReachable(builder, orders.getServer(),
                "Orders - Input zookeper host reachable",
                "Orders - Input zookeper host unreachable",
                "Orders - Unable to ping input zookeeper");

        isHostReachable(builder, promotions.getServer(),
                "Promotions - Input zookeper host reachable",
                "Promotions - Input zookeper host unreachable",
                "Promotions - Unable to ping input zookeeper");
*/
        final AtomicInteger outputNodeCounter = new AtomicInteger();
        outputServers.forEach(hostAndPort -> {
            final int index = outputNodeCounter.incrementAndGet();
            isHostReachable(builder, hostAndPort,
                    "Output kafka host " + index + " reachable",
                    "Output kafka host " + index + " unreachable",
                    "Unable to ping output kafka host " + index);
        });
        createTopicInfo(builder, ngpMessageOutputTopic);
    }

    private void createTopicInfo(Health.Builder builder, String topic) {
        final List<PartitionInfo> partitions = k8Producer.partitionsFor(topic);
        partitions.forEach(partitionInfo ->
                        builder.withDetail(String.format("Topic %s, Partition %s", topic, partitionInfo.partition()), partitionInfo.toString())
                                .status(partitionInfo.offlineReplicas().length == 0 ? Status.UP : Status.DOWN));
    }

    private static void isHostReachable(Health.Builder builder,
                                        String hostAndPort,
                                        String reachableMessage,
                                        String unreachableMessage,
                                        String failedMessage) {
        final String host = hostAndPort.split(":")[0];
        try {
            if (InetAddress.getByName(host).isReachable(5000)) {
                builder.withDetail(reachableMessage, host).up();
            } else {
                builder.withDetail(unreachableMessage, host).down();
            }
        } catch (IOException e) {
            builder.withDetail(failedMessage, host).down(e);
        }
    }
}
