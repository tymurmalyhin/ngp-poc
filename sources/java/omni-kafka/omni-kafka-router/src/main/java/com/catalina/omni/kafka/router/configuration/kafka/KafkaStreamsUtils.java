package com.catalina.omni.kafka.router.configuration.kafka;

import com.catalina.omni.kafka.router.impl.commands.CreateKafkaStreamsCommand;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.message.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

public final class KafkaStreamsUtils {

    public static Optional<KafkaStream<Message>> retrieveKafkaStream(String topicName,
                                                                           ConsumerConnector consumerConnector) {

        return Optional.ofNullable(new CreateKafkaStreamsCommand(consumerConnector,
                Collections.singletonList(topicName), 1500000)
                .execute())
                .map(map -> map.get(topicName))
                .map(Collection::stream)
                .flatMap(Stream::findFirst);
    }
}
