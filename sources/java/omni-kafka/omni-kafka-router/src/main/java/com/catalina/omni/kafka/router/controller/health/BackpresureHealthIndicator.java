package com.catalina.omni.kafka.router.controller.health;

import com.catalina.omni.kafka.router.impl.ForwardingProcessor;
import com.catalina.omni.kafka.router.service.NotificationService;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

@Component
public class BackpresureHealthIndicator extends AbstractHealthIndicator {

    private final int pauseThreshold;
    private final List<ForwardingProcessor> processors;
    private final NotificationService notificationService;

    public BackpresureHealthIndicator(@Value("${events.backpressure.folder}") String backpressureFolder,
                                      @Value("${events.backpressure.pause.threshold}") int pauseThreshold,
                                      List<ForwardingProcessor> processors,
                                      NotificationService notificationService) {
        this.notificationService = notificationService;
        this.pauseThreshold = pauseThreshold;
        this.processors = processors;

        processors.forEach(processor -> {
            try {
                Files.createDirectories(processor.getBackpressurePath());
                Files.createDirectories(processor.getInvalidPath());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        });
    }

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        final List<BackpressureHealth> processorHealths = processors.stream()
                .map(processor -> {
                    final String processorName = processor.getProcessorName();
                    try {
                        final long count = Files.list(processor.getBackpressurePath()).count();
                        final long invalidCount = Files.list(processor.getInvalidPath()).count();

                        if (count > 0) {
                            notificationService.sendMessage("backpressured.message.detected", count, processorName);
                        }

                        if (invalidCount > 0) {
                            notificationService.sendMessage("invalid.message.detected", invalidCount, processorName);
                        }

                        return BackpressureHealth.builder()
                                .count(count)
                                .invalid(invalidCount)
                                .forwardingQueue(processor.getProcessingSemaphore().getQueueLength())
                                .forwardingAvailablePermits(processor.getProcessingSemaphore().availablePermits())
                                .forwardingRate(processor.getProcessingRateLimiter().getRate())
                                .backpressureQueue(processor.getBackpressureSemaphore().getQueueLength())
                                .backpressureAvailablePermits(processor.getBackpressureSemaphore().availablePermits())
                                .backpressureRate(processor.getBackpressureRateLimiter().getRate())
                                .processorName(processorName).build();
                    } catch (IOException e) {
                        throw new IllegalStateException(e);
                    }
                }).collect(Collectors.toList());

        if (processorHealths.stream().anyMatch(backpressureHealth -> backpressureHealth.count() > pauseThreshold)) {
            builder.down();
        } else {
            builder.up();
        }

        builder.withDetails(processorHealths.stream()
                .collect(toMap(BackpressureHealth::processorName, o -> "Backpressured count:" + o.count() + "," +
                        "Forwarding queue:" + o.forwardingQueue() + "," +
                        "Forwarding available permits:" + o.forwardingAvailablePermits() + "," +
                        "Forwarding rate per second:" + Math.round(o.forwardingRate()) + "," +
                        "Backpressured queue:" + o.backpressureQueue() + "," +
                        "Backpressured available permits:" + o.backpressureAvailablePermits() + "," +
                        "Backpressured rate per second:" + Math.round(o.backpressureRate()))));
    }

    @Builder
    @Getter
    @Accessors(fluent = true)
    private static class BackpressureHealth {
        private long count;
        private String processorName;
        private long forwardingQueue;
        private long forwardingAvailablePermits;
        private double forwardingRate;
        private long backpressureQueue;
        private long backpressureAvailablePermits;
        private double backpressureRate;
        private long invalid;
    }
}
