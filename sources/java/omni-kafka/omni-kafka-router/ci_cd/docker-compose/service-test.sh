#!/bin/bash -xe

ENVIRONMENT=$1


case "$ENVIRONMENT" in
        DEV|dev)
            SERVICE_URL="omni-mfd-sqa-node-02.eastus.cloudapp.azure.com:11221"
            ;;
        SQA|sqa)
            SERVICE_URL="10.166.45.68:11221"
            ;;
        PROD|prod)
            SERVICE_URL="10.176.45.68:11221"
            ;;
        *)
            echo "Unknown environment to be checked: ${ENVIRONMENT}"
            exit 1
esac

function checkURL () {
    local URL=$1
    curl --silent --show-error --fail ${URL}
}

function checkURLs (){
    checkURL "http://${SERVICE_URL}/actuator/health"
}


function retryChecks (){
    MAX_RETRY=10
    for RETRY in $(seq 1 $MAX_RETRY)
    do
        echo "Checking services. Retry num: ${RETRY}"
        if checkURLs; then
            return 0
        fi
        sleep 60
    done

    echo "I have tried ${MAX_RETRY} times, but service checks are still not passing, hence exiting with error"
    return -1
}

retryChecks





