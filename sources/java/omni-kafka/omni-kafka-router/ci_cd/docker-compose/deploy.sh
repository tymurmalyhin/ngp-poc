#!/bin/bash -xe
#this script expects to be executed on swarm manager machine
BASE_NAME="omni-kafka-router"
ENVIRONMENT=$1
IMAGE_TAG=$2
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
STACK_NAME=${BASE_NAME}-${ENVIRONMENT}
export IMAGE_TAG
export STACK_NAME


function listDockerService () {
    docker stack list
    docker service list
}

function removeOldService() {
    #remove the current deployment
    docker stack remove ${STACK_NAME} || true

    # A workaround for https://github.com/moby/moby/issues/30942
    NETWORK_NAME="${STACK_NAME}_default"
    while [ "$(docker network ls -f NAME=${NETWORK_NAME} -q)" ]; do
      sleep 1;
    done
}

function deployNewService() {
    #deploy to the specific environment
    docker stack deploy --compose-file docker-compose-stack-${ENVIRONMENT}.yml ${STACK_NAME}
}


function init() {

    case "$ENVIRONMENT" in
        DEV|dev)
            #if doing deployment to DEV and there is no existing swarm, create one
            docker swarm init || echo "Swarm already exists"
            docker load -i ${BASE_NAME}-${IMAGE_TAG}.tar.gz
            rm ${BASE_NAME}-${IMAGE_TAG}.tar.gz
    esac

}

init
listDockerService
removeOldService
deployNewService
listDockerService
sleep 30
listDockerService
$SCRIPT_DIR/service-test.sh ${ENVIRONMENT}
listDockerService