#!/bin/bash -xe

BASE_NAME="omni-attr-service"
ENVIRONMENT=$1
IMAGE_TAG=$2
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

case "$ENVIRONMENT" in
        DEV|dev)
            SWARM_MANAGER_HOST="omni-mfd@omni-mfd-sqa-node-02.eastus.cloudapp.azure.com"
            docker save csulliva/${BASE_NAME}:${IMAGE_TAG} > ${SCRIPT_DIR}/${BASE_NAME}-${IMAGE_TAG}.tar.gz
            ;;
        SQA|sqa)
            SWARM_MANAGER_HOST="vmadmin@10.166.45.133"
            ;;
        PROD|prod)
            SWARM_MANAGER_HOST="vmadmin@10.176.45.134"
            ;;
        *)
            echo "Unknown environment: ${ENVIRONMENT}"
            exit 1
esac

rsync -ravz -e "ssh -o StrictHostKeyChecking=no" --progress --delete ${SCRIPT_DIR}/ ${SWARM_MANAGER_HOST}:~/${BASE_NAME}_deploy/
ssh ${SWARM_MANAGER_HOST} "cd ~/${BASE_NAME}_deploy/; ./deploy.sh ${ENVIRONMENT} ${IMAGE_TAG}"