#!/usr/bin/env groovy

SCM_BRANCH = SCM_BRANCH.minus("origin/")
IMAGE_TAG = SCM_BRANCH.replaceAll('/', '_')
IMAGE_NAME = "csulliva/omni-kafka-router"
MAVEN_OPTS = binding.hasVariable('MAVEN_OPTS') ? MAVEN_OPTS.replaceAll(',', ' ') : ''
BUILD_PATH = "."
PROJECT_PATH = "./omni-kafka-router"

print("SCM_BRANCH: ${SCM_BRANCH}")
print("BUILD_DEPENDENCIES: ${BUILD_DEPENDENCIES}")
print("DEPLOY_ENVS: ${DEPLOY_ENVS}")
print("MAVEN_OPTS: ${MAVEN_OPTS}")


node {

    stage('Build dependencies') {
        if (BUILD_DEPENDENCIES == 'true' || SCM_BRANCH == 'master') {
            print("Building project dependencies with ${SCM_BRANCH} branch. This doesn't guarantee that maven dependencies will be satisfied")
            build job: 'OM-Core', parameters: [[$class: 'GitParameterValue', name: 'SCM_BRANCH', value: "origin/" + SCM_BRANCH]]
        } else {
            print("Skipping build of dependencies")
        }
    }

    stage('Clone sources') {
        git(
                url: 'git@bitbucket.org:cameosaas/omni-kafka.git',
                credentialsId: 'bitbucket-id',
                branch: SCM_BRANCH
        )
    }

    stage('Build Jar File') {
        withMaven(maven: 'M3', jdk: 'jdk8') {
            sh("mvn ${MAVEN_OPTS} -f ${BUILD_PATH}/pom.xml clean install")
        }
    }

    stage("Build Container") {

        def customImage = docker.build(IMAGE_NAME, "-f ${PROJECT_PATH}/ci_cd/docker-image/Dockerfile ${PROJECT_PATH}/")
        customImage.push(IMAGE_TAG)
        if (SCM_BRANCH == 'master') {
            customImage.push("latest")
        }
        docker.withRegistry('https://registry.hub.docker.com', 'docker-hub-credentials') {
            customImage.push(IMAGE_TAG)
            if (SCM_BRANCH == 'master') {
                customImage.push("latest")
            }
        }
    }


    ["DEV", "SQA", "PROD"].each { envi ->
        stage("Deploy to " + envi) {
            if (DEPLOY_ENVS.contains(envi)) {
                println('Deploying to: ' + envi)
                sh "${PROJECT_PATH}/ci_cd/docker-compose/runDeployRemotely.sh ${envi.toLowerCase()} ${IMAGE_TAG}"
                wrap([$class: 'BuildUser']) {
                    slackSend(channel: "C7CTCECSC", color: "GREEN", message: "Build: ${IMAGE_NAME} - branch ${SCM_BRANCH} deployed to ${envi} by ${BUILD_USER}")
                }
            } else {
                println('Skipping deployment to: ' + envi)
            }
        }
    }


}

