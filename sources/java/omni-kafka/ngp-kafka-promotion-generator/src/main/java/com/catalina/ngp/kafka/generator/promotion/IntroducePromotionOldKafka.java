package com.catalina.ngp.kafka.generator.promotion;

import com.catalina.ngp.kafka.serialization.NgpMessageSerializer;
import com.catalina.platform.api.models.advertiser.Promotion;
import com.catalina.platform.api.models.publisher.Experience;
import com.catalina.platform.avro.Event;
import com.catalina.platform.avro.Order;
import com.catalina.platform.avro.serialization.EventSerializer;
import com.catalina.platform.avro.serialization.OrderSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import kafka.javaapi.producer.Producer;
import kafka.javaapi.producer.ProducerData;
import kafka.message.Message;
import kafka.producer.ProducerConfig;
import kafka.serializer.Encoder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.UUID;

public class IntroducePromotionOldKafka {

    /*
         args[0] - zookeeper
         args[1] - topicname
         args[2] - groupid
         args[3] - count of messages

     */
    public static void main(String[] args) {

        int count = 1000;
        if (args[3] != null && !args[3].isEmpty())
            count = Integer.parseInt(args[3]);

        Properties inputProps = new Properties();
        inputProps.put("zk.connect", args[0]);
        inputProps.put("groupid", args[2]);
        inputProps.put("enable.auto.commit", "true");
        inputProps.put("auto.commit.interval.ms", "1000");
        inputProps.put("zk.sessiontimeout.ms", "1000000");
        inputProps.put("serializer.class", EventEncoder.class.getName());

        final Producer producer = new Producer(new ProducerConfig(inputProps));

        for (int i = 0; i < count; i++) {
            Promotion.Builder builder = Promotion.newBuilder(new Experience(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
            builder.withAdgroupId(UUID.randomUUID().toString())
                    .withCountryCode(UUID.randomUUID().toString())
                    .withExternalId(UUID.randomUUID().toString())
                    .withCampaignName(UUID.randomUUID().toString())
            .withId(UUID.randomUUID().toString());
            Promotion promotion = builder.build();
            producer.send(new ProducerData(args[1], promotion));
        }
    }

    public static class EventEncoder implements Encoder<Object> {

        private final ObjectWriter promotionWriter;

        public EventEncoder() {
            ObjectMapper mapper = new ObjectMapper()
                    .setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"))
                    .setSerializationInclusion(JsonInclude.Include.NON_NULL);
            promotionWriter = mapper.writerFor(Promotion.class);
        }

        @Override
        public Message toMessage(Object o) {
            try {
                if (o instanceof Order) {
                    return new Message(OrderSerializer.toBytes((Order) o));
                }

                if (o instanceof Event) {
                    return new Message(EventSerializer.toBytes((Event) o));
                }

                if (o instanceof Promotion) {
                    return NgpMessageSerializer.fromBytes(promotionWriter.writeValueAsBytes(o));
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            throw new IllegalArgumentException("No serializer for " + o.getClass());
        }
    }

}
