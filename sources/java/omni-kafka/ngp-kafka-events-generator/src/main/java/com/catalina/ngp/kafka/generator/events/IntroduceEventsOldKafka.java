package com.catalina.ngp.kafka.generator.events;

import com.catalina.platform.api.models.advertiser.Promotion;
import com.catalina.platform.avro.ChannelType;
import com.catalina.platform.avro.Event;
import com.catalina.platform.avro.EventType;
import com.catalina.platform.avro.serialization.EventSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import kafka.javaapi.producer.Producer;
import kafka.javaapi.producer.ProducerData;
import kafka.message.Message;
import kafka.producer.ProducerConfig;
import kafka.serializer.Encoder;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.UUID;

public class IntroduceEventsOldKafka {

    /*
         args[0] - zookeeper
         args[1] - topicname
         args[2] - groupid
         args[3] - count of messages
         args[4] - count of thread
     */
    public static void main(String[] args) {

        int count = 1000;
        int countOfThread = 1;

        if (args[3] != null && !args[3].isEmpty())
            count = Integer.parseInt(args[3]);

        if (args[4] != null && !args[4].isEmpty())
            countOfThread = Integer.parseInt(args[4]);

        for (int i = 0; i < countOfThread; i++) {
            Runnable r = new GeneratorThread(args[0], args[2], args[1], count);
            new Thread(r).start();
        }

    }


    public static class GeneratorThread implements Runnable {

        Producer producer;
        String topicName;
        int countOfMessages;

        public GeneratorThread(String zkConnection, String groupId, String topicName, int countOfMessages) {
            Properties inputProps = new Properties();
            inputProps.put("zk.connect", zkConnection);
            inputProps.put("groupid", groupId);
            inputProps.put("enable.auto.commit", "true");
            inputProps.put("auto.commit.interval.ms", "1000");
            inputProps.put("zk.sessiontimeout.ms", "1000000");
            inputProps.put("serializer.class", EventEncoder.class.getName());
            producer = new Producer(new ProducerConfig(inputProps));
            this.topicName = topicName;
            this.countOfMessages = countOfMessages;
        }


        @Override
        public void run() {
            for (int i = 0; i < countOfMessages; i++) {
                Event event = new Event();
                event.setId(UUID.randomUUID().toString());
                event.setCampaignName(UUID.randomUUID().toString());
                // to-do should be random
                event.setType(EventType.impression);
                event.setChannelType(ChannelType.store);
                producer.send(new ProducerData(topicName, event));
            }
        }
    }


    public static class EventEncoder implements Encoder<Object> {

        private final ObjectWriter promotionWriter;

        public EventEncoder() {
            ObjectMapper mapper = new ObjectMapper()
                    .setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"))
                    .setSerializationInclusion(JsonInclude.Include.NON_NULL);
            promotionWriter = mapper.writerFor(Promotion.class);
        }

        @Override
        public Message toMessage(Object o) {
            try {
                if (o instanceof Event) {
                    return new Message(EventSerializer.toBytes((Event) o));
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            throw new IllegalArgumentException("No serializer for " + o.getClass());
        }
    }


}
