package com.catalina.ngp.kafka.serialization;

import com.catalina.ngp.kafka.message.ngp.NgpMessage;

//@Slf4j
public class NgpMessageSerializer {

    public static byte[] toBytes(NgpMessage message) {
        byte[] b = new byte[message.payload().remaining()];
        message.payload().get(b);
        return b;
    }

    public static NgpMessage fromBytes(byte[] bytes) {
        return new NgpMessage(bytes);
    }
}



