package com.catalina.ngp.kafka.eventhub.generator;

import com.catalina.platform.avro.ChannelType;
import com.catalina.platform.avro.Event;
import com.catalina.platform.avro.EventType;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;
import java.util.UUID;

public class IntroduceEventsEventHubKafka {
    /*
         args[0] - eventhub connection string
         args[1] - saslJaasConfig
         args[2] - topicname
         args[3] - count of messages
         args[4] - count of thread
     */
    public static void main(String[] args) {
        if (args.length != 0) {
            int count = 1000;
            int countOfThread = 1;

            if (args[3] != null && !args[3].isEmpty())
                count = Integer.parseInt(args[3]);

            if (args[4] != null && !args[4].isEmpty())
                countOfThread = Integer.parseInt(args[4]);

            for (int i = 0; i < countOfThread; i++) {
                Runnable r = new GeneratorThread(args[0], args[2], args[1], count);
                new Thread(r).start();
            }
        } else {
            System.out.println("[args[0]] - eventhub connection string");
            System.out.println("{args[1]] - saslJaasConfig");
            System.out.println("[args[2]] - topicname");
            System.out.println("<args[3]> - count of messages. default value is 1000");
            System.out.println("<args[4]> - count of thread. default value is 1");
        }
    }


    public static class GeneratorThread implements Runnable {

        KafkaProducer producer;
        String topicName;
        int countOfMessages;

        public GeneratorThread(String server, String topicName, String saslJaasConfig, int countOfMessages) {
            Properties props = new Properties();
            this.topicName = topicName;
            this.countOfMessages = countOfMessages;

String qq = "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"$ConnectionString\" password=\"Endpoint=sb://nextgen-np-eventhubs.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=aBSbH4eZ+dRAJSd4yfD6YuxMofqAIZgvmuwjBZxvoqg=\";";



            props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, server);
            props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.LongSerializer");
            props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "com.catalina.ngp.kafka.eventhub.generator.MessageSerializer");

            props.put(ProducerConfig.MAX_REQUEST_SIZE_CONFIG, String.valueOf(4 * 1024 * 1024));
            props.put("security.protocol", "SASL_SSL");
            props.put("sasl.mechanism", "PLAIN");
//            props.put("sasl.jaas.config", saslJaasConfig);
            props.put("sasl.jaas.config", qq);

            producer = new KafkaProducer(props);

        }

        @Override
        public void run() {
            for (int i = 0; i < countOfMessages; i++) {
                Event event = new Event();
                event.setId(UUID.randomUUID().toString());
                event.setCampaignName(UUID.randomUUID().toString());
                // to-do should be random
                event.setType(EventType.impression);
                event.setChannelType(ChannelType.store);
                producer.send(new ProducerRecord(topicName, event));
            }
        }
    }

}
