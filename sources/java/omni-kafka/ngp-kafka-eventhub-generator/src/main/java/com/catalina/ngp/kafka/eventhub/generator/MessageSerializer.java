package com.catalina.ngp.kafka.eventhub.generator;

import com.catalina.platform.avro.Event;
import com.catalina.platform.avro.serialization.EventSerializer;
import org.apache.kafka.common.serialization.Serializer;

import java.io.IOException;
import java.util.Map;

public class MessageSerializer<T> implements Serializer<T> {

    public MessageSerializer() {
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public byte[] serialize(String s, T o) {
        if (o instanceof Event) {
            try {
                return EventSerializer.toBytes((Event) o);
            } catch (IOException e) {
                throw new IllegalStateException(e);            }
        }
        throw new IllegalArgumentException("No serializer found for " + o.getClass());
    }

    @Override
    public void close() {

    }

}
