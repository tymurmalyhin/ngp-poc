package com.catalina.ngp.kafka.generator;



public class MyCustomProducer extends Producer {


  public MyCustomProducer(String topic) {
       super(topic);
  }


  public static void main(String[] args) {
    String topic = args[0];
    MyCustomProducer producer = new MyCustomProducer(topic);
    producer.run();
  }

}
