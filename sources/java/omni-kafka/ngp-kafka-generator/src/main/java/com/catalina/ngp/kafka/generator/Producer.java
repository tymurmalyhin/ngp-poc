package com.catalina.ngp.kafka.generator;


import com.catalina.ngp.kafka.configuration.KafkaProperties;
import com.catalina.ngp.kafka.message.ngp.NgpMessage;

import java.util.Properties;

import com.catalina.ngp.kafka.serialization.NgpMessageSerializer;
import kafka.producer.ProducerConfig;
import kafka.javaapi.producer.ProducerData;


// import kafka.javaapi.

public class Producer extends Thread {

    private final kafka.javaapi.producer.Producer<Long, String> producer;
    private final String topic;
    private final Properties props = new Properties();

    public Producer(String topic) {

        props.put("serializer.class", KafkaProperties.serializerClass);
        props.put("broker.list", "0:" + KafkaProperties.brokerList + ":" + KafkaProperties.brokerPort);


        // Use random partitioner. Don't need the key type. Just set it to Integer.
        // The message is of type String.
        ProducerConfig config = new ProducerConfig(props);

        producer = new kafka.javaapi.producer.Producer<Long, String>(config);
        this.topic = topic;
    }


    public void run() {
        int messageNo = 1;
        while (true) {
/*
            NgpMessage msg = new NgpMessage("Message_" + topic + "_" + messageNo);
            String jsonMsg = NgpMessageSerializer.toJson(msg);
            producer.send(new ProducerData<Long, String>(topic, jsonMsg));
*/
            messageNo++;
            if (messageNo == 1000) {
                break;
            }
        }
    }


}
