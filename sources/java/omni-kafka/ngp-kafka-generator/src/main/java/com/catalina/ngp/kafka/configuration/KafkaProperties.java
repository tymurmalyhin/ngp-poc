package com.catalina.ngp.kafka.configuration;


public interface KafkaProperties {


    final static String topic = "ngp-test-poc";

/*
 for producer

 */


    final static String zkConnect = "vm-eus-temp-zoo.eastus.cloudapp.azure.com:2181";
    final static String groupId = "group2";


//    final static String brokerList = "40.117.195.1";
    final static String brokerList = "localhost";
//    final static String brokerList = "vm-eus-temp-kafka072.eastus.cloudapp.azure.com";

    final static int brokerPort = 9092;
//    final static String brokerList = "172.28.0.3";

//     final static String serializerClass = "com.catalina.ngp.kafka.serialization.NgpMessageEncoder";

      final static String serializerClass = "kafka.serializer.StringEncoder";



// bin/kafka-console-consumer.sh --zookeeper zookeeper:2181 --topic ngp-test-poc  --from-beginning



}
