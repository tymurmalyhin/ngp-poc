
var omnimfd = db.getSiblingDB('omnimfd')

function MongoIndexHolder() {
    this.collectionMap = new Map()
    this.keys = []
}

function IndexRequest(collectionName) {
    this.indexes = []
    this.collectionName = collectionName
}

MongoIndexHolder.prototype.getCollection = function(name) {
    if (this.collectionMap.get(name) !== null) {
        return this.collectionMap.get(name)
    }
    var indexRequest = new IndexRequest(name)
    this.keys.push(name)
    this.collectionMap.put(name, indexRequest)
    return indexRequest
}

IndexRequest.prototype.dropIndex = function(indexName) {
    print('Dropping index ' + indexName + ' from collection' + this.collectionName)
    printjson(omnimfd.getCollection(this.collectionName).dropIndex(indexName))
}


var omnimfdMongoDb = new MongoIndexHolder()

omnimfdMongoDb.getCollection('omniUserOfferV2').dropIndex('_ts')

omnimfdMongoDb.getCollection('omniUserOfferV2').dropIndex('countryCode_1')
omnimfdMongoDb.getCollection('omniUserOfferV2').dropIndex('omniOffer.endDate_1')


omnimfdMongoDb.getCollection('omniOffer').dropIndex('networkId_1')
omnimfdMongoDb.getCollection('omniOffer').dropIndex('state_1')
omnimfdMongoDb.getCollection('omniOffer').dropIndex('startDate_1')
omnimfdMongoDb.getCollection('omniOffer').dropIndex('endDate_1')

omnimfdMongoDb.getCollection('omniOfferTargeted').dropIndex('networkId_1')
omnimfdMongoDb.getCollection('omniOfferTargeted').dropIndex('state_1')
omnimfdMongoDb.getCollection('omniOfferTargeted').dropIndex('startDate_1')
omnimfdMongoDb.getCollection('omniOfferTargeted').dropIndex('endDate_1')

omnimfdMongoDb.getCollection('omniUser').dropIndex('omni_user_idx_1')
omnimfdMongoDb.getCollection('omniUser').dropIndex('countryCode_1')
omnimfdMongoDb.getCollection('omniUser').dropIndex('digitalId_1')
omnimfdMongoDb.getCollection('omniUser').dropIndex('fscId_1')
omnimfdMongoDb.getCollection('omniUser').dropIndex('networkId_1')

omnimfdMongoDb.getCollection('omniUserOfferClip').dropIndex('omniOfferId_1')
omnimfdMongoDb.getCollection('omniUserOfferClip').dropIndex('cid_1')
omnimfdMongoDb.getCollection('omniUserOfferClip').dropIndex('clipIngestionToVault_1')
omnimfdMongoDb.getCollection('omniUserOfferClip').dropIndex('state_1')
omnimfdMongoDb.getCollection('omniUserOfferClip').dropIndex('fscIds_1')

omnimfdMongoDb.getCollection('omniUserOfferClip').dropIndex('omniOfferId_1')
omnimfdMongoDb.getCollection('omniUserOfferClip').dropIndex('cid_1')
omnimfdMongoDb.getCollection('omniUserOfferClip').dropIndex('clipIngestionToVault_1')
omnimfdMongoDb.getCollection('omniUserOfferClip').dropIndex('state_1')
omnimfdMongoDb.getCollection('omniUserOfferClip').dropIndex('fscIds_1')


omnimfdMongoDb.getCollection('omniUserOfferV2').dropIndex('countryCode_1')
omnimfdMongoDb.getCollection('omniUserOfferV2').dropIndex('networkId_1')
omnimfdMongoDb.getCollection('omniUserOfferV2').dropIndex('digitalId_1')
omnimfdMongoDb.getCollection('omniUserOfferV2').dropIndex('fscId_1')
omnimfdMongoDb.getCollection('omniUserOfferV2').dropIndex('omniOffer.state_1')
omnimfdMongoDb.getCollection('omniUserOfferV2').dropIndex('omniOffer.startDate_1')
omnimfdMongoDb.getCollection('omniUserOfferV2').dropIndex('omniOffer.endDate_1')
omnimfdMongoDb.getCollection('omniUserOfferV2').dropIndex('omniOffer.networkId_1')
omnimfdMongoDb.getCollection('omniUserOfferV2').dropIndex('omniOffer.offerType_1')

omnimfdMongoDb.getCollection('store').dropIndex('location_1')
omnimfdMongoDb.getCollection('store').dropIndex('updated_1')

omnimfdMongoDb.getCollection('storeDetail').dropIndex('location_1')

omnimfdMongoDb.getCollection('touchpointToken').dropIndex('country_1')
omnimfdMongoDb.getCollection('touchpointToken').dropIndex('networkId_1')
