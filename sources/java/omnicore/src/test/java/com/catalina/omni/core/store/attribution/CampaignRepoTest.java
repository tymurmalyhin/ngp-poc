package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.MongoTestConfig;
import com.catalina.omni.core.domain.omniattribution.Campaign;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigurationPackage
@ContextConfiguration(classes = MongoTestConfig.class)
@DirtiesContext
public class CampaignRepoTest {

    @Autowired
    private CampaignRepo campaignRepo;

    @Before
    public void init() {
        createCampaign("1",
                LocalDateTime.of(2019, 1, 1, 0, 0, 0),
                LocalDateTime.of(2019, 1, 30, 0, 0, 0));

        createCampaign("2",
                LocalDateTime.of(2019, 1, 15, 0, 0, 0),
                LocalDateTime.of(2019, 1, 30, 0, 0, 0));

        createCampaign("3",
                LocalDateTime.of(2019, 1, 14, 0, 0, 0),
                LocalDateTime.of(2019, 1, 19, 0, 0, 0));
    }

    @Test
    public void testGetCampainsActiveOnCertainDate() {
        final LocalDateTime thirteenth = LocalDateTime.of(2019, 1, 13, 0, 0, 0);
        final List<Campaign> foundOnThirteenth = campaignRepo.findByStartDateLessThanEqualAndEndDateGreaterThanEqual(thirteenth, thirteenth);
        assertThat(foundOnThirteenth, contains(hasProperty("id", is("1"))));
        assertThat(foundOnThirteenth, hasSize(1));

        final LocalDateTime sixteenth = LocalDateTime.of(2019, 1, 16, 0, 0, 0);
        final List<Campaign> foundOnSixteenth = campaignRepo.findByStartDateLessThanEqualAndEndDateGreaterThanEqual(sixteenth, sixteenth);
        assertThat(foundOnSixteenth, containsInAnyOrder(
                hasProperty("id", is("1")),
                hasProperty("id", is("2")),
                hasProperty("id", is("3"))
        ));
        assertThat(foundOnSixteenth, hasSize(3));

        final LocalDateTime twentieth = LocalDateTime.of(2019, 1, 20, 0, 0, 0);
        final List<Campaign> foundOnTwentieth = campaignRepo.findByStartDateLessThanEqualAndEndDateGreaterThanEqual(twentieth, twentieth);
        assertThat(foundOnTwentieth, containsInAnyOrder(
                hasProperty("id", is("1")),
                hasProperty("id", is("2"))));
        assertThat(foundOnTwentieth, hasSize(2));
    }

    private void createCampaign(String id, LocalDateTime startDate, LocalDateTime endDate) {
        final Campaign campaign = new Campaign();
        campaign.setId(id);
        campaign.setStartDate(startDate);
        campaign.setEndDate(endDate);
        campaignRepo.save(campaign);
    }
}
