package com.catalina.omni.core;

import com.catalina.omni.core.configuration.javers.AspectEnablingConfiguration;
import com.catalina.omni.core.configuration.javers.JaversAttributionConfiguration;
import com.catalina.omni.core.configuration.javers.JaversMfdConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Import(value = {EmbeddedMongoAutoConfiguration.class,
        JaversMfdConfiguration.class, JaversAttributionConfiguration.class,
        AspectEnablingConfiguration.class})
@PropertySource("classpath:application-test.properties")
public class MongoTestConfig {

}
