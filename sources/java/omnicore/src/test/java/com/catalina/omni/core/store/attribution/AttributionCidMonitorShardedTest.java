package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.MongoTestConfig;
import com.catalina.omni.core.domain.omniattribution.AttributionCIDMonitor;
import com.catalina.omni.core.domain.omniattribution.LineItemImpressionEvent;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DirtiesContext
//@ContextConfiguration(classes = {ShardedMongoConfig.class, MongoTestConfig.class})
@ContextConfiguration(classes = { MongoTestConfig.class})
public class AttributionCidMonitorShardedTest {

    @Autowired
    private AttributionCIDMonitorRepo cidMonitorRepo;

    @Autowired
    @Qualifier("attributionClient")
    private MongoClient shardedClient;

    @Before
    public void init() {
        //final MongoDatabase adminDb = this.shardedClient.getDatabase("admin");
        //shardCollection(adminDb, "omniattribution", AttributionCIDMonitor.class.getAnnotation(org.springframework.data.mongodb.core.mapping.Document.class).collection());
        cidMonitorRepo.findAll().forEach(attributionCIDMonitor -> cidMonitorRepo.delete(attributionCIDMonitor));
    }

    @Test
    public void testCreateOnShardedCollection() {
        final AttributionCIDMonitor cidMonitor = new AttributionCIDMonitor();
        cidMonitor.setCampaignId("XY");
        cidMonitor.setCid("USA-0099-AAAAAA");
        cidMonitor.setEndDate(LocalDateTime.now().plusDays(4));

        final LineItemImpressionEvent event = lineItemEvent("creative", "lineItem", "tealium", "event", LocalDateTime.now());

        final AttributionCIDMonitor match = cidMonitorRepo.createOrUpdateCidMonitor(cidMonitor.getCid(),
                cidMonitor.getCampaignId(),
                cidMonitor.getEndDate(),
                event);

        assertEquals(cidMonitor.getCid(), match.getCid());
        assertEquals(cidMonitor.getCampaignId(), match.getCampaignId());
        assertEquals(cidMonitor.getEndDate(), match.getEndDate());
        assertEquals(1, cidMonitorRepo.count());
        assertEquals(1, match.getLineItems().size());
        assertEquals(event, match.getLineItems().get(0));
    }

    @Test
    public void testMergeOnShardedCollection() {
        final LocalDateTime date = LocalDateTime.now();

        final LineItemImpressionEvent eventOne = lineItemEvent("creative", "lineItem1", "tealium", "event", date);
        final LineItemImpressionEvent eventTwo = lineItemEvent("creative", "lineItem2", "tealium", "event", date);
        final LineItemImpressionEvent eventThree = lineItemEvent("creative", "lineItem3", "tealium", "event", date);

        final AttributionCIDMonitor cidMonitor = createMonitor(eventOne, "XY", "USA-0099-AAAAAA", LocalDateTime.now().plusDays(4));

        cidMonitorRepo.insert(cidMonitor);

        cidMonitor.setId(null);
        cidMonitor.setLineItems(Collections.singletonList(eventTwo));
        cidMonitorRepo.insert(cidMonitor);

        final AttributionCIDMonitor match = cidMonitorRepo.createOrUpdateCidMonitor(cidMonitor.getCid(),
                cidMonitor.getCampaignId(),
                cidMonitor.getEndDate(),
                eventThree);

        assertEquals(cidMonitor.getCid(), match.getCid());
        assertEquals(cidMonitor.getCampaignId(), match.getCampaignId());
        assertEquals(cidMonitor.getEndDate(), match.getEndDate());
        assertEquals(1, cidMonitorRepo.count());
        assertEquals(3, match.getLineItems().size());
        assertTrue(match.getLineItems().containsAll(Arrays.asList(eventOne, eventTwo, eventThree)));
    }

    @Test
    @Repeat(5)//to ensure consistency
    public void testConcurentInsert() {
        final LocalDateTime date = LocalDateTime.now();

        final LineItemImpressionEvent eventOne = lineItemEvent("creative", "lineItem1", "tealium", "event", date);
        final LineItemImpressionEvent eventTwo = lineItemEvent("creative", "lineItem2", "tealium", "event", date);
        final LineItemImpressionEvent eventThree = lineItemEvent("creative", "lineItem3", "tealium", "event", date);
        final LineItemImpressionEvent eventFour = lineItemEvent("creative", "lineItem4", "tealium", "event", date);

        final CompletableFuture[] saves = Stream.of(eventOne, eventTwo, eventThree)
                .map(lineItem -> CompletableFuture.runAsync(() ->
                        cidMonitorRepo.createOrUpdateCidMonitor("USA-0099-AAAAAA", "XY", date, lineItem)))
                .toArray(CompletableFuture[]::new);

        CompletableFuture.allOf(saves).join();
        //to trigger aggregation part for sure
        cidMonitorRepo.createOrUpdateCidMonitor("USA-0099-AAAAAA", "XY", date, eventFour);

        assertEquals(1, cidMonitorRepo.count());

        final AttributionCIDMonitor record = cidMonitorRepo.findAll().get(0);
        assertEquals("USA-0099-AAAAAA", record.getCid());
        assertEquals("XY", record.getCampaignId());
        assertEquals(date, record.getEndDate());

        final List<LineItemImpressionEvent> lineItems = record.getLineItems();
        assertEquals(4, lineItems.size());
        assertTrue(lineItems.containsAll(Stream.of(eventOne, eventTwo, eventThree, eventFour).collect(Collectors.toList())));
    }


    private AttributionCIDMonitor createMonitor(LineItemImpressionEvent eventOne, String campaignId, String cid, LocalDateTime endDate) {
        final AttributionCIDMonitor cidMonitor = new AttributionCIDMonitor();
        cidMonitor.setCampaignId(campaignId);
        cidMonitor.setCid(cid);
        cidMonitor.setEndDate(endDate);
        cidMonitor.setLineItems(Collections.singletonList(eventOne));
        return cidMonitor;
    }

    private LineItemImpressionEvent lineItemEvent(String creative, String lineItem, String tealium, String id, LocalDateTime date) {
        final LineItemImpressionEvent event = new LineItemImpressionEvent();
        event.setCreativeId(creative);
        event.setImpressionDate(date);
        event.setLineItemId(lineItem);
        event.setTealiumEventId(tealium);
        event.setEventId(id);
        return event;
    }

    private void shardCollection(MongoDatabase adminDb, final String dbName, final String collectionName) {
        final BasicDBObject shardKey = new BasicDBObject("_id", "hashed");

        final BasicDBObject cmd = new BasicDBObject("shardCollection", dbName + "." + collectionName);
        cmd.put("key", shardKey);

        adminDb.runCommand(cmd);
    }
}
