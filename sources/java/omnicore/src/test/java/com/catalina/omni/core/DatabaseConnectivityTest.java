package com.catalina.omni.core;

import com.catalina.omni.core.domain.integration.OmniOffer;
import com.catalina.omni.core.domain.omniattribution.Attribution;
import com.google.common.base.CaseFormat;
import com.google.common.base.Strings;
import com.mongodb.MongoClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.util.TypeInformation;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@AutoConfigurationPackage
@ContextConfiguration(classes = MongoTestConfig.class)
@DirtiesContext
public class DatabaseConnectivityTest {

  private static final Logger LOG = LoggerFactory.getLogger(DatabaseConnectivityTest.class);

  private static final String MFD_PACKAGE = OmniOffer.class.getPackage().getName();
  private static final String ATTR_PACKAGE = Attribution.class.getPackage().getName();

  @Autowired
  @Qualifier("mfdClient")
  private MongoClient mfdClient;

  @Autowired
  @Qualifier("attributionClient")
  private MongoClient attributionClient;

  @Autowired
  @Qualifier("mfdMapping")
  private MongoMappingContext mfdCtx;

  @Autowired
  @Qualifier("attributionMapping")
  private MongoMappingContext attributionCtx;

  @Value("${mongodb.omnimfd.name}")
  private String mfdDb;

  @Value("${mongodb.omniattribution.name}")
  private String attrDb;

  private Map<String, String> mfdNameToPackageIndex;
  private Map<String, String> attributionNameToPackageIndex;

  @Before
  public void init() {
    mfdNameToPackageIndex = mfdCtx.getManagedTypes().stream()
        .map(TypeInformation::getType)
        .filter(isDocument())
        .collect(Collectors.toMap(
            DatabaseConnectivityTest::getCustomCollectionNameOrTypeName, getTypePackage()));

    attributionNameToPackageIndex = attributionCtx.getManagedTypes().stream()
        .map(TypeInformation::getType)
        .filter(isDocument())
        .collect(Collectors.toMap(
            DatabaseConnectivityTest::getCustomCollectionNameOrTypeName, getTypePackage()));
  }

  @Test
  public void testConnectivity() {
    LOG.info("Verifying db {}", mfdDb);
    mfdClient.getDatabase(mfdDb).listCollectionNames()
        .forEach((Consumer<? super String>) collectionName -> {
          if (!mfdNameToPackageIndex.containsKey(collectionName)) {
            return;
          }
          assertEquals(
              format("Package for collection %s is expected to be in %s", collectionName,
                  MFD_PACKAGE),
              MFD_PACKAGE, mfdNameToPackageIndex.get(collectionName));
          LOG.info("  Found collection {} from package {}", collectionName, MFD_PACKAGE);
        });

    LOG.info("Verifying db {}", attrDb);
    attributionClient.getDatabase(attrDb).listCollectionNames()
        .forEach((Consumer<? super String>) collectionName -> {
          if (!attributionNameToPackageIndex.containsKey(collectionName)) {
            return;
          }
          assertEquals(format("Package for collection %s is expected to be in %s", collectionName,
              ATTR_PACKAGE),
              ATTR_PACKAGE, attributionNameToPackageIndex.get(collectionName));
          LOG.info("  Found collection {} from package {}", collectionName, ATTR_PACKAGE);
        });
  }

  private static Function<Class<?>, String> getTypePackage() {
    return type -> type.getPackage().getName();
  }

  private static String getCustomCollectionNameOrTypeName(Class<?> type) {
    return Optional.of(type
        .getAnnotation(org.springframework.data.mongodb.core.mapping.Document.class))
        .map(org.springframework.data.mongodb.core.mapping.Document::collection)
        .map(Strings::emptyToNull)
        .orElse(CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL,type.getSimpleName()));
  }

  private static Predicate<Class<?>> isDocument() {
    return type -> type
        .isAnnotationPresent(org.springframework.data.mongodb.core.mapping.Document.class);
  }
}
