package com.catalina.omni.core;

import com.catalina.omni.core.configuration.MongoBeeConfiguration;
import com.github.mongobee.Mongobee;
import com.github.mongobee.exception.MongobeeException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(properties="mongo-bee-prop.enabled=true")
@RunWith(SpringRunner.class)
@AutoConfigurationPackage
@ContextConfiguration(classes = {MongoTestConfig.class, MongoBeeConfiguration.class})
@DirtiesContext
public class MongobeeConnectivityTest {

  @Autowired
  @Qualifier("mongoBeeMfd")
  private Mongobee mfdMongobee;

  @Autowired
  @Qualifier("mongoBeeAttribution")
  private Mongobee attrMongobee;

  @Test
  public void testMongobeeCanConnect() throws MongobeeException {
    mfdMongobee.execute();
    attrMongobee.execute();
  }
}
