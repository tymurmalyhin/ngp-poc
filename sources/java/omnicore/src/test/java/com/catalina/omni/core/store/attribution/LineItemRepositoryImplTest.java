package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.MongoTestConfig;
import com.catalina.omni.core.domain.omniattribution.Advertiser;
import com.catalina.omni.core.domain.omniattribution.Campaign;
import com.catalina.omni.core.domain.omniattribution.LineItem;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@AutoConfigurationPackage
@ContextConfiguration(classes = MongoTestConfig.class)
@DirtiesContext
public class LineItemRepositoryImplTest {


    @Autowired
    private LineItemRepo lineItemRepo;

    @Autowired
    private CampaignRepo campaignRepo;

    @Autowired
    private AdvertiserRepo advertiserRepo;

    @Test
    public void findAllFromCurrentCampaignWithSuppressionOrRetargetSegments(){
        createCampaignWithLineItemsAndSuppressionAndRetargetSegments();
        List<LineItem> lineItems =  lineItemRepo.findAllFromCurrentCampaignWithSuppressionOrRetargetSegments();
        Assert.assertEquals(5, lineItems.size());
        campaignRepo.deleteAll();
        lineItemRepo.deleteAll();
    }

    private void createCampaignWithLineItemsAndSuppressionAndRetargetSegments(){
        Advertiser advertiser = new Advertiser();
        advertiser.setName("Advertiser");
        advertiser.setAdvertiserId(1);
        advertiserRepo.save(advertiser);
        for (int i =0; i <  5; i++) {
            Campaign campaign = new Campaign();
            campaign.setName("Test Campaign " + i);
            campaign.setBrand("Brand " + i);
            List<LineItem> lineItems   =  new ArrayList<>();
            lineItems.add(createLineItem(false, true, "segment-123","segment-1231"));
            lineItems.add(createLineItem(true, true, "segment-123","segment-123"));
            lineItems.add(createLineItem(false, true, null,null));
            lineItems.add(createLineItem(false, false, "segment123" ,"segment-123"));
            campaign.setAdvertiser(advertiser);
            campaign.setLineItems(lineItems);
            campaignRepo.save(campaign);
        }
    }

    private LineItem createLineItem( boolean ended, boolean started, String suppresionSegmentId, String retargetedSegmentId) {
        LocalDateTime localDateTime  =  LocalDateTime.now();
        LocalDateTime  startDate = started ? localDateTime.minusDays(5): localDateTime.plusDays(5);
        LocalDateTime  endDate = ended ? localDateTime.minusDays(2): localDateTime.plusDays(2);
        LineItem lineItem = new LineItem();
        lineItem.setId(UUID.randomUUID().toString());
        lineItem.setStartDate(startDate);
        lineItem.setEndDate(endDate);
        lineItem.setSuppressionSegmentKey(suppresionSegmentId);
        lineItem.setRetargetingSegmentKey(retargetedSegmentId);
        lineItem.setRetargetingSegment(true);
        lineItem.setSuppressSegment(true);
        return lineItem;
    }

}
