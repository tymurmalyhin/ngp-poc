package com.catalina.omni.core.store.integration;


import com.catalina.omni.core.MongoTestConfig;
import com.catalina.omni.core.domain.enums.OmniOfferState;
import com.catalina.omni.core.domain.integration.OmniOffer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.*;

import static java.time.LocalDateTime.now;
import static java.time.temporal.ChronoUnit.HOURS;

@RunWith(SpringRunner.class)
@AutoConfigurationPackage
@ContextConfiguration(classes = MongoTestConfig.class)
@DirtiesContext
public class OmniOfferRepoImplTest {

    @Autowired
    private OmniOfferRepo omniOfferRepo;
    private static final String NETWORK_ID = "99";

    @Test
    public void checkDuplicityAndRankOrderOfItems(){
        createOffer();
      List<OmniOffer>  offers =  omniOfferRepo.findUniqueByBrandAndNotDigestedBy(OmniOfferState.ACTIVE,NETWORK_ID, now().plus(1, HOURS), now());
        Assert.assertEquals(10, offers.size());
        Set<String> uniqueOffersExternalTitles = new HashSet<>();
        int rank =  9;
        for (OmniOffer offer : offers) {
            uniqueOffersExternalTitles.add(offer.getCreativeVariables().get("externalTitle"));
            Assert.assertEquals(Integer.valueOf(offer.getRank()), Integer.valueOf(rank--));
        }
        Assert.assertEquals(10, uniqueOffersExternalTitles.size());
    }


    private void createOffer() {
        LocalDateTime startDate  =  LocalDateTime.of(2018, 9,1,0,0);
        LocalDateTime endDate = LocalDateTime.now().plusDays(1);
        for (int i=0; i <  10; i++){
            for  (int j=0; j < 2; j++) {
                OmniOffer omniOffer = new OmniOffer();
                omniOffer.setNetworkId(NETWORK_ID);
                omniOffer.setCampaingId("campaing-id" + i + "" + j);
                omniOffer.setClientOfferId("clienOffer-id" + i + "" + j);
                Map<String, String> creativeVariable = new HashMap<>();
                creativeVariable.put("externalTitle", "Item " + i);
                omniOffer.setCreativeVariables(creativeVariable);
                omniOffer.setStartDate(startDate);
                omniOffer.setEndDate(endDate);
                omniOffer.setCreated(LocalDateTime.now());
                omniOffer.setState(OmniOfferState.ACTIVE);
                omniOffer.setRank(i);
                omniOfferRepo.save(omniOffer);
            }
        }
    }
}
