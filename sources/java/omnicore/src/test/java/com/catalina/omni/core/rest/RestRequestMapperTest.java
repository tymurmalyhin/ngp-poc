package com.catalina.omni.core.rest;

import org.javatuples.Pair;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class RestRequestMapperTest {

    @Test
    public void default2xx() {
        final Pair<String, String> result = RestRequestMapper.<String, ResponseEntity<String>, String>builder()
                .invoke(RestRequestMapperTest::responseOK)
                .extractResponse(HttpEntity::getBody)
                .build()
                .get();
        assertEquals("OK", result.getValue0());
        assertNull(result.getValue1());
    }

    @Test
    public void custom2xx() {
        final Pair<String, String> result = RestRequestMapper.<String, ResponseEntity<String>, String>builder()
                .invoke(RestRequestMapperTest::responseOK)
                .on2xx(s -> Pair.with("CustomResponse", null))
                .build()
                .get();
        assertEquals("CustomResponse", result.getValue0());
        assertNull(result.getValue1());
    }

    @Test(expected = IllegalStateException.class)
    public void default3xx() {
        RestRequestMapper.<String, ResponseEntity<String>, String>builder()
                .invoke(() -> throwHttpStatus(HttpStatus.TEMPORARY_REDIRECT))
                .extractResponse(HttpEntity::getBody)
                .build()
                .get();
    }

    @Test
    public void custom3xx() {
        final Pair<String, String> result = RestRequestMapper.<String, ResponseEntity<String>, String>builder()
                .invoke(() -> throwHttpStatus(HttpStatus.TEMPORARY_REDIRECT))
                .extractResponse(HttpEntity::getBody)
                .on3xx(e -> "CustomError3xx")
                .build()
                .get();
        assertNull(result.getValue0());
        assertEquals("CustomError3xx", result.getValue1());
    }

    @Test(expected = IllegalStateException.class)
    public void default4xx() {
        RestRequestMapper.<String, ResponseEntity<String>, String>builder()
                .invoke(() -> throwHttpStatus(HttpStatus.NOT_FOUND))
                .extractResponse(HttpEntity::getBody)
                .build()
                .get();
    }

    @Test
    public void custom4xx() {
        final Pair<String, String> result = RestRequestMapper.<String, ResponseEntity<String>, String>builder()
                .invoke(() -> throwHttpStatus(HttpStatus.NOT_FOUND))
                .extractResponse(HttpEntity::getBody)
                .on4xx(e -> "CustomError4xx")
                .build()
                .get();
        assertNull(result.getValue0());
        assertEquals("CustomError4xx", result.getValue1());
    }

    @Test(expected = IllegalStateException.class)
    public void default5xx() {
        RestRequestMapper.<String, ResponseEntity<String>, String>builder()
                .invoke(() -> throwHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR))
                .extractResponse(HttpEntity::getBody)
                .build()
                .get();
    }

    @Test
    public void custom5xx() {
        final Pair<String, String> result = RestRequestMapper.<String, ResponseEntity<String>, String>builder()
                .invoke(() -> throwHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR))
                .extractResponse(HttpEntity::getBody)
                .on5xx(e -> "CustomError5xx")
                .build()
                .get();
        assertNull(result.getValue0());
        assertEquals("CustomError5xx", result.getValue1());
    }

    @Test(expected = IllegalStateException.class)
    public void defaultGeneralFailure() {
        RestRequestMapper.<String, ResponseEntity<String>, String>builder()
                .extractResponse(HttpEntity::getBody)
                .invoke(this::throwIllegalState)
                .build()
                .get();
    }

    @Test
    public void customGeneralFailure() {
        final Pair<String, String> result = RestRequestMapper.<String, ResponseEntity<String>, String>builder()
                .invoke(this::throwIllegalState)
                .extractResponse(HttpEntity::getBody)
                .onGeneralFailure(e -> "CustomGeneralFailure")
                .build()
                .get();
        assertNull(result.getValue0());
        assertEquals("CustomGeneralFailure", result.getValue1());
    }

    private static ResponseEntity<String> responseOK() {
        return ResponseEntity.ok("OK");
    }

    private static ResponseEntity<String> throwHttpStatus(HttpStatus temporaryRedirect) {
        throw new HttpStatusCodeException(temporaryRedirect) {
        };
    }

    private ResponseEntity<String> throwIllegalState() {
        throw new IllegalStateException() {
        };
    }
}