package com.catalina.omni.core.slack;

import com.catalina.omni.core.configuration.SlackProperties;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

import static java.util.Locale.getDefault;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;

@RunWith(MockitoJUnitRunner.class)
public class SlackNotificationServiceImplTest {

    private static final String SLACK_MESSAGE = "message";
    private static final String SOME_URL = "http://localhost";
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private SlackProperties slackProperties;
    @Mock
    private MessageSource messageSource;
    @InjectMocks
    private SlackNotificationServiceImpl slackNotificationManager;

    @Before
    public void setup() {
        when(slackProperties.getSlackWebHookURL()).thenReturn(SOME_URL);
    }

    @Test
    public void notificationIsTurnedOn() {
        when(slackProperties.isSlackNotificationsTurnOn()).thenReturn(true);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("token", slackProperties.getSlackToken());
        map.add("channel", slackProperties.getSlackChannel());
        map.add("username", messageSource.getMessage("slack.username", null, getDefault()));
        map.add("text", SLACK_MESSAGE);
        slackNotificationManager.sendMessage(SLACK_MESSAGE);
        verify(restTemplate).postForObject(SOME_URL, new HttpEntity<>(map, headers), HashMap.class);
    }

    @Test
    public void notificationTurnedOff() {
        when(slackProperties.isSlackNotificationsTurnOn()).thenReturn(false);
        slackNotificationManager.sendMessage("message");
        verify(restTemplate, never()).postForObject(ArgumentMatchers.anyString(), any(), any(Class.class));
    }


}
