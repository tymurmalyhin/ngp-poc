package com.catalina.omni.core;

import com.mongodb.BasicDBList;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import de.flapdoodle.embed.mongo.*;
import de.flapdoodle.embed.mongo.config.*;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.config.IRuntimeConfig;
import de.flapdoodle.embed.process.config.io.ProcessOutput;
import de.flapdoodle.embed.process.runtime.ICommandLinePostProcessor;
import de.flapdoodle.embed.process.runtime.Network;
import de.flapdoodle.embed.process.store.IArtifactStore;
import org.bson.Document;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
@AutoConfigureBefore({EmbeddedMongoAutoConfiguration.class})
public class ShardedMongoConfig implements DestructionAwareBeanPostProcessor {

    private static final int REPLICA_SET_NODE = 27018;
    private static final int SHARD_PORT = 27019;
    private static final int SHARD_MANAGER_PORT = 27020;

    private MongodProcess replicaSetNode;
    private MongosProcess shardManagerNode;
    private MongodProcess shardNode;

    @Override
    public void postProcessBeforeDestruction(Object bean, String beanName) throws BeansException {
        shardManagerNode.stop();
        shardNode.stop();
        replicaSetNode.stop();
    }

    @Bean(
            initMethod = "start",
            destroyMethod = "stop"
    )
    public MongodExecutable embeddedMongoServer() throws IOException {

        replicaSetNode = nodeExecutable(ShardedMongoConfig.REPLICA_SET_NODE, true, true, false).start();
        final MongoClient replicaSetClient = new MongoClient(new ServerAddress(Network.getLocalHost(), ShardedMongoConfig.REPLICA_SET_NODE));
        MongoDatabase adminDatabase = replicaSetClient.getDatabase("admin");
        initializeReplicaSet(adminDatabase);
        replicaSetClient.close();

        shardManagerNode = shardExecutable().start();

        shardNode = nodeExecutable(SHARD_PORT, false, false, true).start();
        final MongoClient shardClient = new MongoClient("localhost", SHARD_MANAGER_PORT);
        final MongoDatabase shardAdmingDb = shardClient.getDatabase("admin");
        shardAdmingDb.runCommand(new Document("addShard", "localhost:" + SHARD_PORT));
        shardClient.close();

        return dummyExecutable(replicaSetNode, shardNode, shardManagerNode);
    }

    @Bean("attributionClient")
    public MongoClient attributionMongoClient() {
        final MongoClient client = new MongoClient("localhost", SHARD_MANAGER_PORT);
        client.getDatabase("admin").runCommand(new Document("enableSharding", "omniattribution"));
        return client;
    }

    @Bean("mfdClient")
    public MongoClient mfdMongoClient() {
        final MongoClient client = new MongoClient("localhost", SHARD_MANAGER_PORT);
        client.getDatabase("admin").runCommand(new Document("enableSharding", "omnimfd"));
        return client;
    }

    private void initializeReplicaSet(MongoDatabase adminDatabase) {
        Document config = new Document("_id", "rs0");
        BasicDBList members = new BasicDBList();
        members.add(new Document("_id", 0).append("host", "localhost:" + ShardedMongoConfig.REPLICA_SET_NODE));
        config.put("members", members);
        adminDatabase.runCommand(new Document("replSetInitiate", config));
    }

    private MongosExecutable shardExecutable() throws IOException {
        return MongosStarter.getDefaultInstance().prepare(new MongosConfigBuilder()
                .version(Version.Main.PRODUCTION)
                .configDB("localhost:" + REPLICA_SET_NODE)
                .replicaSet("rs0")
                .cmdOptions(new MongoCmdOptionsBuilder().useNoJournal(false).build())
                .net(new Net(SHARD_MANAGER_PORT, Network.localhostIsIPv6())).build());
    }

    private MongodExecutable nodeExecutable(int port, boolean configServer, boolean isReplicaSet, boolean isShardServer) throws IOException {
        MongodConfigBuilder builder = new MongodConfigBuilder()
                .version(Version.Main.PRODUCTION)
                .configServer(configServer);

        if (isReplicaSet) {
            builder = builder.withLaunchArgument("--replSet", "rs0");
        }

        final IMongodConfig config = builder
                .cmdOptions(new MongoCmdOptionsBuilder().useNoJournal(false).build())
                .shardServer(isShardServer)
                .net(new Net(Network.getLocalHost().getHostAddress(), port, Network.localhostIsIPv6()))
                .build();
        return MongodStarter.getDefaultInstance().prepare(config);
    }

    private static MongodExecutable dummyExecutable(MongodProcess nodeOne, MongodProcess nodeTwo, MongosProcess shardNode) {
        return new MongodExecutable(null, null, new IRuntimeConfig() {
            @Override
            public ProcessOutput getProcessOutput() {
                return null;
            }

            @Override
            public ICommandLinePostProcessor getCommandLinePostProcessor() {
                return null;
            }

            @Override
            public IArtifactStore getArtifactStore() {
                return null;
            }

            @Override
            public boolean isDaemonProcess() {
                return false;
            }
        }, null) {
            @Override
            public synchronized MongodProcess start() throws IOException {
                return nodeOne;
            }

            @Override
            public synchronized void stop() {
            }
        };
    }
}
