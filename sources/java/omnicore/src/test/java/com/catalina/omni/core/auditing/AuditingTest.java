package com.catalina.omni.core.auditing;

import com.catalina.omni.core.MongoTestConfig;
import com.catalina.omni.core.domain.integration.OmniOffer;
import com.catalina.omni.core.domain.omniattribution.Attribution;
import com.catalina.omni.core.store.attribution.AttributionRepo;
import com.catalina.omni.core.store.integration.OmniOfferRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@AutoConfigurationPackage
@ContextConfiguration(classes = {MongoTestConfig.class})
@DirtiesContext
public class AuditingTest {

  @Autowired
  private AttributionRepo attrRepo;

  @Autowired
  private OmniOfferRepo offerRepo;

  @Test
  public void testAuditingAttribution() {
    assertNotNull(attrRepo.save(new Attribution()).getCreatedDate());
  }

  @Test
  public void testAuditingIntegration() {
    assertNotNull(offerRepo.save(new OmniOffer()).getCreated());
  }
}
