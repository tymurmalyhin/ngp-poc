package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.MongoTestConfig;
import com.catalina.omni.core.domain.omniattribution.Attribution;
import com.catalina.omni.core.domain.omniattribution.LineItemImpressionEvent;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@AutoConfigurationPackage
@ContextConfiguration(classes = MongoTestConfig.class)
@DirtiesContext
public class AttributionRepoImplTest {

    @Autowired
    private AttributionRepo attributionRepo;

    @Test
    public void testGetAttributionWithParituclarLineItem(){
        List<Attribution> attributions = createAttributionWithLineItemImpressionEvent(5, true);
        attributions.addAll(createAttributionWithLineItemImpressionEvent(3, false));
        Set<String> ids = attributions.stream()
                .flatMap(attribution -> attribution.getLineItems().stream())
                .map(lineItemImpressionEvent -> lineItemImpressionEvent.getLineItemId())
                .collect(Collectors.toSet());
        List<Attribution>  attributionList =  attributionRepo.getByLineItemsIdAndNotSuppressed(ids,0,100);
        Assert.assertEquals(3, attributionList.size());
    }

    private List<Attribution> createAttributionWithLineItemImpressionEvent(int count, boolean suppressed){
        List<Attribution> attributions =  new ArrayList<>();
        for (int i=0; i<count; i++) {
            Attribution attribution = new Attribution();
            List<LineItemImpressionEvent> LineItemImpressionEvents  = new ArrayList<>();
            LineItemImpressionEvents.add(createLineItemImpressionEvent(suppressed));
            attribution.setLineItems(LineItemImpressionEvents);
            attributions.add(attribution);
        }
        attributionRepo.saveAll(attributions);
        return attributions;
    }


    private LineItemImpressionEvent createLineItemImpressionEvent(boolean suppressed) {
        LineItemImpressionEvent lineItemImpressionEvent = new LineItemImpressionEvent();
        lineItemImpressionEvent.setImpressionDate(LocalDateTime.now());
        lineItemImpressionEvent.setLineItemId(UUID.randomUUID().toString());
        lineItemImpressionEvent.setSuppressed(suppressed);
        return lineItemImpressionEvent;
    }
}
