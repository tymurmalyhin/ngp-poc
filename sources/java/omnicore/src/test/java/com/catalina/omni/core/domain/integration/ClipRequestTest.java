package com.catalina.omni.core.domain.integration;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class ClipRequestTest {

    private ObjectMapper objectMapper;

    @Before
    public void init() {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void testSerialization() throws IOException {
        ClipRequest request = ClipRequest.builder()
                .cid("b")
                .componentId(4)
                .dateOfClip(LocalDateTime.MAX).build();

        String json = objectMapper.writer().writeValueAsString(request);
        ClipRequest deserializedRequest = objectMapper.readerFor(ClipRequest.class).readValue(json);
        assertEquals(request, deserializedRequest);
        assertEquals("b", request.getCid());
        assertEquals(4, request.getComponentId().intValue());
        assertEquals(LocalDateTime.MAX, request.getDateOfClip());
    }
}