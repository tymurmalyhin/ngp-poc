package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.MongoTestConfig;
import com.catalina.omni.core.configuration.hystrix.HystrixConfiguration;
import com.catalina.omni.core.domain.omniattribution.AttributionCIDMonitor;
import com.catalina.omni.core.domain.omniattribution.LineItemImpressionEvent;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@AutoConfigurationPackage
//@ContextConfiguration(classes = {ShardedMongoConfig.class, MongoTestConfig.class, HystrixConfiguration.class})
@ContextConfiguration(classes = {MongoTestConfig.class, HystrixConfiguration.class})
@DirtiesContext
public class AttributionCIDMonitorRepoAggregationTest {

    private static final LocalDateTime DATE = LocalDateTime.now();

    @Autowired
    private AttributionCIDMonitorRepo repo;

    @After
    public void close() {
        repo.deleteAll();
    }

    @Test
    public void testAggregationForFindQuery() {
        final ArrayList<AttributionCIDMonitor> toSave = new ArrayList<>(20);
        for (int i = 0; i < 20; i++) {
            final AttributionCIDMonitor cidMonitor = new AttributionCIDMonitor();
            cidMonitor.setCampaignId("campain-" + i);
            cidMonitor.setCid("USA-0099-ABCD");
            cidMonitor.setLineItems(new ArrayList<>(5));

            for (int j = 0; j < 5; j++) {
                final LineItemImpressionEvent event = new LineItemImpressionEvent();
                event.setLineItemId(cidMonitor.getCampaignId() + "-" + j);
                event.setTealiumEventId("te" + j);
                event.setImpressionDate(DATE.plusDays(j));
                event.setCreativeId("ci" + j);
                cidMonitor.getLineItems().add(event);
            }
            toSave.add(cidMonitor);
        }
        repo.saveAll(toSave);

        final CompletableFuture[] futures = new CompletableFuture[toSave.size()];

        ExecutorService exec = null;
        try {
            exec = Executors.newFixedThreadPool(5);
            for (int i = 0; i < toSave.size(); i++) {
                final AttributionCIDMonitor monitor = toSave.get(i);
                final LineItemImpressionEvent item = monitor.getLineItems().get(3);

                futures[i] = CompletableFuture.supplyAsync(() -> repo.findByCampaignIdAndCidAndTealiumEventIdAndImpressionDate(
                        // String campaignId, String cid, String tealiumEventId, LocalDateTime impressionDate
                        monitor.getCampaignId(),
                        monitor.getCid(),
                        item.getTealiumEventId(),
                        item.getImpressionDate()), exec)
                  .thenAccept(found -> {
                    assertEquals("Query for " + monitor + " failed", 1, found.size());
                    final AttributionCIDMonitor first = found.get(0);
                    assertEquals("Query for " + monitor + " failed", monitor.getCid(), first.getCid());
                    assertEquals("Query for " + monitor + " failed", monitor.getCampaignId(), first.getCampaignId());
                });
            }

            CompletableFuture.allOf(futures).join();
        } finally {
            if (exec != null) exec.shutdown();
        }
    }

    @Test
    public void testAggregationForInsert() {

        // Because I want from Histrix to remove duplicate records, I have to set same dataCreated to all records with same data
        LocalDateTime dateCreated = LocalDateTime.now();

        final AttributionCIDMonitor firstMonitor = new AttributionCIDMonitor();
        firstMonitor.setCid("xxxx");
        firstMonitor.setDateCreated(dateCreated);

        final AttributionCIDMonitor secondMonitor = new AttributionCIDMonitor();
        secondMonitor.setCid("xxxx");
        secondMonitor.setDateCreated(dateCreated);

        final AttributionCIDMonitor thirdMonitor = new AttributionCIDMonitor();
        thirdMonitor.setCid("xxxx");
        thirdMonitor.setDateCreated(dateCreated);

        final AttributionCIDMonitor fourthMonitor = new AttributionCIDMonitor();
        fourthMonitor.setCid("xxxa");

        final AttributionCIDMonitor fifthMonitor = new AttributionCIDMonitor();
        fifthMonitor.setCid("xxxb");

        final ExecutorService exec = Executors.newFixedThreadPool(5);
        @SuppressWarnings("unchecked")
        CompletableFuture<AttributionCIDMonitor>[] futures = new CompletableFuture[] {
                CompletableFuture.supplyAsync( () -> repo.save(firstMonitor), exec),
                CompletableFuture.supplyAsync( () -> repo.save(secondMonitor), exec),
                CompletableFuture.supplyAsync( () -> repo.save(thirdMonitor), exec),
                CompletableFuture.supplyAsync( () -> repo.save(fourthMonitor), exec),
                CompletableFuture.supplyAsync( () -> repo.save(fifthMonitor), exec)
        };

        CompletableFuture.allOf(futures).join();
        exec.shutdown();

        final AttributionCIDMonitor first = futures[0].join();
        final AttributionCIDMonitor second = futures[1].join();
        final AttributionCIDMonitor third = futures[2].join();
        final AttributionCIDMonitor four = futures[3].join();
        final AttributionCIDMonitor five = futures[4].join();

        long repoCount = repo.count();
        if (repoCount == 3) { // one hystrix window, the most likely case
            assertNotSame(first, second);
            assertNotSame(first, third);
            assertTrue(first.matchById(second));
            assertTrue(first.matchById(third));
        } else if (repoCount < 3) {
            Assert.fail("Wrong number of AttributionCIDMonitor found: " + repoCount);
        }
        assertFalse(first.matchById(four));
        assertFalse(first.matchById(five));
        assertNotNull(first.getDateCreated());
        assertNotNull(second.getDateCreated());
        assertNotNull(third.getDateCreated());
        assertNotNull(four.getDateCreated());
        assertNotNull(five.getDateCreated());
    }

    @Test
    public void testUpdatingOfCidMonitors() {
        AttributionCIDMonitor cidMonitor = createCidMonitor();
        AttributionCIDMonitor saved = repo.save(cidMonitor);
        saved.setCid("new CID");

        repo.save(saved);

        Assertions.assertThat(saved).isEqualTo(repo.findById(saved.getId()).get());
    }

    @Test
    public void testAggregationForUpdate() {
        AttributionCIDMonitor firstMonitor = new AttributionCIDMonitor();
        firstMonitor.setCid("xxxa");
        AttributionCIDMonitor secondMonitor = new AttributionCIDMonitor();
        secondMonitor.setCid("xxxb");
        AttributionCIDMonitor thirdMonitor = new AttributionCIDMonitor();
        thirdMonitor.setCid("xxxc");

        repo.insert(firstMonitor);
        repo.insert(secondMonitor);
        repo.insert(thirdMonitor);

        firstMonitor.setCampaignId("a");
        secondMonitor.setCampaignId("a");
        thirdMonitor.setCampaignId("a");

        final ExecutorService exec = Executors.newFixedThreadPool(3);
        @SuppressWarnings("unchecked")
        CompletableFuture<AttributionCIDMonitor>[] futures = new CompletableFuture[]{
                CompletableFuture.supplyAsync(() -> repo.save(firstMonitor), exec),
                CompletableFuture.supplyAsync(() -> repo.save(secondMonitor), exec),
                CompletableFuture.supplyAsync(() -> repo.save(thirdMonitor), exec)
        };

        CompletableFuture.allOf(futures).join();
        exec.shutdown();

        final AttributionCIDMonitor first = futures[0].join();
        final AttributionCIDMonitor second = futures[1].join();
        final AttributionCIDMonitor third = futures[2].join();

        assertEquals(firstMonitor, first);
        assertEquals(secondMonitor, second);
        assertEquals(thirdMonitor, third);
    }

    private AttributionCIDMonitor createCidMonitor() {
        AttributionCIDMonitor cidMonitor = new AttributionCIDMonitor();
        cidMonitor.setCid("USA-0099-95793114745");
        cidMonitor.setCampaignId("a2e908ca-e23a-4811-b995-63af05440fc6");

        return cidMonitor;
    }
}