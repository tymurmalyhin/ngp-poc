package org.springframework.data.mongodb.core;

import com.catalina.omni.core.configuration.hystrix.HystrixCommandConfiguration;
import com.catalina.omni.core.domain.omniattribution.AttributionCIDMonitor;
import com.catalina.omni.core.hystrix.command.CompositeQueryRequestCollapser;
import com.catalina.omni.core.hystrix.command.attribution.cidmonitor.save.BulkUpdateOperationCollapser;
import com.catalina.omni.core.hystrix.command.attribution.cidmonitor.save.factory.BulkUpdateCommandFactory;
import com.catalina.omni.core.hystrix.command.attribution.cidmonitor.save.factory.InsertCommandFactory;
import com.catalina.omni.core.hystrix.command.attribution.cidmonitor.save.factory.UpdateCommandFactory;
import com.catalina.omni.core.hystrix.command.factory.QueryCommandFactory;
import com.catalina.omni.core.hystrix.matcher.MatchableByEqualsIgnoreId;
import com.catalina.omni.core.hystrix.matcher.MatchableById;
import com.catalina.omni.core.hystrix.param.find.CompositeQueryCommandParam;
import com.catalina.omni.core.hystrix.param.find.factory.QueryCommandParamFactory;
import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import org.apache.commons.lang.SerializationUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.convert.MongoWriter;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * It has to be in this package due to overriding methods that use package private classes
 */
public class HystrixMongoTemplate extends MongoTemplate {

    private static final Logger LOG = LoggerFactory.getLogger(HystrixMongoTemplate.class);

    private final HystrixRequestContext globalRequestContext;
    private final QueryCommandParamFactory commandParamFactory;
    private final QueryCommandFactory queryCommandFactory;
    private final InsertCommandFactory insertCommandFactory;
    private final UpdateCommandFactory updateCommandFactory;
    private final BulkUpdateCommandFactory bulkUpdateCommandFactory;
    private final MongoConverter mongoConverter;

    public HystrixMongoTemplate(MongoDbFactory mongoDbFactory,
                                MongoConverter mongoConverter,
                                HystrixRequestContext globalRequestContext,
                                HystrixCommandConfiguration commandConfiguration,
                                AuditingHandler auditingHandler) {
        super(mongoDbFactory, mongoConverter);
        this.mongoConverter = mongoConverter;
        this.globalRequestContext = globalRequestContext;
        this.commandParamFactory = new QueryCommandParamFactory(mongoConverter);
        this.queryCommandFactory = new QueryCommandFactory(this, commandConfiguration);
        this.insertCommandFactory = new InsertCommandFactory(this, commandConfiguration, auditingHandler);
        this.updateCommandFactory = new UpdateCommandFactory(this, commandConfiguration, auditingHandler);
        this.bulkUpdateCommandFactory = new BulkUpdateCommandFactory(this, commandConfiguration);
    }

    /**
     * Calls regular parent method
     * */
    public  <T> T doSaveSuper(String collectionName, T objectToSave){
        return (T) super.doSave(collectionName, objectToSave, mongoConverter);
    }

    @Override
    protected <T> T doSave(String collectionName, T objectToSave, MongoWriter<T> writer) {
        if (objectToSave instanceof AttributionCIDMonitor) {
            synchronized (this) {
                if (!HystrixRequestContext.isCurrentThreadInitialized()) {
                    logThread();
                    HystrixRequestContext.setContextOnCurrentThread(globalRequestContext);
                }
            }

            final AttributionCIDMonitor data = (AttributionCIDMonitor) objectToSave;
            try {
                return (T) updateCommandFactory.createCommand(data, collectionName).queue().get(5000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                throw new IllegalStateException("Update operation failed", e);
            }
        }
        return super.doSave(collectionName, objectToSave, writer);
    }


    /**
     * Calls regular parent method
     * */
    public  <T> void doInsertSuper(String collectionName, T objectToSave) {
        super.doInsert(collectionName, objectToSave, this.mongoConverter);
    }

    @Override
    protected <T> T doInsert(String collectionName, T objectToSave, MongoWriter<T> writer) {
        if (objectToSave instanceof MatchableById) {
            synchronized (this) {
                if (!HystrixRequestContext.isCurrentThreadInitialized()) {
                    logThread();
                    HystrixRequestContext.setContextOnCurrentThread(globalRequestContext);
                }
            }

            final MatchableByEqualsIgnoreId data = (MatchableByEqualsIgnoreId) objectToSave;
            try {
                final MatchableByEqualsIgnoreId result = (MatchableByEqualsIgnoreId) insertCommandFactory.createCommand(data, collectionName).queue().get(5000, TimeUnit.MILLISECONDS);
                //to emulate the same behaviour as regular repository. No cloning is needed here
                data.setId(result.getId());
                //we need to set the date from result to the objectToSave to have it distributed to all duplicates
                data.setDateCreated(result.getDateCreated());
                return objectToSave;
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                throw new IllegalStateException("Insert operation failed", e);
            }
        }
        return super.doInsert(collectionName, objectToSave, writer);
    }

    @Override
    <S, T> List<T> doFind(String collectionName, Document query, Document fields,
                          Class<S> sourceClass, Class<T> targetClass, CursorPreparer preparer) {

        if (!sourceClass.equals(targetClass)) {
            return super.doFind(collectionName, query, fields, sourceClass, targetClass, preparer);
        }

        //right now we are testing this only on cid monitors
        if (!AttributionCIDMonitor.class.equals(targetClass)) {
            return super.doFind(collectionName, query, fields, sourceClass, targetClass, preparer);
        }

        synchronized (this) {
            if (!HystrixRequestContext.isCurrentThreadInitialized()) {
                logThread();
                HystrixRequestContext.setContextOnCurrentThread(globalRequestContext);
            }
        }

        final CompositeQueryCommandParam<T> param =
                commandParamFactory.createQueryCommandParam(targetClass, query);

        //not a registered command, so fallback on default query method
        if (param != null) {
            final CompositeQueryRequestCollapser<T> command = queryCommandFactory.createCommand(param);

            if (command != null) {
                try {
                    final List<T> result = command.queue().get(10000, TimeUnit.MILLISECONDS);
                    //IMPORTANT - Every aggregated method HAS to clone the results, otherwise we are going to have
                    //all sorts of weird situations where multiple threads would modify same instance of result
                    return (List<T>) SerializationUtils.clone((Serializable) result);
                } catch (InterruptedException | ExecutionException | TimeoutException e) {
                    throw new IllegalStateException(e);
                }
            }
        }

        return super.doFind(collectionName, query, fields, sourceClass, targetClass, preparer);
    }

    public void bulkUpdate(Query query, Update update, Class<?> entityClass, boolean upsert, boolean multi) {
        String collectionName = getCollectionName(entityClass);
        synchronized (this) {
            if (!HystrixRequestContext.isCurrentThreadInitialized()) {
                logThread();
                HystrixRequestContext.setContextOnCurrentThread(globalRequestContext);
            }
        }

        final BulkUpdateOperationCollapser<?> command =
                bulkUpdateCommandFactory.createCommand(collectionName, entityClass, query, update, upsert, multi);
        try {
            command.queue().get(10000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            throw new IllegalStateException(e);
        }
    }

    private void logThread() {
        LOG.info("Setting hystrix context for thread {}", Thread.currentThread().getName());
    }

}