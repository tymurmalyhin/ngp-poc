package com.catalina.omni.core.mapper;

import com.catalina.omni.core.domain.omniattribution.TealiumPixelEventHub;
import com.catalina.omni.core.domain.omniattribution.TealiumPixelEventHubData;
import com.catalina.omni.core.domain.omniattribution.TealiumPixelEventHubUdo;
import com.catalina.platform.avro.ChannelType;
import com.catalina.platform.avro.Event;

public final class TealiumPixelEventHubMapper {

    private TealiumPixelEventHubMapper() {
    }

    public static TealiumPixelEventHub mapPixelEventToVaultEvent(Event event) {
        TealiumPixelEventHub tealiumPixelEventHub = new TealiumPixelEventHub();
        tealiumPixelEventHub.setData(new TealiumPixelEventHubData());
        TealiumPixelEventHubUdo udo = new TealiumPixelEventHubUdo();
        tealiumPixelEventHub.getData().setUdo(udo);

        if (event != null) {
            tealiumPixelEventHub.setPostTime(event.getTime());
            tealiumPixelEventHub.setUserAgent(event.getUserAgent());
            tealiumPixelEventHub.setEventId(event.getId());

            udo.setV("1");
            udo.setT(event.getType().name());
            udo.setDid(event.getDeviceId());
            udo.setEa(event.getEventAction());
            udo.setUip(event.getUserIp());
            udo.setEv(event.getEventValue());
            udo.setAid(event.getMobileAppId());
            udo.setAv(event.getMobileAppVersion());
            udo.setSte(event.getSiteId());
            udo.setCi(event.getCampaignId());
            udo.setEl(event.getEventLabel());
            udo.setCn(event.getCampaignName());
            udo.setSid(event.getSessionId());
            udo.setSys("TEALIUM");

            if (event.getType() != null) {
                udo.setT(event.getType().name());
            }
            udo.setCs(ChannelType.web.name());

            udo.setLon(mapNumberToString(event.getLongitude()));
            udo.setLat(mapNumberToString(event.getLatitude()));

//        udo.setUdoNetworkId(event.getNetworkId());
//        udo.setUdoCid(event.getCid());
//        udo.setUdoUserLanguage(event.getUserLanguage());
//        udo.setCountryCode(event.getCountryCode());
//        udo.setCm(event.getMediaType());
//        udo.setUdoDr(event.getDocReferrer());
//        udo.setUdoDh(event.getDocHost());
//        udo.setUdoDocPath(event.getDocPath());
//        udo.setUdoDocTitle(event.getDocTitle());
//        udo.setUdoCampaignKeyword(event.getCampaignKeyword());
//        udo.setUdoSystemId(event.getSystemId());
//        udo.setUdoEventCatgory(event.getEventCategory());
//        udo.setUdoSocialNetwork(event.getSocialNetwork());
//        udo.setUdoSocialAction(event.getSocialAction());
//        udo.setSte(event.getSocialActionTarget());
//        if (udo.getAds() != null) {
//            event.setSourceIds(newArrayList(udo.getAds()));
//        }
//        udo.setUdoAdGroupId(event.getAdGroupId());
//        udo.setUdoCAdCreativeSetId(event.getAdCreativeSetId());
//        udo.setUdoModel(event.getModel());
//        udo.setUdoMobileAppName(event.getMobileAppName());
        }
        return tealiumPixelEventHub;
    }

    private static String mapNumberToString(Number val) {
        return (val == null)? null : val.toString();
    }

}
