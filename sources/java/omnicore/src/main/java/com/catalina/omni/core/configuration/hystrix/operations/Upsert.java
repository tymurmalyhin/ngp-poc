package com.catalina.omni.core.configuration.hystrix.operations;

public final class Upsert {

    private Insert insert;
    private Update update;

    public Insert getInsert() {
        return insert;
    }

    public void setInsert(Insert insert) {
        this.insert = insert;
    }

    public Update getUpdate() {
        return update;
    }

    public void setUpdate(Update update) {
        this.update = update;
    }
}
