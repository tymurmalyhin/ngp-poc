package com.catalina.omni.core.migration;


import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.client.MongoDatabase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.function.Consumer;

/**
 * Currently we're using sprig @scheduled instead of quartz scheduler - we need to remove old legacy quartz collections
 */
@ChangeLog(order = "004")
public class MigrationScript004 {
    private static Logger LOG = LoggerFactory.getLogger(MigrationScript004.class);

    @ChangeSet(author = "pziak", order = "04", id = "removeOldQuartzCollections")
    public void removeOldQuartzCollections(MongoTemplate template, Environment environment) {

        final MongoDatabase db = template.getDb();
        final String integratioDbName = environment.getProperty("mongodb.omnimfd.name");
        final String databaseName = db.getName().trim();

        //only relevant for mfd database
        if (databaseName.equalsIgnoreCase(integratioDbName)) {
            LOG.info("Script invoked for db {}", db.getName());
            db.listCollectionNames().forEach((Consumer<? super String>) collectionName -> {
                if (collectionName.startsWith("quartz_")) {
                    LOG.info("Droping collection {}", collectionName);
                    db.getCollection(collectionName).drop();
                }
            });
        } else {
            LOG.info("Skipping execution of migration script 004, not relevant for db {}", databaseName);
        }
    }
}
