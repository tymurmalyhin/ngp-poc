package com.catalina.omni.core.domain.integration;

import com.catalina.omni.core.domain.enums.EventAction;
import com.catalina.omni.core.domain.enums.EventType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by ziak on 1/24/18.
 */
@Document
@Getter
@Setter
public class OmniEvent {
    @Id
    private String eventId;
    private String offerId;
    private String campaignId;
    private String promotionId;
    private EventAction action;
    private EventType eventType;
    private String json;
}
