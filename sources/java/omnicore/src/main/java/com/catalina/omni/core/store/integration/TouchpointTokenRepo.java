package com.catalina.omni.core.store.integration;

import com.catalina.omni.core.domain.enums.CountryEnum;
import com.catalina.omni.core.domain.integration.TouchpointToken;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface TouchpointTokenRepo extends MongoRepository<TouchpointToken, String> {

    Optional<TouchpointToken> findByCountryAndNetworkId(CountryEnum country, String networkId);
}
