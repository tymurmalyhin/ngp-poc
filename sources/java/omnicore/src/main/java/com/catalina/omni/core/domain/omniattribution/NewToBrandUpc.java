package com.catalina.omni.core.domain.omniattribution;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@Setter
public class NewToBrandUpc {
    @Id
    private String id;
    private String upc;
}
