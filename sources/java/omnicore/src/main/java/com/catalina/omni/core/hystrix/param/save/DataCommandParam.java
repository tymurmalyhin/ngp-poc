package com.catalina.omni.core.hystrix.param.save;

public abstract class DataCommandParam<T> {
    protected final T data;
    protected final String collectionName;

    protected DataCommandParam(T data, String collectionName) {
        this.data = data;
        this.collectionName = collectionName;
    }

    public T getData() {
        return data;
    }

    public String getCollectionName() {
        return collectionName;
    }
}
