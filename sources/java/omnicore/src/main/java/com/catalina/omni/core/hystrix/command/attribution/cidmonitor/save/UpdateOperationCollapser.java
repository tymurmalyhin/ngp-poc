package com.catalina.omni.core.hystrix.command.attribution.cidmonitor.save;

import com.catalina.omni.core.configuration.hystrix.operations.Update;
import com.catalina.omni.core.hystrix.matcher.MatchableById;
import com.catalina.omni.core.hystrix.param.save.DataCommandParam;
import com.catalina.omni.core.hystrix.param.save.UpdateCommandParam;
import com.mongodb.BasicDBObject;
import com.netflix.hystrix.*;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.HystrixMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;
import static org.springframework.data.mongodb.core.query.Criteria.where;

public class UpdateOperationCollapser<T extends MatchableById<T>>
        extends HystrixCollapser<Iterable<T>, T, UpdateCommandParam<T>> {

    private final UpdateCommandParam<T> param;
    private final HystrixCommand.Setter commandSetter;
    private final HystrixMongoTemplate mongoTemplate;
    private final Class<T> domainClass;
    private final AuditingHandler auditingHandler;

    public UpdateOperationCollapser(UpdateCommandParam<T> param, Update params, HystrixMongoTemplate mongoTemplate, AuditingHandler auditingHandler) {
        super(HystrixCollapser.Setter.withCollapserKey(
                HystrixCollapserKey.Factory.asKey(param.toKey()))
                .andScope(Scope.GLOBAL)
                .andCollapserPropertiesDefaults(HystrixCollapserProperties.Setter()
                        .withTimerDelayInMilliseconds(params.getTimeWindowSize())
                        .withMaxRequestsInBatch(params.getMaxRequestsInBatch())));
        this.param = param;
        this.auditingHandler = auditingHandler;
        this.mongoTemplate = mongoTemplate;
        final String key = param.toKey();
        this.commandSetter = HystrixCommand.Setter
                .withGroupKey(HystrixCommandGroupKey.Factory.asKey(key))
                .andCommandKey(HystrixCommandKey.Factory.asKey(key))
                .andCommandPropertiesDefaults(
                        HystrixCommandProperties.Setter()
                                .withCircuitBreakerEnabled(true)
                                .withCircuitBreakerErrorThresholdPercentage(
                                        params.getCircuitBreakerErrorThresholdPercentage())
                                .withCircuitBreakerSleepWindowInMilliseconds(
                                        params.getSleepWindowInMilliseconds())
                                .withExecutionIsolationSemaphoreMaxConcurrentRequests(10000)
                                .withExecutionIsolationStrategy(HystrixCommandProperties.ExecutionIsolationStrategy.SEMAPHORE)
                                .withExecutionTimeoutEnabled(false)
                                .withRequestLogEnabled(true));
        domainClass = (Class<T>) this.param.getData().getClass();
    }

    @Override
    public UpdateCommandParam<T> getRequestArgument() {
        return param;
    }

    @Override
    protected HystrixCommand<Iterable<T>> createCommand(Collection<CollapsedRequest<T, UpdateCommandParam<T>>> collection) {
        return new HystrixCommand<Iterable<T>>(commandSetter) {
            @Override
            protected Iterable<T> run() throws Exception {
                final BulkOperations bulkOps = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, domainClass, param.getCollectionName());

                final List<UpdateWithId> updates = collection.stream()
                        .map(CollapsedRequest::getArgument)
                        .map(DataCommandParam::getData)
                        .peek(auditingHandler::markModified)
                        .map(data -> {
                            BasicDBObject doc = new BasicDBObject();
                            mongoTemplate.getConverter().write(data, doc);
                            doc.remove("_id");

                            final org.springframework.data.mongodb.core.query.Update up = new org.springframework.data.mongodb.core.query.Update();
                            doc.forEach(up::set);

                            return new UpdateWithId(up, data.getId());
                        }).collect(Collectors.toList());

                if (updates.size() == 1) {
                    //if singular save, just call the regular save - it's shard aware
                    return Collections.singletonList(mongoTemplate.doSaveSuper(param.getCollectionName(),
                            collection.iterator().next().getArgument().getData()));
                }
                updates.forEach(updateWithId -> bulkOps.upsert(new Query(where("id").is(updateWithId.id)), updateWithId.update));

                // bulkOperation is executed on db server as n-commands (depends on updates count)
                bulkOps.execute();

                final Set<String> ids = updates.stream().map(updateWithId -> updateWithId.id).collect(Collectors.toSet());

                return mongoTemplate.find(new Query(where("id").in(ids)), domainClass);
            }
        };
    }

    @Override
    protected void mapResponseToRequests(Iterable<T> responses, Collection<CollapsedRequest<T, UpdateCommandParam<T>>> requests) {
        final List<CollapsedRequest<T, UpdateCommandParam<T>>> withoutAnswer = new LinkedList<>();

        requests.forEach(request -> {
            final Iterator<T> responseIterator = responses.iterator();
            final T requestData = request.getArgument().getData();

            while (responseIterator.hasNext()) {
                final T response = responseIterator.next();

                if (response.matchById(requestData)) {
                    request.setResponse(response);
                    request.setComplete();
                    return;
                }
            }
            //not sure if this situation can even occur but just in case so that request doesnt hang in the air
            //till timeout hits
            withoutAnswer.add(request);
        });

        withoutAnswer.forEach(nonAnswered -> {
            nonAnswered.setException(new IllegalStateException(format("Updated object for data %s not found in database",
                    nonAnswered.getArgument().getData())));
            nonAnswered.setComplete();
        });
    }

    private static final class UpdateWithId {
        private final org.springframework.data.mongodb.core.query.Update update;
        private final String id;

        private UpdateWithId(org.springframework.data.mongodb.core.query.Update update, String id) {
            this.update = update;
            this.id = id;
        }
    }
}
