package com.catalina.omni.core.hystrix.param.save;

import org.springframework.data.mongodb.core.query.Update;
import java.util.Objects;

import org.springframework.data.mongodb.core.query.Query;

public class BulkUpdateCommandParam<T> extends DataCommandParam<T> {

    protected final Query query;
    protected final Update update;
    protected final boolean upsert;
    protected final boolean multi;
    protected final Class<?> entityClass;

    public BulkUpdateCommandParam(Query query, Update update, boolean upsert, boolean multi, String collectionName, Class<T> entityClass) {
        super(null, collectionName);
        this.query = query;
        this.update = update;
        this.upsert = upsert;
        this.multi = multi;
        this.entityClass = entityClass;
    }

    public Query getQuery() {
        return query;
    }

    public Update getUpdate() {
        return update;
    }

    public boolean isUpsert() {
        return upsert;
    }

    public boolean isMulti() {
        return multi;
    }

    public Class<?> getEntityClass() {
        return entityClass;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      BulkUpdateCommandParam<?> that = (BulkUpdateCommandParam<?>) o;
      return Objects.equals(entityClass, that.entityClass) &&
              Objects.equals(collectionName, that.collectionName) &&
              Objects.equals(query, that.query) &&
              Objects.equals(update, that.update) &&
              Objects.equals(upsert, that.upsert) &&
              Objects.equals(multi, that.multi);
    }

    @Override
    public int hashCode() {
      return Objects.hash(entityClass, collectionName, query, update, upsert, multi);
    }


    public String toKey() {
        return entityClass.getSimpleName() + "_" + collectionName + "_upsert"; // CompositeQueryCommandParam
    }
}
