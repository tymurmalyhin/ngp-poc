package com.catalina.omni.core.domain.omniattribution;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kirk on 4/23/18.
 */
@Document
@Getter
@Setter
public class Campaign {
    @Id
    private String id;
    private String name;
    private List<String> upcs = new ArrayList<>();
    private List<Network> networks = new ArrayList<>();
    private BigDecimal budget;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    @CreatedDate
    private LocalDateTime createdDate;
    private List<FrequencyCap> frequencyCaps;
    private String advertiserId;
    private String countryCode;
    private String attrId;
    private List<LineItem> lineItems;
    private String quarter;
    private String year;
    private String brand;
    private String productType = "MTA";
    private String tag;
    private Advertiser advertiser;
    private int campaignId;
    private String customerType;
    private int timeWindow;
    private List<String> productCategories;
    private LocalDateTime postCampaignEndDate;
    private String lastUpdatedBy;
    private List<String> newBrandUPCs = new ArrayList<>();
    private int newToBrandWeeks = 26;
    private int newToCategoryWeeks = 26;


    public List<Network> getNetworks() {
        return networks;
    }

    public void setNetworks(List<Network> networks) {
        this.networks = networks;
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getUpcs() {
        return upcs;
    }

    public void setUpcs(List<String> upcs) {
        this.upcs = upcs;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getBudget() {
        return budget;
    }

    public void setBudget(BigDecimal budget) {
        this.budget = budget;
    }

    public List<FrequencyCap> getFrequencyCaps() {
        return frequencyCaps;
    }

    public void setFrequencyCaps(List<FrequencyCap> frequencyCaps) {
        this.frequencyCaps = frequencyCaps;
    }

    public String getAdvertiserId() {
        return advertiserId;
    }

    public void setAdvertiserId(String advertiserId) {
        this.advertiserId = advertiserId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAttrId() {
        return attrId;
    }

    public void setAttrId(String attrId) {
        this.attrId = attrId;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = "MTA";
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public int getTimeWindow() {
        return timeWindow;
    }

    public void setTimeWindow(int timeWindow) {
        this.timeWindow = timeWindow;
    }

    public List<String> getProductCategories() {
        return productCategories;
    }

    public void setProductCategories(List<String> productCategories) {
        this.productCategories = productCategories;
    }

    public Advertiser getAdvertiser() {
        return advertiser;
    }

    public void setAdvertiser(Advertiser advertiser) {
        this.advertiser = advertiser;
    }

    public List<String> getNewBrandUPCs() {
        return newBrandUPCs;
    }

    public void setNewBrandUPCs(List<String> newBrandUPCs) {
        this.newBrandUPCs = newBrandUPCs;
    }

    public int getNewToBrandWeeks() {
        return newToBrandWeeks;
    }

    public void setNewToBrandWeeks(int newToBrandWeeks) {
        this.newToBrandWeeks = newToBrandWeeks;
    }

    public int getNewToCategoryWeeks() {
        return newToCategoryWeeks;
    }

    public void setNewToCategoryWeeks(int newToCategoryWeeks) {
        this.newToCategoryWeeks = newToCategoryWeeks;
    }

    public LocalDateTime getPostCampaignEndDate() {
        return postCampaignEndDate;
    }

    public void setPostCampaignEndDate(LocalDateTime postCampaignEndDate) {
        this.postCampaignEndDate = postCampaignEndDate;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }
}
