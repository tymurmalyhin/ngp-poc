package com.catalina.omni.core.hystrix.param.save;

import com.catalina.omni.core.hystrix.matcher.MatchableByEqualsIgnoreId;

import java.util.Objects;

public class InsertCommandParam<T extends MatchableByEqualsIgnoreId<T>> extends DataCommandParam<T> {

    public InsertCommandParam(T data, String collectionName) {
        super(data, collectionName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InsertCommandParam<T> that = (InsertCommandParam<T>) o;
        return Objects.equals(data.getClass(), that.getData().getClass())
                && collectionName.equals(that.collectionName)
                && data.matchIgnoreId(that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data.getClass(), data);
    }

    public String toKey() {
        return data.getClass().getSimpleName() + "_" + collectionName + "_insert";
    }
}
