package com.catalina.omni.core.domain.integration;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class PreferredLocation {

    private Double latitude;
    private Double longitude;
    private Object altitude;
    private Object relevantText;
    private Map<String, String> attributes = new HashMap<>();
    private Map<String, Object> additionalProperties = new HashMap<>();

}
