package com.catalina.omni.core.domain.integration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

/**
 * Created by ziak on 1/24/18.
 */
@Document
@Getter
@Setter
public class OmniUser {

    @Id
    private String id;
    private String countryCode;
    private String digitalId;
    private String fscId;
    private String networkId;
    private String mfdPassworksLandingURL;
    private String mfdLandingPageURL;

    @CreatedDate
    private LocalDateTime created;

    private PreferredLocation preferredLocation;
}
