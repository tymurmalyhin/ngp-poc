package com.catalina.omni.core.configuration.print;

import static java.lang.System.lineSeparator;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import java.util.Optional;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Used to verify if collections are mapped to their respective databases correctly
 */
@Component
public class MongoMappingPrinter {

  private static final Logger LOG = LoggerFactory.getLogger(MongoMappingPrinter.class);

  private final Optional<MongoClient> mfdClient;
  private final Optional<MongoClient> attributionClient;

  @Autowired
  public MongoMappingPrinter(@Qualifier("mfdClient") Optional<MongoClient> mfdClient,
      @Qualifier("attributionClient") Optional<MongoClient> attributionClient) {
    this.mfdClient = mfdClient;
    this.attributionClient = attributionClient;
  }

  @EventListener(ApplicationReadyEvent.class)
  public void onDbStartup() {
    mfdClient.ifPresent(client -> printCollectionMapping(client, "mfdClient"));
    attributionClient.ifPresent(client -> printCollectionMapping(client, "attributionClient"));
  }

  private static void printCollectionMapping(MongoClient client, String name) {
    StringBuilder messageBuilder = new StringBuilder();
    messageBuilder.append("Mongo Client ").append(name).append(lineSeparator());
    client.listDatabaseNames().forEach((Consumer<? super String>) db -> {
      String dbPrefix = "Database " + db + " :: ";
      client.getDatabase(db)
          .listCollectionNames().forEach((Block<? super String>) collection ->
          messageBuilder.append(dbPrefix).append("Collection ").append(collection)
              .append(lineSeparator()));
    });
    LOG.info("{}", messageBuilder);
  }
}
