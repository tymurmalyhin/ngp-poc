package com.catalina.omni.core.dummy;

import com.catalina.omni.core.domain.omniattribution.AttributionCIDMonitor;
import com.catalina.omni.core.domain.omniattribution.LineItemImpressionEvent;
import com.catalina.omni.core.store.attribution.AttributionCIDMonitorRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@RequestMapping("/dummy")
@Profile({"local","qa"})
public class AggregationTriggeringEndpoint implements AutoCloseable {

    private static final Logger LOG = LoggerFactory.getLogger(AggregationTriggeringEndpoint.class);

    private static final String TRIGGERED = "Triggered !";

    private final ExecutorService executorService;
    private final Random random;
    private final AttributionCIDMonitorRepo cidMonitorRepo;
    private final String[] params;

    @Autowired
    public AggregationTriggeringEndpoint(AttributionCIDMonitorRepo repo) {
        cidMonitorRepo = repo;
        executorService = Executors.newWorkStealingPool(200);
        random = new Random();
        params = new String[]{"a", "b", "c", "d"};
    }

    @GetMapping("/trigger-aggregation-requests-find/{nrOfRequests}")
    public String triggerRequestsFind(@PathVariable("nrOfRequests") int nrOfRequests) {
        final CompletableFuture[] futures = new CompletableFuture[nrOfRequests];
        final long start = System.currentTimeMillis();
        for (int i = 0; i < nrOfRequests; i++) {
            //just so there is a variance in load
            futures[i] = CompletableFuture
                    .supplyAsync(() -> cidMonitorRepo.findByCampaignIdAndCidAndTealiumEventIdAndImpressionDate(
                            params[random.nextInt(4)],
                            params[random.nextInt(4)],
                            params[random.nextInt(4)],
                            LocalDateTime.now()),
                            executorService);
        }

        CompletableFuture.allOf(futures).whenComplete((aVoid, throwable) -> {
            if(throwable != null){
                LOG.error("Error when executing find",throwable);
                return;
            }
            LOG.info("{} finds took {} ms", nrOfRequests, System.currentTimeMillis() - start);
        });

        return TRIGGERED;
    }

    @GetMapping("/trigger-aggregation-requests-insert/{nrOfRequests}")
    public String triggerRequestsInsert(@PathVariable("nrOfRequests") int nrOfRequests) {
        final CompletableFuture[] futures = new CompletableFuture[nrOfRequests];
        final long start = System.currentTimeMillis();
        for (int i = 0; i < nrOfRequests; i++) {
            //just so there is a variance in load
            int finalI = i;
            futures[i] = CompletableFuture.runAsync(() -> {
                String strId = "" + finalI;
                final AttributionCIDMonitor monitor = new AttributionCIDMonitor();
                monitor.setCid(strId);
                monitor.setDateCreated(LocalDateTime.now());
                monitor.setCampaignId(strId);

                final LineItemImpressionEvent event = new LineItemImpressionEvent();
                event.setLineItemId(strId);
                event.setImpressionDate(LocalDateTime.now());

                monitor.setLineItems(Collections.singletonList(event));
                cidMonitorRepo.save(monitor);
            }, executorService);
        }

        CompletableFuture.allOf(futures).whenComplete((aVoid, throwable) -> {
            LOG.info("{} saves took {} ms", nrOfRequests, System.currentTimeMillis() - start);
        });

        return TRIGGERED;
    }

    @GetMapping("/trigger-aggregation-requests-update/{nrOfRequests}")
    public String triggerRequestsUpdate(@PathVariable("nrOfRequests") int nrOfRequests) {
        final List<AttributionCIDMonitor> all = cidMonitorRepo.findAll();

        final CompletableFuture[] futures = new CompletableFuture[nrOfRequests];
        final long start = System.currentTimeMillis();
        for (int i = 0; i < nrOfRequests; i++) {
            futures[i] = CompletableFuture.runAsync(() -> {
                final AttributionCIDMonitor monitor = all.get(random.nextInt(all.size()));
                monitor.setCampaignId("" + 0);
                cidMonitorRepo.save(monitor);
            }, executorService);
        }

        CompletableFuture.allOf(futures).whenComplete((aVoid, throwable) -> {
            LOG.info("{} updates took {} ms", nrOfRequests, System.currentTimeMillis() - start);
        });

        return TRIGGERED;
    }

    @Override
    public void close() {
        executorService.shutdown();
    }
}
