package com.catalina.omni.core.configuration;

import com.catalina.omni.core.configuration.hystrix.HystrixCommandConfiguration;
import com.catalina.omni.core.domain.omniattribution.Attribution;
import com.catalina.omni.core.domain.omniattribution.converter.FormatToStringConverter;
import com.catalina.omni.core.domain.omniattribution.converter.StringToFormatConverter;
import com.catalina.omni.core.store.attribution.AttributionRepo;
import com.mongodb.*;
import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.domain.EntityScanner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.annotation.Persistent;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.data.auditing.IsNewAwareAuditingHandler;
import org.springframework.data.mapping.context.PersistentEntities;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.HystrixMongoTemplate;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.*;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.core.mapping.event.AuditingEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.*;
import java.util.stream.Collectors;

/* *
 * Configures all attribution repos to be saved in to omniattribution databse.
 * To save you local repos to this
 * database just use @EnableMongoRepositories(basePackages="YOUR_PCKG_WITH_REPOS",
 * mongoTemplateRef = "attributionTemplate") in your project and set mongo-db-attribution.enabled
 * */
@Configuration
@EntityScan(basePackageClasses = Attribution.class)
@EnableMongoRepositories(
    basePackageClasses = AttributionRepo.class,
    mongoTemplateRef = "attributionTemplate")
@Import(MongoCommonConfiguration.class)
@ConditionalOnProperty(value = "mongodb.omniattribution.enabled", havingValue = "true")
public class MongoAttributionConfiguration {

  private static final String ATTRIBUTION_PACKAGE = Attribution.class.getPackage().getName();

  @Value("${mongodb.omniattribution.name}")
  private String dbName;

  @Value("${mongodb.omniattribution.uri}")
  private String mongoUri;

  @ConditionalOnMissingBean(name = "attributionClient")
  @Bean(name = "attributionClient")
  public MongoClient attributionClient(MongoClientOptions mongoClientOptions) {
    return new MongoClient(new MongoClientURI(mongoUri, new MongoClientOptions.Builder(mongoClientOptions)));
  }

  @Bean(name = "attributionTemplate")
  public MongoTemplate attributionTemplate(@Qualifier("attributionFactory") MongoDbFactory factory,
                                           @Qualifier("attributionConverter") MongoConverter converter,
                                           HystrixRequestContext hystrixContext,
                                           HystrixCommandConfiguration commandConfiguration,
                                           @Qualifier("attributionAuditingHandler") AuditingHandler auditingHandler) {
    return new HystrixMongoTemplate(factory, converter, hystrixContext, commandConfiguration, auditingHandler);
  }

  @Bean(name = "attributionFactory")
  public MongoDbFactory attributionFactory(
          @Qualifier("attributionClient") MongoClient mongoClient) {
    return new SimpleMongoDbFactory(mongoClient, dbName);
  }

  @Bean(name = "attributionMapping")
  public MongoMappingContext attributionMapping(ApplicationContext applicationContext)
          throws ClassNotFoundException {
    MongoMappingContext context = new MongoMappingContext();
    Set<Class<?>> all = new EntityScanner(applicationContext)
            .scan(Document.class, Persistent.class);

    context.setInitialEntitySet(all.stream()
            .filter(clazz -> clazz.getPackage().getName().contains(ATTRIBUTION_PACKAGE))
            .collect(Collectors.toSet()));
    return context;
  }

  @Bean(name = "attributionConverter")
  public MappingMongoConverter attributionMappingMongoConverter(
          @Qualifier("attributionFactory") MongoDbFactory factory,
          @Qualifier("attributionMapping") MongoMappingContext context,
          Optional<MongoCustomConversions> conversions) {
    DbRefResolver dbRefResolver = new DefaultDbRefResolver(factory);
    MappingMongoConverter mappingConverter = new MappingMongoConverter(dbRefResolver, context);
    conversions.ifPresent(mappingConverter::setCustomConversions);
    mappingConverter.afterPropertiesSet();
    return mappingConverter;
  }

  @Bean(name = "attributionAuditingHandler")
  public IsNewAwareAuditingHandler attributionAuditingHandler(@Qualifier("attributionMapping") MongoMappingContext context) {
    return new IsNewAwareAuditingHandler(new PersistentEntities(Collections.singletonList(context)));
  }

  @Bean(name = "attributionAuditingEventListener")
  public AuditingEventListener attributionAuditingEventListener(@Qualifier("attributionAuditingHandler") IsNewAwareAuditingHandler handler) {
    return new AuditingEventListener(() -> handler) {

      @Override
      public void onApplicationEvent(BeforeConvertEvent<Object> event) {
        if (event.getSource().getClass().getPackage().getName().contains(ATTRIBUTION_PACKAGE)) {
          super.onApplicationEvent(event);
        }
      }
    };
  }

  @Bean
  public MongoCustomConversions customConversions() {
    List<Converter> customConverters = Collections.unmodifiableList(Arrays.asList(new StringToFormatConverter(),
            new FormatToStringConverter()));
    return new MongoCustomConversions(customConverters);
  }
}
