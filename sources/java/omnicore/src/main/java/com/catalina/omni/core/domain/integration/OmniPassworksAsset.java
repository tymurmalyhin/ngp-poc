package com.catalina.omni.core.domain.integration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by ziak on 1/24/18.
 */
@Document
@Getter
@Setter
public class OmniPassworksAsset {

    @Id
    private String id;
    private String promotionId;
    private String stripAssetId;
}
