package com.catalina.omni.core.hystrix.matcher;

import java.time.LocalDateTime;

public interface DateCreatedAware {

    void setDateCreated(LocalDateTime date);

    LocalDateTime getDateCreated();
}
