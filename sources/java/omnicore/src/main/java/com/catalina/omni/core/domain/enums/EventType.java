package com.catalina.omni.core.domain.enums;

public enum EventType implements BaseEnum {
    PASSWORKS("Passworks", 1);
    private String name;
    private int id;


    EventType(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int getId() {
        return id;
    }

    public static EventType getById(int id) {
        for (EventType event : EventType.values()) {
            if (event.getId() == id) {
                return event;
            }
        }
        return null;
    }

}
