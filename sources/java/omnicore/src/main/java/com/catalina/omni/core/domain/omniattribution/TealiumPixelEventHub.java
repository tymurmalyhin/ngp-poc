package com.catalina.omni.core.domain.omniattribution;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@JsonIgnoreProperties(ignoreUnknown = true)
public class TealiumPixelEventHub {

    @JsonProperty("account")
    private String account;
    @JsonProperty("profile")
    private String profile;
    @JsonProperty("env")
    private String env;
    @JsonProperty("data")
    private TealiumPixelEventHubData data;
    @JsonProperty("post_time")
    private Long postTime;
    @JsonProperty("useragent")
    private String userAgent;
    @JsonProperty("event_id")
    private String eventId;
    @JsonProperty("visitor_id")
    private String visitorId;
}