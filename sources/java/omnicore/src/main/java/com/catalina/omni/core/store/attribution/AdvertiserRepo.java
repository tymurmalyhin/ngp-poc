package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.domain.omniattribution.Advertiser;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

@JaversSpringDataAuditable
public interface AdvertiserRepo extends MongoRepository<Advertiser, String> {
    Page<Advertiser> findByNameLikeIgnoreCase(Pageable pageable, String name);
}
