package com.catalina.omni.core.domain.integration;

import com.catalina.omni.core.domain.enums.CountryEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

/**
 * This class represents a token usable for example in /dmp/rest/events/collect to identify touchpoint
 */
@Document
@Getter
@Setter
@Accessors(chain = true)
public class TouchpointToken {

    @Id
    private String id;

    private String name;

    private CountryEnum country;

    private String networkId;

    @CreatedDate
    private LocalDateTime createdAt;
}
