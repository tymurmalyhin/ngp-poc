package com.catalina.omni.core.configuration;

import com.catalina.omni.core.domain.integration.OmniOffer;
import com.catalina.omni.core.store.integration.OmniOfferRepo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.domain.EntityScanner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.annotation.Persistent;
import org.springframework.data.auditing.IsNewAwareAuditingHandler;
import org.springframework.data.mapping.context.PersistentEntities;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.core.mapping.event.AuditingEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/* *
 * Configures all mfd repos to be saved in to omnimfd databse.
 *
 * To enable MFD database in you project you need to
 * <li> @Import(MongoMfdConfiguration.class)
 * <li> mongodb.omnimfd.enabled = true - this options was introduced for projects that have
 * complex configuration and would have problems with something picking up this configuration.
 * <li> mongodb.omnimfd.uri = DB_URI - This is necessary as in order for this to work, there has
 * to be distinct MongoClient for every DB. Or at least up this point i have'nt found a way to make
 * this work with single MongoClient
 * */
@Configuration
@EntityScan(basePackageClasses = OmniOffer.class)
@EnableMongoRepositories(
        basePackageClasses = OmniOfferRepo.class,
        mongoTemplateRef = "mfdTemplate")
@Import(MongoCommonConfiguration.class)
@ConditionalOnProperty(value = "mongodb.omnimfd.enabled", havingValue = "true")
public class MongoMfdConfiguration {

    private static final String OFFER_PACKAGE = OmniOffer.class.getPackage().getName();

    @Value("${mongodb.omnimfd.name}")
    private String dbName;

    @Value("${mongodb.omnimfd.uri}")
    private String mongoUri;

    @ConditionalOnMissingBean(name = "mfdClient")
    @Bean(name = "mfdClient")
    public MongoClient mfdClient(MongoClientOptions mongoClientOptions) {
        return new MongoClient(new MongoClientURI(mongoUri, new MongoClientOptions.Builder(mongoClientOptions)));
    }

    @Bean(name = "mfdTemplate")
    public MongoTemplate mfdTemplate(@Qualifier("mfdFactory") MongoDbFactory factory,
      @Qualifier("mfdConverter") MongoConverter converter) {
    //NOTE - it's crucial to use this constructor with converter as it contains mapping context
    //which is required for auditing(@EnableMongoAuditing)
    return new MongoTemplate(factory, converter);
    }

    /**
     * Necessary to assign converter aka domain context to the connected client
     */
    @Bean(name = "mfdFactory")
    public MongoDbFactory mfdFactory(@Qualifier("mfdClient") MongoClient mongoClient) {
        return new SimpleMongoDbFactory(mongoClient, dbName);
    }

    /**
     * This bean holds mapping of domain objects, has to be configured this way to pickup just the
     * MFD domain objects.This way when it's assigned to mongo client connected to mfd database,
     * only the correct domain objects will be created.
     */
    @Bean(name = "mfdMapping")
    public MongoMappingContext mfdMapping(ApplicationContext applicationContext)
            throws ClassNotFoundException {
        MongoMappingContext context = new MongoMappingContext();
        Set<Class<?>> all = (new EntityScanner(applicationContext))
                .scan(Document.class, Persistent.class);

        context.setInitialEntitySet(all.stream()
                .filter(
                        //exact match of package so it does not take attribution subpackage
                        clazz -> clazz.getPackage().getName().contains(OFFER_PACKAGE))
                .collect(Collectors.toSet()));
        return context;
    }

    @Bean(name = "mfdConverter")
    public MappingMongoConverter mfdMappingMongoConverter(
            @Qualifier("mfdFactory") MongoDbFactory factory,
            @Qualifier("mfdMapping") MongoMappingContext context,
            Optional<MongoCustomConversions> conversions) {
        DbRefResolver dbRefResolver = new DefaultDbRefResolver(factory);
        MappingMongoConverter mappingConverter = new MappingMongoConverter(dbRefResolver, context);
        conversions.ifPresent(mappingConverter::setCustomConversions);
        return mappingConverter;
    }

    @Bean(name = "mfdAuditingHandler")
    public IsNewAwareAuditingHandler mfdAuditingHandler(@Qualifier("mfdMapping") MongoMappingContext context) {
        return new IsNewAwareAuditingHandler(new PersistentEntities(Collections.singletonList(context)));
    }

    @Bean(name = "mfdAuditingEventListener")
    public AuditingEventListener mfdAuditingListener(@Qualifier("mfdAuditingHandler") IsNewAwareAuditingHandler handler) {
        return new AuditingEventListener(() -> handler) {

            @Override
            public void onApplicationEvent(BeforeConvertEvent<Object> event) {
                if (event.getSource().getClass().getPackage().getName().contains(OFFER_PACKAGE)) {

                    super.onApplicationEvent(event);
                }
            }
        };
    }
}
