package com.catalina.omni.core;

import org.javers.spring.boot.mongo.JaversMongoAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class, JaversMongoAutoConfiguration.class})
public class OmniCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(OmniCoreApplication.class, args);
    }
}
