package com.catalina.omni.core.domain.solrresponse;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.ToString;

/**
 * Represent a list of {@linkplain SolrDoc} returned from a search.
 * This includes position and offset information.
 */
@Getter
@ToString
public class SolrData {

    @Expose
    private Long numFound;
    @Expose
    private Long start;
    @Expose
    private Double maxScore;

    @JsonProperty("docs")
    private List<SolrDoc> docs;

    public SolrData() {
        // Empty default constructor
    }
}