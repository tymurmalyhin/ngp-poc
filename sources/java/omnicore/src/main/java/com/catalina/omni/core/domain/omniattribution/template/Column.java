package com.catalina.omni.core.domain.omniattribution.template;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Column {
    private String name;
    private String field;
    private String type;
    private String lookup;
    private String defaultvalue;
}
