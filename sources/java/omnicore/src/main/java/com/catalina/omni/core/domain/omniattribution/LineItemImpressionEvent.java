package com.catalina.omni.core.domain.omniattribution;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class LineItemImpressionEvent implements Serializable {

    /** serialVersionUID */
    private static final long serialVersionUID = -1875860621295088624L;

    private String lineItemId;
    private LocalDateTime impressionDate;
    private String eventId;
    private String tealiumEventId;
    private Long version;
    private Boolean suppressed;
    private Boolean retargeted;
    private String creativeId;
}