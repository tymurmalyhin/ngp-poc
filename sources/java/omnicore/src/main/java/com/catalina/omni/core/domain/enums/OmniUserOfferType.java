package com.catalina.omni.core.domain.enums;

public enum OmniUserOfferType {
    MFD, TL2C, CRITEO, STATIC
}
