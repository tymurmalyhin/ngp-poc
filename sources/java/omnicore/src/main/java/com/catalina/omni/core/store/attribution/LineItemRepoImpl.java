package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.domain.omniattribution.Campaign;
import com.catalina.omni.core.domain.omniattribution.LineItem;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.ArrayOperators.Filter.filter;

public class LineItemRepoImpl  implements LineItemRepoCustom{

    @Autowired
    @Qualifier("attributionTemplate")
    private MongoTemplate mongoTemplate;

    public List<LineItem> findAllFromCurrentCampaignWithSuppressionOrRetargetSegments() {
        Date now = new Date();
        DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        BsonArray conditions = new BsonArray();
        BsonArray segmentCond = new BsonArray();
        BsonArray suppressionCond = new BsonArray();
        BsonArray retargetingCond = new BsonArray();
        segmentCond.add(new BsonDocument("$and", suppressionCond));
        segmentCond.add(new BsonDocument("$and", retargetingCond));
        suppressionCond.add(new BsonDocument("$gt", BsonArray.parse("[\"$$lineItem.suppressionSegmentKey\",0]")));
        suppressionCond.add(new BsonDocument("$eq", BsonArray.parse("[\"$$lineItem.suppressSegment\",true]")));
        retargetingCond.add(new BsonDocument("$gt", BsonArray.parse("[\"$$lineItem.retargetingSegmentKey\",0]")));
        retargetingCond.add(new BsonDocument("$eq", BsonArray.parse("[\"$$lineItem.retargetingSegment\",true]")));
        conditions.add(new BsonDocument("$lte", BsonArray.parse("[\"$$lineItem.startDate\",  new ISODate(\"" + fmt.format(now) +  "\")]}")));
        conditions.add(new BsonDocument("$gte", BsonArray.parse("[\"$$lineItem.endDate\",  new ISODate(\"" + fmt.format(now) +  "\")]}")));
        conditions.add(new BsonDocument("$or", segmentCond));
        Aggregation aggregation =  newAggregation(match(Criteria.where("lineItems")
                .elemMatch(Criteria.where("startDate").lte(now)
                .and("endDate").gte(now)
                .orOperator(Criteria.where("suppressionSegmentKey").exists(true).and("suppressSegment").is(true),
                Criteria.where("retargetingSegmentKey").exists(true).and("retargetingSegment").is(true)))),
                project("lineItems")
                .and(filter("lineItems")
                .as("lineItem")
                .by(new Document("$and", conditions))).as("lineItems"));
        List<Campaign> campaigns = mongoTemplate.aggregate(aggregation, "campaign", Campaign.class).getMappedResults();
        return campaigns.stream().flatMap(campaign ->   campaign.getLineItems().stream()).collect(Collectors.toList());
        }



}
