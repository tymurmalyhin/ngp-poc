package com.catalina.omni.core.domain.enums;

public enum OmniOfferState {
    ACTIVE, CAPPED, INACTIVE
}
