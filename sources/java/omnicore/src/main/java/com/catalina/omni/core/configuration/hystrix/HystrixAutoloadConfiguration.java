package com.catalina.omni.core.configuration.hystrix;


import com.netflix.config.ConfigurationManager;
import org.apache.commons.configuration.AbstractConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.env.EnvironmentUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;

/**
 * This configuration automatically pick's up standard configuration properties for hystrix
 */
@Configuration
public class HystrixAutoloadConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(HystrixAutoloadConfiguration.class);

    @Autowired
    private Environment environment;

    @EventListener(classes = {ContextRefreshedEvent.class})
    public void handleMultipleEvents() {
        AbstractConfiguration baseConfiguration = ConfigurationManager.getConfigInstance();

        EnvironmentUtils.getSubProperties(environment, "hystrix")
                .forEach((key, value) -> {
                    LOG.info("Setting property hystrix.{} to value {}", key, value);
                    baseConfiguration.setProperty("hystrix." + key, value);
                });
    }

}
