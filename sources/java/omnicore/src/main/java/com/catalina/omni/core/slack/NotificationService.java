package com.catalina.omni.core.slack;

public interface NotificationService {

    void sendMessage(String message);
    void sendMessage(String message, String source);

}
