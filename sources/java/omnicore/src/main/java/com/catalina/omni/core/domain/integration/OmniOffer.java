package com.catalina.omni.core.domain.integration;


import com.catalina.omni.core.domain.enums.OmniOfferSource;
import com.catalina.omni.core.domain.enums.OmniOfferState;
import com.catalina.omni.core.domain.enums.OmniUserOfferType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ziak on 3/23/18.
 */
@Document
@Getter
@Setter
@ToString
public class OmniOffer {

    @Id
    private String id;

    private String omniOfferId;// (vault promotion id, or cellfire offerid) this is old OmniUserOffer offerId
    private String clientOfferId;// (this is the retailer's id)
    private String campaingId;
    private String networkId;
    private OmniUserOfferType offerType;
    private String creativeURL;
    private Map<String, String> creativeVariables = new HashMap<>();
    private OmniOfferState state;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private OmniOfferSource source;
    private String campaignName;
    private List<String> upcs;
    private Integer rank = 0;

    @CreatedDate
    private LocalDateTime created;

    private LocalDateTime mfdIngestionToVault;
    private String gs1CompanyPrefix;
    private Double discountAmount;
    private String brand;
}
