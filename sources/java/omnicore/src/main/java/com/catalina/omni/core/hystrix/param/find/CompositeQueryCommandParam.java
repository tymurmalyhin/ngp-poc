package com.catalina.omni.core.hystrix.param.find;

import org.bson.Document;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Allows to differ between requests on different tables/queries
 */
public class CompositeQueryCommandParam<T> {

  private final Class<T> entityClass;
  private final String collectionName;
  private final Document query;

  public CompositeQueryCommandParam(Class<T> entityClass, String collectionName, Document query) {
    this.entityClass = entityClass;
    this.collectionName = collectionName;
    this.query = query;
  }

  private static Stream<Object> getAllKeys(Document query) {
    return query.keySet().stream()
        .flatMap(o -> {
          final Object obj = query.get(o);
          if (obj instanceof Document) {
            return Stream.concat(Stream.of(o), getAllKeys((Document) obj));
          } else if (obj instanceof List) {
            return Stream.concat(Stream.of(o),
                ((List) obj).stream().flatMap(o1 -> getAllKeys((Document) o1)));
          } else {
            return Stream.of(o);
          }
        });
  }

  public Document getQuery() {
    return query;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CompositeQueryCommandParam<?> that = (CompositeQueryCommandParam<?>) o;
    return Objects.equals(entityClass, that.entityClass) &&
            Objects.equals(collectionName, that.collectionName) &&
            Objects.equals(query, that.query);
  }

  @Override
  public int hashCode() {
    return Objects.hash(entityClass, collectionName, query);
  }

  /**
   * Takes entity class name and query qualifiers and creates unique key from it
   */
  public String toKey() {
    return entityClass.getName() + "_" + collectionName + "_" + getAllKeys(query).map(Object::toString)
            .collect(Collectors.joining("_"));
  }
}
