package com.catalina.omni.core.domain.integration;

import lombok.Getter;
import lombok.Setter;

import java.time.DayOfWeek;
import java.time.LocalTime;

@Getter
@Setter
public class DayTime {
    private DayOfWeek day;
    private LocalTime time;

    public DayTime() {
    }

    public DayTime(DayOfWeek day, LocalTime time) {
        this.day = day;
        this.time = time;
    }
}
