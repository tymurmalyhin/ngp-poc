package com.catalina.omni.core.store.integration;

import com.catalina.omni.core.domain.enums.OmniUserOfferType;
import com.catalina.omni.core.domain.integration.OmniUserOffer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ziak on 1/24/18.
 */
@Repository
public interface OmniUserOfferRepo extends MongoRepository<OmniUserOffer, String> {

    List<OmniUserOffer> findByCountryCodeAndNetworkIdAndDigitalId(String countryCode, String networkId, String digitalId);

    List<OmniUserOffer> findByCountryCodeAndNetworkIdAndDigitalIdAndOmniOfferOfferTypeOrderByOmniOfferRankDesc(
            String countryCode, String networkId, String digitalId, OmniUserOfferType offerType);

    List<OmniUserOffer> findByCountryCodeAndNetworkIdAndFscIdAndOmniOfferOfferTypeOrderByOmniOfferRankDesc(
            String countryCode, String networkId, String fscId, OmniUserOfferType offerType);

    List<OmniUserOffer> findByOmniOfferOmniOfferIdInAndCountryCodeAndNetworkIdAndDigitalIdAndOmniOfferOfferType(
            List<String> omniOfferIds, String countryCode, String networkId, String digitalId, OmniUserOfferType offerType);

    List<OmniUserOffer> findByCountryCodeAndNetworkIdAndFscIdAndOmniOfferOfferType(
            String countryCode, String networkId, String fscId, OmniUserOfferType offerType);

    int deleteByCreatedBefore(@Param("date") LocalDateTime date);

    int deleteByCreatedIsNull();

    List<OmniUserOffer> findByCreatedBefore(@Param("date") LocalDateTime date);
}
