package com.catalina.omni.core.hystrix.command.attribution.cidmonitor.save;

import com.catalina.omni.core.configuration.hystrix.operations.Update;
import com.catalina.omni.core.hystrix.param.save.BulkUpdateCommandParam;
import com.netflix.hystrix.*;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Collection;

public class BulkUpdateOperationCollapser<T>
        extends HystrixCollapser<Iterable<T>, T, BulkUpdateCommandParam<T>> {

    private final BulkUpdateCommandParam<T> param;
    private final HystrixCommand.Setter commandSetter;
    private final MongoTemplate mongoTemplate;
    private final Class<T> domainClass;

    public BulkUpdateOperationCollapser(BulkUpdateCommandParam<T> param, Update params, MongoTemplate mongoTemplate) {
        super(HystrixCollapser.Setter.withCollapserKey(
                HystrixCollapserKey.Factory.asKey(param.toKey()))
                .andScope(Scope.GLOBAL)
                .andCollapserPropertiesDefaults(HystrixCollapserProperties.Setter()
                        .withTimerDelayInMilliseconds(params.getTimeWindowSize())
                        .withMaxRequestsInBatch(params.getMaxRequestsInBatch())));
        this.param = param;
        this.mongoTemplate = mongoTemplate;
        final String key = param.toKey();
        this.commandSetter = HystrixCommand.Setter
                .withGroupKey(HystrixCommandGroupKey.Factory.asKey(key))
                .andCommandKey(HystrixCommandKey.Factory.asKey(key))
                .andCommandPropertiesDefaults(
                        HystrixCommandProperties.Setter()
                                .withCircuitBreakerEnabled(true)
                                .withCircuitBreakerErrorThresholdPercentage(
                                        params.getCircuitBreakerErrorThresholdPercentage())
                                .withCircuitBreakerSleepWindowInMilliseconds(
                                        params.getSleepWindowInMilliseconds())
                                .withExecutionIsolationSemaphoreMaxConcurrentRequests(10000)
                                .withExecutionIsolationStrategy(HystrixCommandProperties.ExecutionIsolationStrategy.SEMAPHORE)
                                .withExecutionTimeoutEnabled(false)
                                .withRequestLogEnabled(false));
        domainClass = (Class<T>) this.param.getEntityClass();
    }

    @Override
    public BulkUpdateCommandParam<T> getRequestArgument() {
        return param;
    }

    @Override
    protected HystrixCommand<Iterable<T>> createCommand(Collection<CollapsedRequest<T, BulkUpdateCommandParam<T>>> collection) {
        return new HystrixCommand<Iterable<T>>(commandSetter) {
            @Override
            protected Iterable<T> run() throws Exception {
                final BulkOperations bulkOps = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, domainClass, param.getCollectionName());

                for (CollapsedRequest<T, BulkUpdateCommandParam<T>> collapsedRequest : collection) {
                    BulkUpdateCommandParam<T> arg = collapsedRequest.getArgument();

                    if (arg.isUpsert()) {
                        bulkOps.upsert(arg.getQuery(), arg.getUpdate());
                    } else {
                        if (arg.isMulti()) {
                            bulkOps.updateMulti(arg.getQuery(), arg.getUpdate());
                        } else {
                            bulkOps.updateOne(arg.getQuery(), arg.getUpdate());
                        }
                    }
                }

                bulkOps.execute();
                return null; // no result expected
            }
        };
    }

    @Override
    protected void mapResponseToRequests(Iterable<T> responses, Collection<CollapsedRequest<T, BulkUpdateCommandParam<T>>> requests) {
        requests.forEach(request -> {
            request.setResponse(null);
            request.setComplete();
        });
    }

}
