package com.catalina.omni.core.configuration.javers;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Aspect was previously configured before exclusion of default auto configuration was done, this way it
 * will work with beans that we defined.
 */
@Configuration
@AutoConfigureAfter(value = {JaversMfdConfiguration.class, JaversAttributionConfiguration.class})
@EnableAspectJAutoProxy
public class AspectEnablingConfiguration {
}
