package com.catalina.omni.core.hystrix.param.find.factory;

import com.catalina.omni.core.hystrix.param.find.CompositeQueryCommandParam;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import com.google.common.base.Strings;
import org.bson.Document;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.util.TypeInformation;

import static com.google.common.base.CaseFormat.LOWER_CAMEL;
import static com.google.common.base.CaseFormat.UPPER_CAMEL;

/**
 * Factory for providing params for the aggregated requests. Fully automatic registration for all
 * domain classes in mapping context, so no need to register anything here when adding support for
 * new query
 */
public class QueryCommandParamFactory {

  private final Map<Class<?>, Function<Document, CompositeQueryCommandParam<?>>> queryParamSupplierRegistry;

  public QueryCommandParamFactory(final MongoConverter mongoConverter) {
    queryParamSupplierRegistry = new HashMap<>();

    mongoConverter.getMappingContext().getManagedTypes()
        .forEach(typeInformation -> {
          final Class<?> type = Optional.ofNullable(typeInformation.getActualType())
              .map(TypeInformation::getType)
              .orElseThrow(IllegalArgumentException::new);

            queryParamSupplierRegistry
              .put(type, document -> new CompositeQueryCommandParam<>(type, extractCollectionName(type), document));
        });
  }

  public <T> CompositeQueryCommandParam<T> createQueryCommandParam(Class<T> entityClass,
      Document query) {
    return (CompositeQueryCommandParam<T>) Optional
        .ofNullable(queryParamSupplierRegistry.get(entityClass))
        .map(function -> function.apply(query))
        .orElse(null);
  }

  public static String extractCollectionName(Class<?> type){
      return Optional.ofNullable(type.getAnnotation(org.springframework.data.mongodb.core.mapping.Document.class))
              .map(org.springframework.data.mongodb.core.mapping.Document::collection)
              .map(Strings::emptyToNull)
              .orElse(UPPER_CAMEL.to(LOWER_CAMEL, type.getSimpleName()));
  }
}
