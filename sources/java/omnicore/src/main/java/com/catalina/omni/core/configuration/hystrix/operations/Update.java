package com.catalina.omni.core.configuration.hystrix.operations;

import com.catalina.omni.core.configuration.hystrix.DefaultHystrixCommandSettings;

public final class Update extends DefaultHystrixCommandSettings {

    private Boolean aggregationEnabled;

    public Boolean isAggregationEnabled() {
        return aggregationEnabled;
    }

    public void setAggregationEnabled(Boolean aggregationEnabled) {
        this.aggregationEnabled = aggregationEnabled;
    }
}
