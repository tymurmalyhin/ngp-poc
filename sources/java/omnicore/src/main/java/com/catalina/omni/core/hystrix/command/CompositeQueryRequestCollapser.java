package com.catalina.omni.core.hystrix.command;

import com.catalina.omni.core.configuration.hystrix.operations.Find;
import com.catalina.omni.core.hystrix.param.find.CompositeQueryCommandParam;
import com.netflix.hystrix.*;
import com.netflix.hystrix.HystrixCommandProperties.ExecutionIsolationStrategy;
import org.bson.Document;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.google.common.base.Preconditions.checkState;

/**
 * This is a template on for composite query aggregation
 */
public abstract class CompositeQueryRequestCollapser<T>
    extends HystrixCollapser<Iterable<T>, List<T>, CompositeQueryCommandParam<T>> {

  private final CompositeQueryCommandParam<T> param;
  private final MongoTemplate template;
  private final Class<T> entityClass;
  private final HystrixCommand.Setter commandSetter;
  private final BiFunction<CompositeQueryCommandParam<T>, T, Boolean> resultMatcher;

  /**
   * @param param - Parameter that defines input arguments for the query
   * @param template Mongo template to perform the db operations
   * @param entityClass Type of result that this query returns
   * @param resultMatcher Function to match result to original request
   * @param queryParams settings for this particular query
   */
  protected CompositeQueryRequestCollapser(CompositeQueryCommandParam<T> param,
      MongoTemplate template,
      Class<T> entityClass,
      BiFunction<CompositeQueryCommandParam<T>, T, Boolean> resultMatcher,
      Find queryParams) {
    super(HystrixCollapser.Setter.withCollapserKey(
        HystrixCollapserKey.Factory.asKey(param.toKey()))
        .andScope(Scope.GLOBAL)
        .andCollapserPropertiesDefaults(HystrixCollapserProperties.Setter()
            .withTimerDelayInMilliseconds(queryParams.getTimeWindowSize())
            .withMaxRequestsInBatch(queryParams.getMaxRequestsInBatch())));
    checkState(entityClass instanceof Serializable, "% must be Serializable", entityClass);
    this.param = param;
    this.template = template;
    this.entityClass = entityClass;
    this.resultMatcher = resultMatcher;
    final String key = param.toKey();
    this.commandSetter = HystrixCommand.Setter
        .withGroupKey(HystrixCommandGroupKey.Factory.asKey(key))
        .andCommandKey(HystrixCommandKey.Factory.asKey(key))
        .andCommandPropertiesDefaults(
            HystrixCommandProperties.Setter()
                .withCircuitBreakerEnabled(true)
                .withCircuitBreakerErrorThresholdPercentage(
                    queryParams.getCircuitBreakerErrorThresholdPercentage())
                .withCircuitBreakerSleepWindowInMilliseconds(
                    queryParams.getSleepWindowInMilliseconds())
                .withExecutionIsolationSemaphoreMaxConcurrentRequests(10000)
                .withExecutionIsolationStrategy(ExecutionIsolationStrategy.SEMAPHORE)
                .withExecutionTimeoutEnabled(false)
                .withRequestLogEnabled(false));
  }

  @Override
  public CompositeQueryCommandParam<T> getRequestArgument() {
    return param;
  }

  @Override
  protected HystrixCommand<Iterable<T>> createCommand(
      Collection<CollapsedRequest<List<T>, CompositeQueryCommandParam<T>>> collection) {
    return new HystrixCommand<Iterable<T>>(commandSetter) {
      @Override
      protected Iterable<T> run() throws Exception {

        //cannot use Criteria#is as it takes a loong time to execute on cosmo db

        final List<Document> partialDocs = collection.stream()
                .map(CollapsedRequest::getArgument)
                .map(CompositeQueryCommandParam::getQuery)
                .map(partial -> {
                  final List<Document> queryParts = partial.entrySet().stream()
                          .map(entry -> new Document(entry.getKey(), entry.getValue()))
                          .collect(Collectors.toList());

                  final Document doc = new Document();
                  doc.put("$and", queryParts);
                  return doc;
                })
                .collect(Collectors.toList());

        final Document queryDoc = new Document();
        queryDoc.put("$or", partialDocs);

        return template.find(new BasicQuery(queryDoc), entityClass);
      }
    };
  }

  @Override
  protected void mapResponseToRequests(Iterable<T> results,
      Collection<CollapsedRequest<List<T>, CompositeQueryCommandParam<T>>> requests) {
    requests.forEach(request -> {
      request.setResponse(StreamSupport.stream(results.spliterator(), false)
          .filter(result -> resultMatcher.apply(request.getArgument(), result))
          .collect(Collectors.toList()));
      request.setComplete();
    });
  }
}
