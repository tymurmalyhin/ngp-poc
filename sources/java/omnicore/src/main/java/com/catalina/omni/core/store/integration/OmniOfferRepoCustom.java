package com.catalina.omni.core.store.integration;


import com.catalina.omni.core.domain.enums.OmniOfferState;
import com.catalina.omni.core.domain.integration.OmniOffer;

import java.time.LocalDateTime;
import java.util.List;

public interface OmniOfferRepoCustom {


    List<OmniOffer> findUniqueByBrandAndNotDigestedBy(OmniOfferState state, String andNetworkId,
                                                      LocalDateTime from, LocalDateTime to);


}
