package com.catalina.omni.core.domain.integration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.google.common.collect.ImmutableMap;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Map;
import java.util.Objects;

import static com.google.common.base.Preconditions.checkNotNull;

@Getter
@Builder
@ToString
public class ClipRequest {

    public static final String SYSTEM_ID = "OMNI";

    private final Integer componentId;
    private final String cid;
    @Getter(onMethod_ = {@JsonSerialize(using = LocalDateTimeSerializer.class), @JsonDeserialize(using = LocalDateTimeDeserializer.class)})
    private final LocalDateTime dateOfClip;

    @JsonCreator
    ClipRequest(@JsonProperty("componentId") Integer componentId,
                @JsonProperty("cid") String cid,
                @JsonProperty("dateOfClip") LocalDateTime dateOfClip) {
        this.componentId = checkNotNull(componentId, "componentId cannot be null");
        this.cid = checkNotNull(cid, "cid cannot be null");
        this.dateOfClip = checkNotNull(dateOfClip, "dateOfClip cannot be null");
    }

    public Map<String, String> toParamsMap() {
        return ImmutableMap.<String, String>builder()
                .put("system_id", SYSTEM_ID)
                .put("cid", cid)
                .put("t", "clip")
                .put("timestamp", String.valueOf(dateOfClip.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()))
                .build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClipRequest that = (ClipRequest) o;
        return Objects.equals(componentId, that.componentId) &&
                Objects.equals(cid, that.cid) &&
                Objects.equals(dateOfClip, that.dateOfClip);
    }

    @Override
    public int hashCode() {
        return Objects.hash(componentId, cid, dateOfClip);
    }


}
