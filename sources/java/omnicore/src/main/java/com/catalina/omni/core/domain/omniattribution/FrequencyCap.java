package com.catalina.omni.core.domain.omniattribution;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FrequencyCap {
    private Long duration;
    private int impressions;
}
