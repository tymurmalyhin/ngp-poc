package com.catalina.omni.core.domain.omniattribution.converter;

import com.catalina.omni.core.domain.omniattribution.Format;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalConverter;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;


@WritingConverter
public class FormatToStringConverter implements ConditionalConverter, Converter<Format, String> {

    @Override
    public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
        return String.class.getName().equals(targetType.getName())
                && Format.class.getName().equals(sourceType.getType().getName());
    }

    @Override
    public String convert(Format source) {
        return source.getName();
    }
}
