package com.catalina.omni.core.hystrix.matcher;

public interface IdAware {

    String getId();

    void setId(String id);
}
