package com.catalina.omni.core.store.integration;

import com.catalina.omni.core.domain.integration.OmniOfferTargeted;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by ziak on 9/26/18.
 */
@Repository
public interface OmniOfferTargetedRepo extends MongoRepository<OmniOfferTargeted, String> {

    List<OmniOfferTargeted> findByNetworkIdAndClientOfferId(String networkId, String clientOfferId);

    Optional<OmniOfferTargeted> findByOmniOfferId(String omniOfferId);
}
