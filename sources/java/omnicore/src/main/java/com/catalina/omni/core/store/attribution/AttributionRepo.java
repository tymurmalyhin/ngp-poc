package com.catalina.omni.core.store.attribution;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.catalina.omni.core.domain.omniattribution.Attribution;

import java.util.List;

@Repository
public interface AttributionRepo extends MongoRepository<Attribution, String>, AttributionRepoCustom{

    Attribution findByCidAndCampaignId(String cid, String campaignId);
    List<Attribution> getByCampaignId(String campaignId);
    List<Attribution> getByCid(String cid);

}
