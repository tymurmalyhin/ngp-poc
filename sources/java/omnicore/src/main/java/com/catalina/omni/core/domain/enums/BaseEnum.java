package com.catalina.omni.core.domain.enums;

import java.io.Serializable;

public interface BaseEnum extends Serializable {

    int getId();
}
