package com.catalina.omni.core.domain.enums;

public enum EventAction implements BaseEnum {
    CLIP(1), UNCLIP(2), REDIRECT(3);

    private int id;

    EventAction(int id) {
        this.id = id;
    }

    @Override
    public int getId() {
        return id;
    }

    public String getName() {
        return this.name().toLowerCase();

    }

    public static EventAction getById(int id) {
        for (EventAction event : EventAction.values()) {
            if (event.getId() == id) {
                return event;
            }
        }
        return null;
    }
}
