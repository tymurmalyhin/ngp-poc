package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.domain.omniattribution.LineItem;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

@JaversSpringDataAuditable
public interface LineItemRepo extends MongoRepository<LineItem, String> , LineItemRepoCustom {
    Page<LineItem> findByCampaignId(Pageable pageable, String campaignId);
    Page<LineItem> findByCampaignIdAndNameLikeIgnoreCase(Pageable pageable, String campaignId, String name);
    List<LineItem> findByCampaignId(String campaignId);
}
