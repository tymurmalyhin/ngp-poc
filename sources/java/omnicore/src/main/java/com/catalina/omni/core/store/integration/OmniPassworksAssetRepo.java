package com.catalina.omni.core.store.integration;

import com.catalina.omni.core.domain.integration.OmniPassworksAsset;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by ziak on 1/24/18.
 */
@Repository
public interface OmniPassworksAssetRepo extends MongoRepository<OmniPassworksAsset, String> {

    OmniPassworksAsset findByPromotionId(String promotionId);

}