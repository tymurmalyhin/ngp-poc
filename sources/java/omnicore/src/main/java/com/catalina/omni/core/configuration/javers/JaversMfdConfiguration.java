package com.catalina.omni.core.configuration.javers;

import com.catalina.omni.core.configuration.MongoMfdConfiguration;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.repository.api.JaversRepository;
import org.javers.repository.mongo.MongoRepository;
import org.javers.spring.auditable.AuthorProvider;
import org.javers.spring.auditable.CommitPropertiesProvider;
import org.javers.spring.auditable.EmptyPropertiesProvider;
import org.javers.spring.auditable.aspect.JaversAuditableAspect;
import org.javers.spring.auditable.aspect.springdata.JaversSpringDataAuditableRepositoryAspect;
import org.javers.spring.boot.mongo.JaversMongoProperties;
import org.javers.spring.mongodb.DBRefUnproxyObjectAccessHook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({MongoMfdConfiguration.class, JaversCommonConfiguration.class})
@ConditionalOnProperty(value = "mongodb.omnimfd.enabled", havingValue = "true")
@EnableConfigurationProperties({JaversMongoProperties.class})
public class JaversMfdConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(JaversMfdConfiguration.class);

    private final JaversMongoProperties javersMongoProperties;

    private final MongoClient mongoClient;
    private final MongoProperties mongoProperties;

    @Autowired
    public JaversMfdConfiguration(JaversMongoProperties javersMongoProperties,
                                  @Qualifier("mfdClient") MongoClient mongoClient,
                                  MongoProperties mongoProperties) {
        this.javersMongoProperties = javersMongoProperties;
        this.mongoClient = mongoClient;
        this.mongoProperties = mongoProperties;
    }

    @Bean(name = {"mfdJaversFromStarter"})
    public Javers javers() {
        LOG.info("Starting javers-spring-boot-starter-mongo for mfd ...");
        MongoDatabase mongoDatabase = this.mongoClient.getDatabase(this.mongoProperties.getMongoClientDatabase());
        LOG.info("connecting to database: {}", this.mongoProperties.getMongoClientDatabase());
        JaversRepository javersRepository = new MongoRepository(mongoDatabase);
        return JaversBuilder.javers().registerJaversRepository(javersRepository).withProperties(this.javersMongoProperties).withObjectAccessHook(new DBRefUnproxyObjectAccessHook()).build();
    }

    @Bean(name = {"mfdEmptyPropertiesProvider"})
    public CommitPropertiesProvider commitPropertiesProvider() {
        return new EmptyPropertiesProvider();
    }

    @Bean("mfdJaversAuditableAspect")
    @ConditionalOnProperty(name = {"javers.auditableAspectEnabled"}, havingValue = "true", matchIfMissing = true)
    public JaversAuditableAspect javersAuditableAspect(@Qualifier("mfdJaversFromStarter") Javers javers,
                                                       AuthorProvider authorProvider,
                                                       @Qualifier("mfdEmptyPropertiesProvider") CommitPropertiesProvider commitPropertiesProvider) {
        return new JaversAuditableAspect(javers, authorProvider, commitPropertiesProvider);
    }

    @Bean("mfdSpringDataAuditableAspect")
    @ConditionalOnProperty(name = {"javers.springDataAuditableRepositoryAspectEnabled"}, havingValue = "true", matchIfMissing = true
    )
    public JaversSpringDataAuditableRepositoryAspect javersSpringDataAuditableAspect(@Qualifier("mfdJaversFromStarter") Javers javers,
                                                                                     AuthorProvider authorProvider,
                                                                                     @Qualifier("mfdEmptyPropertiesProvider") CommitPropertiesProvider commitPropertiesProvider) {
        return new JaversSpringDataAuditableRepositoryAspect(javers, authorProvider, commitPropertiesProvider);
    }
}
