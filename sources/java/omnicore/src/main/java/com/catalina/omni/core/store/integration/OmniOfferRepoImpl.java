package com.catalina.omni.core.store.integration;

import com.catalina.omni.core.domain.enums.OmniOfferState;
import com.catalina.omni.core.domain.integration.OmniOffer;
import com.mongodb.BasicDBObject;
import org.bson.BsonArray;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.aggregation.ArrayOperators.Filter.filter;


public class OmniOfferRepoImpl implements OmniOfferRepoCustom {

    @Autowired
    @Qualifier("mfdTemplate")
    private MongoTemplate mongoTemplate;

    @Override
    public List<OmniOffer> findUniqueByBrandAndNotDigestedBy(OmniOfferState state, String andNetworkId, LocalDateTime from, LocalDateTime to) {
        Date fromDate = Date.from(from.atZone(ZoneId.systemDefault()).toInstant());
        Date toDate = Date.from(to.atZone(ZoneId.systemDefault()).toInstant());
        Sort sort = new Sort(Sort.Direction.DESC, "offers.rank");
        BasicDBObject omniOfferPush = getPushOmniOfferObject();

        Aggregation aggregation = newAggregation(
                match(Criteria.where("state").is(state.toString())
                        .and("networkId").is(andNetworkId)
                        .and("endDate").gt(fromDate)
                        .and("startDate").lt(toDate)),
                group("creativeVariables.externalTitle")
                        .max("$rank").as("rank")
                        .push(omniOfferPush).as("offers"),
                project("offers")
                        .and(filter("offers")
                                .as("offer")
                                .by(new Document("$eq", BsonArray.parse("[\"$$offer.rank\", \"$rank\"]")))).as("offers"),
                sort(sort)

        );
        AggregationResults<HashMap> map = mongoTemplate.aggregate(aggregation, "omniOffer", HashMap.class);
        return map.getMappedResults().stream().map(hashMap -> {
            List<OmniOffer> omniOffers = (List<OmniOffer>) hashMap.get("offers");
            int randomIndex = ThreadLocalRandom.current().nextInt(0, omniOffers.size());
            return omniOffers.get(randomIndex);
        }).collect(Collectors.toList());
    }

    @NotNull
    private BasicDBObject getPushOmniOfferObject() {
        BasicDBObject omniOfferPush = new BasicDBObject();
        omniOfferPush.append("_id", "$_id")
                .append("_class", "$_class")
                .append("omniOfferId", "$omniOfferId")
                .append("clientOfferId", "$clientOfferId")
                .append("networkId", "$networkId")
                .append("offerType", "$offerType")
                .append("creativeURL", "$creativeURL")
                .append("creativeVariables", "$creativeVariables")
                .append("state", "$state")
                .append("startDate", "$startDate")
                .append("endDate", "$endDate")
                .append("rank", "$rank")
                .append("source", "$source")
                .append("mfdIngesstionToVault", "$mfdIngestionToVault");
        return omniOfferPush;
    }
}
