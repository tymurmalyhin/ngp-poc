package com.catalina.omni.core.store.integration;

import com.catalina.omni.core.domain.integration.OmniUser;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by ziak on 1/24/18.
 */
public interface OmniUserRepo extends MongoRepository<OmniUser, String> {

    List<OmniUser> findByCountryCodeAndNetworkIdAndDigitalId(String countryCode, String networkId, String digitalId);

    List<OmniUser> findByCountryCodeAndNetworkIdAndFscId(String countryCode, String networkId, String fscId);

    List<OmniUser> findByFscId(String fscId);

    List<OmniUser> findByDigitalId(String digitaId);
}
