package com.catalina.omni.core.hystrix.command.attribution.cidmonitor.save.factory;

import com.catalina.omni.core.configuration.hystrix.HystrixCommandConfiguration;
import com.catalina.omni.core.configuration.hystrix.operations.Insert;
import com.catalina.omni.core.hystrix.command.attribution.cidmonitor.save.InsertOperationCollapser;
import com.catalina.omni.core.hystrix.matcher.MatchableByEqualsIgnoreId;
import com.catalina.omni.core.hystrix.param.save.InsertCommandParam;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.data.mongodb.core.HystrixMongoTemplate;

public class InsertCommandFactory {

    private final HystrixMongoTemplate mongoTemplate;
    private final HystrixCommandConfiguration commandConfiguration;
    private final AuditingHandler auditingHandler;

    public InsertCommandFactory(HystrixMongoTemplate mongoTemplate, HystrixCommandConfiguration commandConfiguration,
                                AuditingHandler auditingHandler) {
        this.mongoTemplate = mongoTemplate;
        this.commandConfiguration = commandConfiguration;
        this.auditingHandler = auditingHandler;
    }

    public <T extends MatchableByEqualsIgnoreId<T>> InsertOperationCollapser<T> createCommand(T objectToSave, String collectionName) {
        final InsertCommandParam<T> param = new InsertCommandParam<>(objectToSave, collectionName);
        final Insert config = commandConfiguration.getUpsert().get(objectToSave.getClass().getSimpleName()).getInsert();

        return new InsertOperationCollapser<>(param, config, mongoTemplate, auditingHandler);
    }
}
