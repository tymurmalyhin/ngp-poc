package com.catalina.omni.core.domain.solrresponse;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

public enum SolrIdType {

    FSC_ID(singletonList("fsc")),
    DIGITAL_ID(asList("aaid", "idfa", "beeswax"));

    private final List<String> idTypeCol;

    SolrIdType(List<String> idTypeCol) {
        this.idTypeCol = idTypeCol;
    }

    public List<String> getIdTypeCol() {
        return idTypeCol;
    }
}
