package com.catalina.omni.core.domain.omniattribution.tag;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LineItemTag {

    private String tealiumBaseTag;

    private String beeswaxTag;

    private String tradeDeskTag;

    private String fourInfoTag;

    private int lineItem;

    private String lineItemId;

    private String lineItemName;
}
