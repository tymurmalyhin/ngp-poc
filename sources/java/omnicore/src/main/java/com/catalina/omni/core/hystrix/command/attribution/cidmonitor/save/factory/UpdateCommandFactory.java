package com.catalina.omni.core.hystrix.command.attribution.cidmonitor.save.factory;

import com.catalina.omni.core.configuration.hystrix.HystrixCommandConfiguration;
import com.catalina.omni.core.configuration.hystrix.operations.Update;
import com.catalina.omni.core.hystrix.command.attribution.cidmonitor.save.UpdateOperationCollapser;
import com.catalina.omni.core.hystrix.matcher.MatchableById;
import com.catalina.omni.core.hystrix.param.save.UpdateCommandParam;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.data.mongodb.core.HystrixMongoTemplate;

public class UpdateCommandFactory {

    private final HystrixMongoTemplate mongoTemplate;
    private final HystrixCommandConfiguration commandConfiguration;
    private final AuditingHandler auditingHandler;

    public UpdateCommandFactory(HystrixMongoTemplate mongoTemplate, HystrixCommandConfiguration commandConfiguration, AuditingHandler auditingHandler) {
        this.mongoTemplate = mongoTemplate;
        this.commandConfiguration = commandConfiguration;
        this.auditingHandler = auditingHandler;
    }

    public <T extends MatchableById<T>> UpdateOperationCollapser<T> createCommand(T objectToSave, String collectionName) {
        final UpdateCommandParam<T> param = new UpdateCommandParam<>(objectToSave, collectionName);
        final Update config = commandConfiguration.getUpsert().get(objectToSave.getClass().getSimpleName()).getUpdate();

        return new UpdateOperationCollapser<>(param, config, mongoTemplate, auditingHandler);
    }
}
