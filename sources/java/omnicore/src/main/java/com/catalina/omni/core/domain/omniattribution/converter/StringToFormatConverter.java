package com.catalina.omni.core.domain.omniattribution.converter;

import com.catalina.omni.core.domain.omniattribution.Format;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalConverter;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;

@ReadingConverter
public class StringToFormatConverter implements ConditionalConverter, Converter<String, Format> {

    @Override
    public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
        return String.class.getName().equals(sourceType.getName())
                && Format.class.getName().equals(targetType.getType().getName());
    }

    @Override
    public Format convert(String source) {
        return Format.getByName(source);
    }
}
