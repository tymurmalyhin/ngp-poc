package com.catalina.omni.core.hystrix.command.attribution.cidmonitor.find;

import static com.catalina.omni.core.configuration.hystrix.HystrixCommandConfiguration.compositeKey;
import static com.catalina.omni.core.hystrix.param.find.factory.QueryCommandParamFactory.extractCollectionName;
import static java.util.Collections.emptyList;
import static java.util.Date.from;

import java.time.ZoneId;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.catalina.omni.core.configuration.hystrix.HystrixCommandConfiguration;
import com.catalina.omni.core.domain.omniattribution.AttributionCIDMonitor;
import com.catalina.omni.core.hystrix.command.CompositeQueryRequestCollapser;
import com.catalina.omni.core.hystrix.param.find.CompositeQueryCommandParam;

public class FindAttributionMonitorByCampaignIdAndCidAndTealiumEventIdAndImpressionDate
    extends CompositeQueryRequestCollapser<AttributionCIDMonitor> {

    private static final String QUERY_NAME = "findByCampaignIdAndCidAndTealiumEventIdAndImpressionDate";

    public FindAttributionMonitorByCampaignIdAndCidAndTealiumEventIdAndImpressionDate(
            CompositeQueryCommandParam<AttributionCIDMonitor> param,
            MongoTemplate template,
            HystrixCommandConfiguration hystrixCommandConfiguration) {
          super(param, template, AttributionCIDMonitor.class,
                FindAttributionMonitorByCampaignIdAndCidAndTealiumEventIdAndImpressionDate::resultMatcher,
                hystrixCommandConfiguration.getFind().get(compositeKey(AttributionCIDMonitor.class, QUERY_NAME)));
        }


    private static boolean resultMatcher(CompositeQueryCommandParam<AttributionCIDMonitor> requestParam,
            AttributionCIDMonitor result) {
        final String campaignId = requestParam.getQuery().get("campaignId", String.class);
        final String cid = requestParam.getQuery().get("cid", String.class);
        final String tealiumEventId = requestParam.getQuery().get("lineItems.tealiumEventId", String.class);
        final Date impressionDate = requestParam.getQuery().get("lineItems.impressionDate", Date.class);
        return new EqualsBuilder()
                .append(campaignId, result.getCampaignId())
                .append(cid, result.getCid())
                .append(true, Optional.ofNullable(result.getLineItems()).orElse(emptyList()).stream()
                        .anyMatch(lineItem -> new EqualsBuilder()
                                .append(tealiumEventId, lineItem.getTealiumEventId())
                                .append(true, Objects.equals(impressionDate,
                                        from(lineItem.getImpressionDate().atZone(ZoneId.systemDefault()).toInstant())))
                                .isEquals()))
                .isEquals();

    }

    public static String queryKey() {
        return AttributionCIDMonitor.class.getName()
                + "_" + extractCollectionName(AttributionCIDMonitor.class)
                + "_campaignId_cid_lineItems.tealiumEventId_lineItems.impressionDate";
    }
}
