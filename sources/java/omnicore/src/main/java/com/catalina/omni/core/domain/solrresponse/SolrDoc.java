package com.catalina.omni.core.domain.solrresponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

/**
 * A concrete representation of a document (doc) within a Solr index.
 */
@Getter
public class SolrDoc {
    @JsonProperty("effective_date")
    private String effectiveDate;

    @JsonProperty("end_date")
    private String endDate;

    @JsonProperty("cchid")
    private String cchid;

    @JsonProperty("id")
    private String id;

    @JsonProperty("id_raw")
    private String idRaw;

    @JsonProperty("id_type")
    private String idType;

    @JsonProperty("_version_")
    private Long version;

    public SolrDoc() {
        // Empty default constructor
    }
}