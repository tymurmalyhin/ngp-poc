package com.catalina.omni.core.domain.omniattribution;

public enum BiddingStrategy {
    CPM, CPC, CPA;
}
