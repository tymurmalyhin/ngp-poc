package com.catalina.omni.core.configuration.hystrix;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.catalina.omni.core.configuration.hystrix.operations.Find;
import com.catalina.omni.core.configuration.hystrix.operations.Upsert;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@ConfigurationProperties(prefix = "hystrix-command")
public class HystrixCommandConfiguration {

  @NestedConfigurationProperty
  private final Map<String, Find> find = new HashMap<>();

  @NestedConfigurationProperty
  private final Map<String, Upsert> upsert = new HashMap<>();

  public static String compositeKey(final Class<? extends Serializable> domainClass,
      final String queryName) {
    return domainClass.getSimpleName() + "." + queryName;
  }

  public Map<String, Find> getFind() {
    return find;
  }

  public Map<String, Upsert> getUpsert() {
    return upsert;
  }
}
