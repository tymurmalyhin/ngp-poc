package com.catalina.omni.core.configuration;

import com.github.mongobee.Mongobee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Properties;


@Configuration
@Import({MongoMfdConfiguration.class,MongoAttributionConfiguration.class})
@ConfigurationProperties
@PropertySource(value = {
    "classpath:application-core.properties",
    "classpath:application-mongo.properties"})
public class MongoBeeConfiguration {
    protected Properties mongoBeeProp = new Properties();
    protected Properties mongodb = new Properties();

    @Autowired
    protected Environment environment;

    @Bean(name = "mongoBeeMfd", destroyMethod = "close")
    @ConditionalOnProperty(value = "mongodb.omnimfd.enabled", havingValue = "true")
    public Mongobee mongobeeMfd(@Qualifier("mfdTemplate") MongoTemplate mfdTemplate) {
        Mongobee runner = new Mongobee((String) mongodb.get("omnimfd.uri"));
        //NOTE template has to be set manually here, otherwise mongobee will try to create it, wich we don't need
        //and it also crashes as it tries to use constructor that is not in this version of MongoTemplate
        runner.setMongoTemplate(mfdTemplate);
        runner.setDbName((String) mongodb.get("omnimfd.name"));
        runner.setChangeLogsScanPackage(mongoBeeProp.getProperty("package-scan"));
        runner.setEnabled(Boolean.valueOf(mongoBeeProp.getProperty("enabled")));
        runner.setSpringEnvironment(environment);
        return runner;
    }

    @Bean(name = "mongoBeeAttribution", destroyMethod = "close")
    @ConditionalOnProperty(value = "mongodb.omniattribution.enabled", havingValue = "true")
    public Mongobee mongobeeAttribution(@Qualifier("attributionTemplate") MongoTemplate attributionTemplate){
        Mongobee runner = new Mongobee((String)mongodb.get("omniattribution.uri"));
        runner.setMongoTemplate(attributionTemplate);
        runner.setDbName((String) mongodb.get("omniattribution.name"));
        runner.setChangeLogsScanPackage(mongoBeeProp.getProperty("package-scan"));
        runner.setEnabled(Boolean.valueOf(mongoBeeProp.getProperty("enabled")));
        runner.setSpringEnvironment(environment);
        return runner;
    }

    public Properties getMongoBeeProp() {
        return mongoBeeProp;
    }

    public void setMongoBeeProp(Properties mongoBeeProp) {
        this.mongoBeeProp = mongoBeeProp;
    }

    public Properties mongodb() {
        return mongodb;
    }

    public void setMongodb(Properties mongodb) {
        this.mongodb = mongodb;
    }
}
