package com.catalina.omni.core.metrics;

import io.micrometer.core.instrument.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.boot.actuate.health.Status;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.StreamSupport;

@Component
public class MetricsCore implements MeterRegistryCustomizer {

    public static double[] HISTOGRAMS = {0.9D, 0.99D, 0.995D};

    private final String[] tags;
    private final MeterRegistry meterRegistry;
    private final HealthEndpoint healthEndpoint;

    public MetricsCore(@Value("${spring.profiles.active}") String profile, @Lazy MeterRegistry meterRegistry, HealthEndpoint healthEndpoint) {
        this.meterRegistry = meterRegistry;
        this.healthEndpoint = healthEndpoint;
        final Optional<InetAddress> localhost = resolveLocalhost();
        String host = localhost.map(InetAddress::getHostName).orElse("unknown_host");
        String address = localhost.map(InetAddress::getHostAddress).orElse("unknown_address");
        this.tags = new String[]{"environment", profile, "host", host, "address", address};
    }

    public Double filterValueFromIterable(final Iterable<Measurement> iterable, Statistic... statisticType) {
        return StreamSupport.stream(iterable.spliterator(), false)
                .filter(measurement -> Arrays.asList(statisticType).contains(measurement.getStatistic()))
                .findFirst()
                .map(Measurement::getValue)
                .orElseThrow(() -> new IllegalStateException("Failed to find " + statisticType));
    }

    public Counter counterWithDescription(String name, String description) {
        return Counter.builder(name)
                .tags(tags)
                .description(description)
                .register(meterRegistry);
    }

    public AtomicInteger gaugeWithDescription(String name, String description) {
        final AtomicInteger gaugeReference = new AtomicInteger(0);
        Gauge.builder(name, gaugeReference, AtomicInteger::doubleValue)
                .tags(tags)
                .description(description)
                .register(meterRegistry);

        return gaugeReference;
    }

    public Timer timerWithDescription(String name, String description) {
        return Timer.builder(name)
                .tags(tags)
                .description(description)
                .publishPercentileHistogram()
                .publishPercentiles(HISTOGRAMS)
                .register(meterRegistry);
    }

    private static Optional<InetAddress> resolveLocalhost() {
        try {
            return Optional.ofNullable(InetAddress.getLocalHost());
        } catch (UnknownHostException e) {
            return Optional.empty();
        }
    }

    public String[] getTags() {
        return tags;
    }

    public MeterRegistry getMeterRegistry() {
        return meterRegistry;
    }

    @Override
    public void customize(MeterRegistry registry) {
        registry.gauge("health", healthEndpoint, MetricsCore::healthToCode);
    }

    private static int healthToCode(HealthEndpoint ep) {
        Status status = ep.health().getStatus();

        return status.equals(Status.UP) ? 1 : 0;
    }
}
