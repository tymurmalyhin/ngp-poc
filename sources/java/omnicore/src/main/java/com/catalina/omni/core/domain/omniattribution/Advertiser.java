package com.catalina.omni.core.domain.omniattribution;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

/**
 * Created by kirk on 4/24/18.
 */
@Document
@Getter
@Setter
public class Advertiser {
    @Id
    private String id;
    private String name;
    private int AdvertiserId;
    @CreatedDate
    private LocalDateTime createdDate;
    private String countryCode;
    private String lastUpdatedBy;
}
