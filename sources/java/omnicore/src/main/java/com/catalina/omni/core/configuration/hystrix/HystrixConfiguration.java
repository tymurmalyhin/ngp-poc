package com.catalina.omni.core.configuration.hystrix;

import com.catalina.omni.core.metrics.MetricsCore;
import com.netflix.hystrix.strategy.HystrixPlugins;
import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import com.netflix.hystrix.strategy.metrics.HystrixMetricsPublisher;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.hystrix.MicrometerMetricsPublisher;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@EnableHystrix
/*
 * Dashboard is by default exposed on http://localhost:8080/hystrix
 *
 * To look at the values you have to put there http://localhost:8080/actuator/hystrix.stream
 * */
@EnableHystrixDashboard
@PropertySource("classpath:application-hystrix.properties")
@EnableConfigurationProperties(HystrixCommandConfiguration.class)
@Import(value = {HystrixAutoloadConfiguration.class, MetricsCore.class})
public class HystrixConfiguration {

  @Bean
  public HystrixRequestContext globalContext(MeterRegistry meterRegistry) {
    final HystrixRequestContext hystrixRequestContext = HystrixRequestContext.initializeContext();
    final HystrixMetricsPublisher originalPublisher = HystrixPlugins.getInstance().getMetricsPublisher();
    HystrixPlugins.reset();
    HystrixPlugins.getInstance().registerMetricsPublisher(
            new MicrometerMetricsPublisher(meterRegistry, originalPublisher));

    return hystrixRequestContext;
  }
}
