package com.catalina.omni.core.domain.omniattribution.template;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class WorkSheets {
    private String name;
    private String object;
    private List<Column> columns;
}
