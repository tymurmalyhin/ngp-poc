package com.catalina.omni.core.exception;

import org.springframework.http.HttpStatus;

public class WebRestException extends RuntimeException {

    /** serialVersionUID */
    private static final long serialVersionUID = -5607577268997270975L;

    private HttpStatus status;

    public WebRestException(HttpStatus status) {
        this.status = status;
    }

    public WebRestException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public WebRestException(String message, Throwable cause, HttpStatus status) {
        super(message, cause);
        this.status = status;
    }

    public WebRestException(Throwable cause, HttpStatus status) {
        super(cause);
        this.status = status;
    }

    public WebRestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, HttpStatus status) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }
}
