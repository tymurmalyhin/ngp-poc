package com.catalina.omni.core.migration;


import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

import static com.mongodb.client.model.Filters.*;

/**
 * Entities in com.catalina.omni.core.domain.integration used to be in package com.catalina.omni.core.domain,
 * therefore existing ones from previous version has to have their _class attribute updated
 */
@ChangeLog(order = "001")
public class MigrationScript001 {
    private static Logger LOG = LoggerFactory.getLogger(MigrationScript001.class);

    @ChangeSet(author = "jsrnicek", order = "01", id = "migrateToIntegrationPackage")
    public void migrateToIntegrationPackage(MongoTemplate template, Environment environment) {

        final MongoDatabase db = template.getDb();
        final String integratioDbName = environment.getProperty("mongodb.omnimfd.name");

        final String databaseName = db.getName().trim();

        //only relevant for mfd database, attribution classes were not moved
        if (databaseName.equalsIgnoreCase(integratioDbName)) {
            LOG.info("Script invoked for db {}", db.getName());

            db.listCollectionNames().forEach((Consumer<? super String>) collectionName -> {
                LOG.info("Migrating collection {}", collectionName);
                final AtomicLong updatedCounter = new AtomicLong();
                final MongoCollection<Document> dbCollection = db.getCollection(collectionName);

                /*
                * - exists("_class") - includes all documents that represent domain classes
                * - regex("_class","com.catalina.omni.core.domain"),
                    not(regex("_class","com.catalina.omni.core.domain.integration"))) - Includes all ***core.domain,
                    and excludes all ***core.domain.integration, so we are getting only the ones that should be updated
                * */
                dbCollection.find(and(
                        exists("_class"),
                        regex("_class", "com.catalina.omni.core.domain"),
                        not(regex("_class", "com.catalina.omni.core.domain.integration"))))
                        .forEach((Consumer<? super Document>) document -> {
                            final String updatedClass = document.getString("_class")
                                    .replace("com.catalina.omni.core.domain",
                                            "com.catalina.omni.core.domain.integration");

                            final Document classField = new Document("_class", updatedClass);
                            dbCollection.updateOne(eq("_id", document.get("_id")), new Document("$set", classField));
                            updatedCounter.incrementAndGet();
                        });
                LOG.info("{} elements updated in collection {}", updatedCounter.get(), collectionName);
            });
        } else {
            LOG.info("Skipping execution of migration script 001, not relevant for db {}", databaseName);
        }
    }
}
