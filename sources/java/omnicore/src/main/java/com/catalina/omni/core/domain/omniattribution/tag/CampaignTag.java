package com.catalina.omni.core.domain.omniattribution.tag;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CampaignTag {

    private String campaignId;

    private String campaignName;

    private List<LineItemTag> tags;
}
