package com.catalina.omni.core.domain.omniattribution;

import com.catalina.omni.core.hystrix.capability.UpsertAggregationCapable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

/**
 * Created by iolha on 8/9/18.
 */
@Document(collection = "AttributionCIDMonitorV2")
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class AttributionCIDMonitor implements UpsertAggregationCapable<AttributionCIDMonitor> {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -2321257953001727074L;

    @Id
    private String id;
    private String cid;
    private String campaignId;
    private LocalDateTime endDate;
    private List<LineItemImpressionEvent> lineItems;

    @CreatedDate
    private LocalDateTime dateCreated;

    @LastModifiedDate
    private LocalDateTime lastUpdateDate;

    @Override
    public boolean matchIgnoreId(AttributionCIDMonitor other) {
        return Objects.equals(cid, other.cid) &&
                Objects.equals(campaignId, other.campaignId) &&
                Objects.equals(endDate, other.endDate) &&
                Objects.equals(lineItems, other.lineItems) &&
                Objects.equals(dateCreated, other.dateCreated) &&
                Objects.equals(lastUpdateDate, other.lastUpdateDate);
    }

    @Override
    public boolean matchById(AttributionCIDMonitor other) {
        return Objects.equals(id, other.id);
    }
}