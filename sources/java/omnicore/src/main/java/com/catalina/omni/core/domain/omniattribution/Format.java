package com.catalina.omni.core.domain.omniattribution;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Format  {
    BANNER("banner-ba"), RICHMEDIABANNER("RichMediaBanner-rm"), PREROLLVIDEO("PreRollVideo-vi"),
    NATIVE("native-na"), INTERSTITIAL("interstitial-in"), SHOPPER("shopper-sh");
    private String name;
    Format(String name) {
        this.name = name;
    }

    @JsonCreator
    public static Format getByName(String value) {
        for (Format format: Format.values()) {
            if (format.name.equals(value)) {
                return format;
            };
        }
        return null;
    }

    @JsonValue
    public String getName() {
        return name;
    }
}
