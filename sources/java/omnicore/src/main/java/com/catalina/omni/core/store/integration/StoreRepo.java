package com.catalina.omni.core.store.integration;

import com.catalina.omni.core.domain.integration.Store;
import org.springframework.data.geo.Box;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

public interface StoreRepo extends MongoRepository<Store, String> {
    List<Store> findByLocationWithin(Box box);
    Long deleteByUpdatedBefore(Date date);
}
