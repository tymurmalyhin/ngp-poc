package com.catalina.omni.core.migration;


import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

import static com.mongodb.client.model.Filters.exists;

/**
 * Entity OmniUserOfferClip is completly refactored for Send Ad2L2C Untargeted clips to Vault
 */
@ChangeLog(order = "002")
public class MigrationScript002 {
    private static Logger LOG = LoggerFactory.getLogger(MigrationScript002.class);
    private static final Bson BSON_FSC_IDS_FILTER = exists("fscIds");

    @ChangeSet(author = "pziak", order = "02", id = "refactorOfferClip")
    public void refactorOfferClip(MongoTemplate template, Environment environment) {

        final MongoDatabase db = template.getDb();
        final String integratioDbName = environment.getProperty("mongodb.omnimfd.name");
        final String databaseName = db.getName().trim();

        //only relevant for mfd database
        if (databaseName.equalsIgnoreCase(integratioDbName)) {
            LOG.info("Script invoked for db {}", db.getName());
            final AtomicLong updatedCounter = new AtomicLong();
            String collectionName = "omniUserOfferClip";
            final MongoCollection<Document> clipCollection = db.getCollection(collectionName);

            clipCollection.find(BSON_FSC_IDS_FILTER)
                    .forEach((Consumer<? super Document>) document -> {
                        ((List<String>) document.get("fscIds")).forEach(fscId -> {
                            LinkedHashMap<String, Object> clipOfferMap = new LinkedHashMap<>();
                            mapToClipOfferMap(document, clipOfferMap, "omniOfferId");
                            mapToClipOfferMap(document, clipOfferMap, "clientOfferId");
                            clipOfferMap.put("cid", "USA-0099-" + fscId);
                            clipOfferMap.put("dateOfClip", document.get("startDate"));
                            mapToClipOfferMap(document, clipOfferMap, "_class");

                            final Document classField = new Document(clipOfferMap);
                            clipCollection.insertOne(new Document(classField));
                            updatedCounter.incrementAndGet();
                        });
                    });
            clipCollection.deleteMany(BSON_FSC_IDS_FILTER);
            LOG.info("{} elements updated in collection {}", updatedCounter.get(), collectionName);
        } else {
            LOG.info("Skipping execution of migration script 002, not relevant for db {}", databaseName);
        }
    }

    private void mapToClipOfferMap(Document document, LinkedHashMap<String, Object> clipOfferMap, String property) {
        if (document.get(property) != null) {
            clipOfferMap.put(property, document.get(property));
        }
    }
}
