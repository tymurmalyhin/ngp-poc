package com.catalina.omni.core.domain.enums;

public class EventActionType extends EnumUserType<EventAction> {

    @Override
    public BaseEnum getById(int id) {
        return EventAction.getById(id);
    }
}