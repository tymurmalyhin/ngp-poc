package com.catalina.omni.core.store.integration;

import com.catalina.omni.core.domain.integration.OmniEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ziak on 1/24/18.
 */
@Repository
public interface EventRepo extends MongoRepository<OmniEvent, String> {

    List<OmniEvent> findByCampaignId(String campaingId);

}