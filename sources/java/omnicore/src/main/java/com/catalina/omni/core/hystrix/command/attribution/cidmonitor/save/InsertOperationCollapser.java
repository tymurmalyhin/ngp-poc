package com.catalina.omni.core.hystrix.command.attribution.cidmonitor.save;

import com.catalina.omni.core.configuration.hystrix.operations.Insert;
import com.catalina.omni.core.hystrix.matcher.MatchableByEqualsIgnoreId;
import com.catalina.omni.core.hystrix.param.save.DataCommandParam;
import com.catalina.omni.core.hystrix.param.save.InsertCommandParam;
import com.mongodb.client.model.BulkWriteOptions;
import com.mongodb.client.model.InsertOneModel;
import com.netflix.hystrix.*;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.auditing.AuditingHandler;
import org.springframework.data.mongodb.core.HystrixMongoTemplate;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.String.format;

public class InsertOperationCollapser<T extends MatchableByEqualsIgnoreId<T>>
        extends HystrixCollapser<Iterable<T>, T, InsertCommandParam<T>> {

    private static final Logger LOG = LoggerFactory.getLogger(InsertOperationCollapser.class);

    private final InsertCommandParam<T> param;
    private final HystrixCommand.Setter commandSetter;
    private final HystrixMongoTemplate mongoTemplate;
    private final AuditingHandler auditingHandler;
    private final Class<T> domainClass;

    public InsertOperationCollapser(InsertCommandParam<T> param, Insert params,
                                    HystrixMongoTemplate mongoTemplate, AuditingHandler auditingHandler) {
        super(HystrixCollapser.Setter.withCollapserKey(
                HystrixCollapserKey.Factory.asKey(param.toKey()))
                .andScope(Scope.GLOBAL)
                .andCollapserPropertiesDefaults(HystrixCollapserProperties.Setter()
                        .withTimerDelayInMilliseconds(params.getTimeWindowSize())
                        .withMaxRequestsInBatch(params.getMaxRequestsInBatch())));
        this.param = param;
        this.mongoTemplate = mongoTemplate;
        this.auditingHandler = auditingHandler;
        final String key = param.toKey();
        this.commandSetter = HystrixCommand.Setter
                .withGroupKey(HystrixCommandGroupKey.Factory.asKey(key))
                .andCommandKey(HystrixCommandKey.Factory.asKey(key))
                .andCommandPropertiesDefaults(
                        HystrixCommandProperties.Setter()
                                .withCircuitBreakerEnabled(true)
                                .withCircuitBreakerErrorThresholdPercentage(
                                        params.getCircuitBreakerErrorThresholdPercentage())
                                .withCircuitBreakerSleepWindowInMilliseconds(
                                        params.getSleepWindowInMilliseconds())
                                .withExecutionIsolationSemaphoreMaxConcurrentRequests(10000)
                                .withExecutionIsolationStrategy(HystrixCommandProperties.ExecutionIsolationStrategy.SEMAPHORE)
                                .withExecutionTimeoutEnabled(false)
                                .withRequestLogEnabled(true));
        this.domainClass = (Class<T>) this.param.getData().getClass();
    }

    @Override
    public InsertCommandParam<T> getRequestArgument() {
        return param;
    }

    @Override
    protected HystrixCommand<Iterable<T>> createCommand(Collection<CollapsedRequest<T, InsertCommandParam<T>>> collection) {
        return new HystrixCommand<Iterable<T>>(commandSetter) {
            @Override
            protected Iterable<T> run() throws Exception {

                final List<T> allData = collection.stream()
                        .map(CollapsedRequest::getArgument)
                        .map(DataCommandParam::getData)
                        //bulk operations does not emmit events thus the auditing wont work, so we need to call it directly
                        .peek(auditingHandler::markCreated)
                        .collect(Collectors.toList());

                if (allData.size() == 1) {
                    //if singular insert, just call the regular insert - it's shard aware
                    final T data = allData.get(0);
                    mongoTemplate.doInsertSuper(param.getCollectionName(), data);

                    return Collections.singletonList(data);
                }

                final List<InsertOneModel<Document>> allInserts = allData.stream()
                        .map(t -> getMappedObject(t))
                        .map(InsertOneModel::new)
                        .collect(Collectors.toList());

                mongoTemplate.getCollection(param.getCollectionName())
                        .bulkWrite(allInserts, new BulkWriteOptions().ordered(false).bypassDocumentValidation(true));

                return allInserts.stream()
                        .map(InsertOneModel::getDocument)
                        .map(document -> getDomainObject(document))
                        .collect(Collectors.toSet());
            }
        };
    }

    @Override
    protected void mapResponseToRequests(Iterable<T> responses, Collection<CollapsedRequest<T, InsertCommandParam<T>>> requests) {
        final List<CollapsedRequest<T, InsertCommandParam<T>>> withoutAnswer = new LinkedList<>();

        requests.forEach(request -> {
            final Iterator<T> responseIterator = responses.iterator();
            final T requestData = request.getArgument().getData();

            while (responseIterator.hasNext()) {
                final T response = responseIterator.next();

                if (response.matchIgnoreId(requestData)) {
                    request.setResponse(response);
                    request.setComplete();
                    return;
                }
            }
            //not sure if this situation can even occur but just in case so that request doesn't hang in the air
            //till timeout hits
            withoutAnswer.add(request);
        });

        withoutAnswer.forEach(nonAnswered -> {
            nonAnswered.setException(new IllegalStateException(format("Inserted object for data %s not found in database",
                    nonAnswered.getArgument().getData())));
            nonAnswered.setComplete();
        });
    }

    private Document getMappedObject(Object source) {

        if (source instanceof Document) {
            return (Document) source;
        }

        Document sink = new Document();
        mongoTemplate.getConverter().write(source, sink);
        return sink;
    }

    private T getDomainObject(Document source){
        return mongoTemplate.getConverter().read(domainClass,source);
    }
}
