package com.catalina.omni.core.domain.omniattribution;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

// @JsonProperty mapping for this class was mostly comes from Catalina / Vault / catalyst-dmp /
// com.catalina.platform.dmp.services.EventService @GET @Path("/collect") public Response track(...)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class TealiumPixelEventHubUdo {

    /** ads - A comma separated sequence of unique media ids */
    @JsonProperty("ads")
    private String ads;
    /** app id */
    @JsonProperty("aid")
    private String aid;
    /** app version */
    @JsonProperty("av")
    private String av;
    /** cat attr id */
    @JsonProperty("cat_attr_id")
    private String catAttrId;
    /** campaign id */
    @JsonProperty("ci")
    private String ci;
    /** media type */
    @JsonProperty("cm")
    private String cm;
    /** campaign name */
    @JsonProperty("cn")
    private String cn;
    /** country code */
    @JsonProperty("country_code")
    private String countryCode;
    /** channel type */
    @JsonProperty("cs")
    private String cs;
    /** unique device identifier */
    @JsonProperty("did")
    private String did;
    /** event action */
    @JsonProperty("ea")
    private String ea;
    /** event label */
    @JsonProperty("el")
    private String el;
    /** event value */
    @JsonProperty("ev")
    private String ev;
    /** latitude */
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("ln")
    private String ln;
    /** longitude */
    @JsonProperty("lon")
    private String lon;
    /** publisher id */
    @JsonProperty("pub")
    private String pub;
    /** session id */
    @JsonProperty("sid")
    private String sid;
    /** site id */
    @JsonProperty("ste")
    private String ste;
    /** system id */
    @JsonProperty("sys")
    private String sys;
    /** EventType event type */
    @JsonProperty("t")
    private String t;
    @JsonProperty("tealium_event")
    private String tealiumEvent;
    /** user account token */
    @JsonProperty("ua")
    private String ua;
    /** user ip */
    @JsonProperty("uip")
    private String uip;
    @JsonProperty("uip_ust")
    private String uipCust;
    /** version */
    @JsonProperty("v")
    private String v;
}