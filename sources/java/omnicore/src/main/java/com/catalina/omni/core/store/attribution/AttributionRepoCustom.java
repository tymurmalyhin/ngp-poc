package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.domain.omniattribution.Attribution;

import java.util.List;
import java.util.Set;

public interface AttributionRepoCustom {
    List<Attribution> getByLineItemsIdAndNotSuppressed(Set<String> lineItemIds, int index, int number);
    List<Attribution> getByLineItemsIdAndNotRetargeted(Set<String> lineItemIds, int index, int number);
}
