package com.catalina.omni.core.store.integration;

import com.catalina.omni.core.domain.integration.ClipResult;
import com.catalina.omni.core.domain.integration.OmniUserOfferClip;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by ziak on 7/27/18.
 */
@Repository
public interface OmniUserOfferClipRepo extends MongoRepository<OmniUserOfferClip, String> {

    Optional<OmniUserOfferClip> findByOmniOfferIdAndCid(String offerId, String cid);

    Optional<OmniUserOfferClip> findByClientOfferIdAndCid(String clientOfferId, String cid);

    Page<OmniUserOfferClip> findByClipIngestionToVaultIsNullAndCidIsNotNullAndOmniOfferIdIsNotNull(Pageable page);

    Page<OmniUserOfferClip> findByState(Pageable page, ClipResult.ClipResultStage state);
}
