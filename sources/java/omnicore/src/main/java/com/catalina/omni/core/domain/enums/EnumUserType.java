package com.catalina.omni.core.domain.enums;

public abstract class EnumUserType<E extends Enum<E> & BaseEnum> {

    public abstract BaseEnum getById(int id);

}
