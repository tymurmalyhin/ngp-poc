package com.catalina.omni.core.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "slack-configuration")
@Data
public class SlackProperties {

    private String slackWebHookURL;
    private String slackChannel;
    private String slackToken;
    private boolean slackNotificationsTurnOn;
}
