package com.catalina.omni.core.controller;


import com.catalina.omni.core.exception.NotFoundException;
import com.netflix.config.ConcurrentCompositeConfiguration;
import com.netflix.config.ConfigurationManager;
import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hystrix-config")
@Log
public class HystrixConfigurationController {

    @GetMapping("/{commandKey}/{property}/{propertyValue}")
    public String setHystrixProperty(@PathVariable String commandKey,
                                     @PathVariable String property,
                                     @PathVariable String propertyValue) throws NotFoundException {
        ConcurrentCompositeConfiguration config = (ConcurrentCompositeConfiguration) ConfigurationManager.getConfigInstance();
        String key;

        switch (property) {
            case "timeoutInMilliseconds":
                key = "execution.isolation.thread.timeoutInMilliseconds";
                break;
            case "executionTimeoutEnabled":
                key = "execution.timeout.enabled";
                break;
            case "requestVolumeThreshold":
                key = "circuitBreaker.requestVolumeThreshold";
                break;
            case "sleepWindowInMilliseconds":
                key = "circuitBreaker.sleepWindowInMilliseconds";
                break;
            case "errorThresholdPercentage":
                key = "circuitBreaker.errorThresholdPercentage";
                break;
            case "fallbackEnabled":
                key = "fallback.enabled";
                break;
            default:
                throw new NotFoundException("Wrong property name for hystrix command." +
                        "Possible properties are: " +
                        "timeoutInMilliseconds, executionTimeoutEnabled, requestVolumeThreshold, " +
                        "sleepWindowInMilliseconds, errorThresholdPercentage, fallbackEnabled");
        }
        String prefixPlusKey = "hystrix.command." + commandKey + "." + key;
        Object oldPropertyValue = config.getProperty(prefixPlusKey);
        config.setOverrideProperty(prefixPlusKey, propertyValue);

        String s = "Property " + property + " has been set for hystrix command " + commandKey +
                " from " + (oldPropertyValue == null ? "not-setted" : oldPropertyValue) + " to " + propertyValue + ".";
        log.info(s);
        return s;
    }
}
