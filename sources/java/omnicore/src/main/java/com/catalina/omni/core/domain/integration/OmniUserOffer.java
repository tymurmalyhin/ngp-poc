package com.catalina.omni.core.domain.integration;

import com.catalina.omni.core.domain.enums.OmniUserOfferState;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

/**
 * Created by ziak on 1/24/18.
 */
@Document (collection = "omniUserOfferV2")
@Getter
@Setter
public class OmniUserOffer {

    @Id
    private String id;
    private String countryCode;
    private String networkId;
    private String digitalId;
    private String fscId;
    private String walletURL;
    private String walletId;
    private OmniUserOfferState state = OmniUserOfferState.IN_PROGRESS;
    private OmniOffer omniOffer;
    private String advertisementResponseJSON;

    @CreatedDate
    private LocalDateTime created;
}
