package com.catalina.omni.core.hystrix.matcher;

import java.io.Serializable;

/**
 * Interface to match results for insert operation
 */
public interface MatchableByEqualsIgnoreId<T extends Serializable> extends Serializable, IdAware, DateCreatedAware {

    boolean matchIgnoreId(T other);
}
