package com.catalina.omni.core.rest;

import lombok.Builder;
import lombok.experimental.Accessors;
import org.javatuples.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.Optional;
import java.util.function.Function;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Contains common logic to deal with error codes on rest calls
 */
@Builder
@Accessors(fluent = true, chain = true)
public class RestRequestMapper<RESULT, REST_RESULT, CODE> {

    /**
     * Function to invoke the rest call itself.
     * <p>
     * Should only contain the invoke part itself
     */
    private RestInvoker<REST_RESULT> invoke;

    /**
     * Function to extract body of the response. Use either this or custom 2xx
     */
    private Function<REST_RESULT, RESULT> extractResponse;

    /**
     * Used to map all 5xx errors.
     * <p>
     * Default behaviour will throw {@link IllegalStateException}
     */
    private Function<HttpStatusCodeException, CODE> on5xx;

    /**
     * Used to map all 4xx errors.
     * <p>
     * Default behaviour will throw {@link IllegalStateException}
     */
    private Function<HttpStatusCodeException, CODE> on4xx;

    /**
     * Used to map all 4xx errors.
     * <p>
     * Default behaviour will throw {@link IllegalStateException}
     */
    private Function<HttpStatusCodeException, CODE> on3xx;

    /**
     * Used to map all 2xx responses. Can be used for further validation of returned data(for example vault X-Errors)
     * <p>
     * Default behaviour will return result without any response code
     */
    private Function<REST_RESULT, Pair<RESULT, CODE>> on2xx;

    /**
     * Any other exception that occurs during invocation and isn't {@link HttpStatusCodeException}
     */
    private Function<Exception, CODE> onGeneralFailure;

    /**
     * Get result
     */
    public Pair<RESULT, CODE> get() {
        checkArgument(extractResponse != null || on2xx != null, "Response extractor or on2xx must be defined");

        try {
            return Optional.ofNullable(on2xx)
                    .orElse(default2xx())
                    .apply(invoke.invoke());
        } catch (Exception e) {
            if (e instanceof HttpStatusCodeException) {
                final HttpStatusCodeException statusEx = (HttpStatusCodeException) e;
                final HttpStatus statusCode = statusEx.getStatusCode();

                if (statusCode.is3xxRedirection()) {
                    return Pair.with(null, Optional.ofNullable(on3xx)
                            .orElse(default3xx())
                            .apply(statusEx));
                } else if (statusCode.is4xxClientError()) {
                    return Pair.with(null, Optional.ofNullable(on4xx)
                            .orElse(default4xx())
                            .apply(statusEx));
                } else if (statusCode.is5xxServerError()) {
                    return Pair.with(null, Optional.ofNullable(on5xx)
                            .orElse(default5xx())
                            .apply(statusEx));
                }
            }

            return Pair.with(null, Optional.ofNullable(onGeneralFailure)
                    .orElse(defaultGeneralFailure())
                    .apply(e));

        }
    }

    @FunctionalInterface
    public interface RestInvoker<RESULT> {
        RESULT invoke() throws Exception;
    }

    private Function<Exception, CODE> defaultGeneralFailure() {
        return e -> {
            throw new IllegalStateException("Unexpected failure", e);
        };
    }

    private Function<REST_RESULT, Pair<RESULT, CODE>> default2xx() {
        return result -> Pair.with(extractResponse.apply(result), null);
    }

    private Function<HttpStatusCodeException, CODE> default3xx() {
        return e -> {
            throw new IllegalStateException("Unexpected 3xx error", e);
        };
    }

    private Function<HttpStatusCodeException, CODE> default4xx() {
        return e -> {
            throw new IllegalStateException("Unexpected 4xx error", e);
        };
    }

    private Function<HttpStatusCodeException, CODE> default5xx() {
        return e -> {
            throw new IllegalStateException("Unexpected 5xx error", e);
        };
    }
}
