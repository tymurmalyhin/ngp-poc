package com.catalina.omni.core.domain.omniattribution.template;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Document
@Getter
@Setter
public class Template {
    @Id
    private String id;
    private String name;
    private boolean lookupIdsByName;
    private int version;
    private String createdBy;
    @CreatedDate
    private LocalDateTime createdDate;
    private String lastUpdatedBy;
    private List<WorkSheets> worksheets;
}
