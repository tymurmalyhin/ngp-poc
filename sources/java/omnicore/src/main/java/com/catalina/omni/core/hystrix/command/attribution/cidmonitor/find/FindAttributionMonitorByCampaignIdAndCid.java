package com.catalina.omni.core.hystrix.command.attribution.cidmonitor.find;

import static com.catalina.omni.core.configuration.hystrix.HystrixCommandConfiguration.compositeKey;
import static com.catalina.omni.core.hystrix.param.find.factory.QueryCommandParamFactory.extractCollectionName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.catalina.omni.core.configuration.hystrix.HystrixCommandConfiguration;
import com.catalina.omni.core.domain.omniattribution.AttributionCIDMonitor;
import com.catalina.omni.core.hystrix.command.CompositeQueryRequestCollapser;
import com.catalina.omni.core.hystrix.param.find.CompositeQueryCommandParam;

public class FindAttributionMonitorByCampaignIdAndCid
    extends CompositeQueryRequestCollapser<AttributionCIDMonitor> {

    private static final String QUERY_NAME = "findByCampaignIdAndCid";

    public FindAttributionMonitorByCampaignIdAndCid(
            CompositeQueryCommandParam<AttributionCIDMonitor> param,
            MongoTemplate template,
            HystrixCommandConfiguration hystrixCommandConfiguration) {
          super(param, template, AttributionCIDMonitor.class,
                FindAttributionMonitorByCampaignIdAndCid::resultMatcher,
                hystrixCommandConfiguration.getFind().get(compositeKey(AttributionCIDMonitor.class, QUERY_NAME)));
        }


    private static boolean resultMatcher(CompositeQueryCommandParam<AttributionCIDMonitor> requestParam,
            AttributionCIDMonitor result) {
        final String campaignId = requestParam.getQuery().get("campaignId", String.class);
        final String cid = requestParam.getQuery().get("cid", String.class);
        return new EqualsBuilder()
                .append(campaignId, result.getCampaignId())
                .append(cid, result.getCid())
                .isEquals();
    }

    public static String queryKey() {
        return AttributionCIDMonitor.class.getName()
                + "_" + extractCollectionName(AttributionCIDMonitor.class)
                + "_campaignId_cid";
    }
}
