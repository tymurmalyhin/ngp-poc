package com.catalina.omni.core.hystrix.command.factory;

import com.catalina.omni.core.configuration.hystrix.HystrixCommandConfiguration;
import com.catalina.omni.core.domain.omniattribution.AttributionCIDMonitor;
import com.catalina.omni.core.hystrix.command.CompositeQueryRequestCollapser;
import com.catalina.omni.core.hystrix.command.attribution.cidmonitor.find.FindAttributionMonitorByCampaignIdAndCid;
import com.catalina.omni.core.hystrix.command.attribution.cidmonitor.find.FindAttributionMonitorByCampaignIdAndCidAndEventIdAndImpressionDate;
import com.catalina.omni.core.hystrix.command.attribution.cidmonitor.find.FindAttributionMonitorByCampaignIdAndCidAndTealiumEventIdAndImpressionDate;
import com.catalina.omni.core.hystrix.param.find.CompositeQueryCommandParam;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

/**
 * Factory for providing hystrix commands. In order to support new query, a new entry has to be
 * added to requestCreatorFunctionRegistry on per table/query basis.
 */
public class QueryCommandFactory {

  private final Map<String, Function<CompositeQueryCommandParam<?>,
      CompositeQueryRequestCollapser<?>>> requestCreatorFunctionRegistry;

  @SuppressWarnings("unchecked")
  public QueryCommandFactory(MongoTemplate template,
      HystrixCommandConfiguration commandConfiguration) {
    requestCreatorFunctionRegistry = new HashMap<>();
    //--
    requestCreatorFunctionRegistry
        .put(FindAttributionMonitorByCampaignIdAndCidAndTealiumEventIdAndImpressionDate.queryKey(),
            param -> new FindAttributionMonitorByCampaignIdAndCidAndTealiumEventIdAndImpressionDate(
                (CompositeQueryCommandParam<AttributionCIDMonitor>) param, template,
                commandConfiguration));
    //--
    requestCreatorFunctionRegistry
        .put(FindAttributionMonitorByCampaignIdAndCidAndEventIdAndImpressionDate.queryKey(),
            param -> new FindAttributionMonitorByCampaignIdAndCidAndEventIdAndImpressionDate(
                (CompositeQueryCommandParam<AttributionCIDMonitor>) param, template,
                commandConfiguration));
    //--
    requestCreatorFunctionRegistry
        .put(FindAttributionMonitorByCampaignIdAndCid.queryKey(),
            param -> new FindAttributionMonitorByCampaignIdAndCid(
                    (CompositeQueryCommandParam<AttributionCIDMonitor>) param, template,
                    commandConfiguration));
  }

  @SuppressWarnings("unchecked")
  public <T> CompositeQueryRequestCollapser<T> createCommand(CompositeQueryCommandParam<T> param) {
    return (CompositeQueryRequestCollapser<T>) Optional
        .ofNullable(requestCreatorFunctionRegistry.get(param.toKey()))
        .map(function -> function.apply(param))
        .orElse(null);
  }
}
