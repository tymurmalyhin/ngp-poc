package com.catalina.omni.core.domain.integration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document
@Getter
@Setter
public class Store {
    @Id
    private String id;
    private GeoJsonPoint location;
    private String name;
    private String address;
    private String phoneNumber;
    private List<OpeningHour> openingHours;
    private Date created;
    private Date updated;
    /** THe URL to Google page */
    private String pageUrl;
    /** The number of minutes this place’s current timezone is offset from UTC. */
    private int utcOffset;


}
