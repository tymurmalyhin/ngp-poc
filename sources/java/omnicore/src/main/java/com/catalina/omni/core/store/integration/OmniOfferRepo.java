package com.catalina.omni.core.store.integration;

import com.catalina.omni.core.domain.enums.OmniOfferState;
import com.catalina.omni.core.domain.integration.OmniOffer;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created by ziak on 4/4/18.
 */

@Repository
public interface OmniOfferRepo extends MongoRepository<OmniOffer, String>, OmniOfferRepoCustom {

    List<OmniOffer> findByOmniOfferIdInAndStateAndEndDateAfterAndStartDateBefore(List<String> omniOfferIds, OmniOfferState state, LocalDateTime inAnHour, LocalDateTime now);

    List<OmniOffer> findByStateAndNetworkId(OmniOfferState state, String networkId);

    List<OmniOffer> findByNetworkIdAndClientOfferId(String networkId, String clientOfferId);

    List<OmniOffer> findByStateAndNetworkIdAndEndDateAfterAndStartDateBeforeOrderByRankDesc(OmniOfferState state, String networkId, LocalDateTime inAnHour, LocalDateTime now);

    List<OmniOffer> findByNetworkIdInAndStateAndMfdIngestionToVaultIsNullAndUpcsIsNotNull(Set<String> networkIds, OmniOfferState state, Pageable pageable);

    List<OmniOffer> findByIdIn(Collection<String> ids);

    Stream<OmniOffer> findByNetworkIdAndStateAndMfdIngestionToVaultIsNull(String networkId, OmniOfferState state);

  List<OmniOffer> findByOmniOfferIdIn(Set<String> ids);

}
