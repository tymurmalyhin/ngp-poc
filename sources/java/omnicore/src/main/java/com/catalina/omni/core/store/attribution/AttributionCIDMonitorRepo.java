package com.catalina.omni.core.store.attribution;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.catalina.omni.core.domain.omniattribution.AttributionCIDMonitor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by iolha on 8/9/18.
 */
@Repository
public interface AttributionCIDMonitorRepo extends MongoRepository<AttributionCIDMonitor, String>, AttributionCIDMonitorRepoCustom {

    List<AttributionCIDMonitor> findByCid(String cid);

    List<AttributionCIDMonitor> findByCidAndEndDateAfter(String cid, LocalDateTime today);

    Optional<AttributionCIDMonitor> findFirstByCidAndCampaignId(String cid, String campaignId);

    List<AttributionCIDMonitor> findAllByCampaignIdAndCid(String campaignId, String cid);

    // XXX this method will be later removed as part of the omni-camel DumpCidMonitorRouter removal
    List<AttributionCIDMonitor> deleteAllByEndDateBefore(LocalDateTime now);

    Optional<AttributionCIDMonitor> findFirstByEndDateBefore(LocalDateTime now);

    // Finds all records fulfilling the following criteria for campaignId, cid, lineItems.tealiumEventId and lineItems.impressionDate
    @Query(value = "{ \"campaignId\": ?0, \"cid\": ?1, \"lineItems.tealiumEventId\": ?2, \"lineItems.impressionDate\": ?3 }")
    List<AttributionCIDMonitor> findByCampaignIdAndCidAndTealiumEventIdAndImpressionDate(String campaignId, String cid,
                                                                                                     String tealiumEventId, LocalDateTime impressionDate);

}