package com.catalina.omni.core.hystrix.param.save;

import com.catalina.omni.core.hystrix.matcher.MatchableById;

import java.io.Serializable;
import java.util.Objects;

public class UpdateCommandParam<T extends MatchableById<T> & Serializable> extends DataCommandParam<T> {

    public UpdateCommandParam(T data, String collectionName) {
        super(data, collectionName);
    }

    @Override
    public boolean equals(Object obj) {
        UpdateCommandParam<T> that = (UpdateCommandParam<T>) obj;
        if (that == null) {
            return false;
        }
        return Objects.equals(this.data.getClass(), that.data.getClass())
                && collectionName.equals(that.collectionName)
                && data.matchById(that.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data.getClass(), data);
    }

    public String toKey() {
        return data.getClass().getSimpleName() + "_" + collectionName + "_update";
    }
}
