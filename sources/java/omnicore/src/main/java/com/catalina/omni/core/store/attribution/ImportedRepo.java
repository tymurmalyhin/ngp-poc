package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.domain.omniattribution.ImportedItem;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ImportedRepo extends MongoRepository<ImportedItem, String> {

}
