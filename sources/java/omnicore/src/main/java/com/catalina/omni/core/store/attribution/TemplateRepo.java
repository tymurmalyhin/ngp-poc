package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.domain.omniattribution.template.Template;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TemplateRepo extends MongoRepository<Template, String> {
}
