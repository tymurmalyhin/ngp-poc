package com.catalina.omni.core.controller;

import com.catalina.omni.core.configuration.SlackProperties;
import com.catalina.omni.core.slack.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/slack")
@Profile("!prod")
public class SlackTestController {

    @Autowired
    private SlackProperties slackProperties;

    @Autowired
    private NotificationService notificationService;

    @Value("${slack-configuration.slackNotificationsTurnOn:false}")
    private boolean originalSlackConfiguration;

    @GetMapping
    public void sendSLackMessage() {
        try {
            slackProperties.setSlackNotificationsTurnOn(true);

            notificationService.sendMessage("This is the test message on dev and local");

        } finally {
            slackProperties.setSlackNotificationsTurnOn(originalSlackConfiguration);
        }
    }

}
