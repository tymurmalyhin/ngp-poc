package com.catalina.omni.core.store.attribution;

import java.time.LocalDateTime;

import com.catalina.omni.core.domain.omniattribution.AttributionCIDMonitor;
import com.catalina.omni.core.domain.omniattribution.LineItemImpressionEvent;

public interface AttributionCIDMonitorRepoCustom {

    AttributionCIDMonitor createOrUpdateCidMonitor(String cid, String campaignId, LocalDateTime endDate, LineItemImpressionEvent event);

}
