package com.catalina.omni.core.domain.errorhandling;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@EqualsAndHashCode
public class ErrorDetails implements Serializable {

    /** serialVersionUID */
    private static final long serialVersionUID = -5162096912595897231L;

    private Date timestamp;
    private Integer status;
    private String error;
    private String message;

    public ErrorDetails() {
        // EMPTY constructor
    }

    public ErrorDetails(HttpStatus status, String message) {
        this.timestamp = new Date();
        if (status != null) {
            this.status = status.value();
            this.error = status.getReasonPhrase();
        }
        this.message = message;
    }

}
