package com.catalina.omni.core.configuration;

import com.catalina.omni.core.configuration.hystrix.HystrixConfiguration;
import com.catalina.omni.core.configuration.print.MongoMappingPrinter;
import com.mongodb.MongoClientOptions;
import com.mongodb.management.JMXConnectionPoolListener;
import org.javers.spring.boot.mongo.JaversMongoAutoConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;

import static java.util.concurrent.TimeUnit.SECONDS;

@Configuration
@ComponentScan(basePackageClasses = MongoMappingPrinter.class)
@Import(HystrixConfiguration.class)
@EnableConfigurationProperties({MongoProperties.class})
@EnableAutoConfiguration(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class, JaversMongoAutoConfiguration.class})
@PropertySource("classpath:application-mongo.properties")
public class MongoCommonConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(MongoCommonConfiguration.class);

    /*
     * Configuration for mongo timeouts
     * */
    @Bean
    public MongoClientOptions mongoClientOptions() {
        return MongoClientOptions.builder()
                .connectTimeout((int) SECONDS.toMillis(120))
                .socketTimeout((int) SECONDS.toMillis(120))
                .maxConnectionIdleTime((int) SECONDS.toMillis(60))
                /*
                 * This is just to get the information when connection gets opened/used/closed
                 * */
                .addConnectionPoolListener(new JMXConnectionPoolListener())
                /*
                 * This is the most important change - if we dont set life-time of the connection, its ~8 minutes,
                 * tomcat will continue to create new connection, and not close old ones, unless the load goes down,
                 * and connection is idle for 60 seconds, or JVM starts to GC tomcat threads,
                 * which in case of service like attribution-service almost never.
                 *
                 * The connectionPerHost limit doesn't seem to affect this at all. With this timeout set, it's been tested
                 * than connections are getting closed even under significant load(in test case 250 threads).
                 * */
                .maxConnectionLifeTime((int) SECONDS.toMillis(60))
                .connectionsPerHost(600)
                .threadsAllowedToBlockForConnectionMultiplier(500)
                .build();
    }
}
