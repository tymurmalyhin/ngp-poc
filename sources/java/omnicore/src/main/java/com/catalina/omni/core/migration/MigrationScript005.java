package com.catalina.omni.core.migration;


import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;

import static com.mongodb.client.model.Filters.exists;
import static java.lang.Boolean.TRUE;

/**
 * We need to add isRealClip for all existing documents from OmniUserOfferClip - it's new property
 * and for all old documents have to be fill with value=true
 */
@ChangeLog(order = "005")
public class MigrationScript005 {
    private static Logger LOG = LoggerFactory.getLogger(MigrationScript005.class);

    @ChangeSet(author = "pziak", order = "05", id = "addRealFlagForExistingClipDocuments")
    public void addRealFlagForExistingClipDocuments(MongoTemplate template, Environment environment) {

        final MongoDatabase db = template.getDb();
        final String integratioDbName = environment.getProperty("mongodb.omnimfd.name");
        final String databaseName = db.getName().trim();

        //only relevant for mfd database
        if (databaseName.equalsIgnoreCase(integratioDbName)) {
            LOG.info("Script invoked for db {}", db.getName());
            MongoCollection<Document> clipCollection = db.getCollection("omniUserOfferClip");
            Document update = new Document("$set", new Document("realClip", TRUE));
            UpdateResult updateResult = clipCollection.updateMany(exists("_id"), update);
            LOG.info("Script changed in omniUserOfferClip {} documents.", updateResult.getModifiedCount());
        } else {
            LOG.info("Skipping execution of migration script 004, not relevant for db {}", databaseName);
        }
    }
}
