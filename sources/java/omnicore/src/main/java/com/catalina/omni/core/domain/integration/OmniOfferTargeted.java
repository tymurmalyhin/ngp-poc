package com.catalina.omni.core.domain.integration;


import com.catalina.omni.core.domain.enums.OmniOfferState;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

/**
 * Created by ziak on 9/26/18.
 */
@Document
@Getter
@Setter
@ToString
public class OmniOfferTargeted {

    @Id
    private String id;

    private String omniOfferId;
    private String clientOfferId;
    private String networkId;
    private OmniOfferState state;
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    @CreatedDate
    private LocalDateTime created;
}
