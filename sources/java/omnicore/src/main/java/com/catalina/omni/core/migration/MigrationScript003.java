package com.catalina.omni.core.migration;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

import static com.mongodb.client.model.Filters.*;
import static org.apache.commons.lang3.StringUtils.EMPTY;

@ChangeLog(order = "003")
public class MigrationScript003 {

    private static Logger logger = LoggerFactory.getLogger(MigrationScript003.class);

    private static final String DIGITAL_ID = "digitalId";
    private static final String ATTRIBUTION_COLLECTION = "AttributionV2";
    private static final String CID_MONITOR_COLLECTION = "AttributionCIDMonitorV2";

    @ChangeSet(author = "jregec", order = "03", id = "removeDigitalId")
    public void removeDigitalIds(MongoTemplate template, Environment environment) {

        final MongoDatabase db = template.getDb();
        final String integratioDbName = environment.getProperty("mongodb.omniattribution.name");
        final String databaseName = db.getName().trim();

        if (databaseName.equalsIgnoreCase(integratioDbName)) {
            logger.info("Script to remove digitalIds from Attribution and AttributionCidMonitor collections invoked for db {}", db.getName());

            removeDigitalIdForCollection(db.getCollection(ATTRIBUTION_COLLECTION));
            removeDigitalIdForCollection(db.getCollection(CID_MONITOR_COLLECTION));
        }
    }

    private void removeDigitalIdForCollection(MongoCollection<Document> collection) {
        AtomicLong updatedCounter = new AtomicLong();

        FindIterable<Document> documents = collection.find(exists(DIGITAL_ID));

        documents.forEach((Consumer<? super Document>) document -> {
            Document update = new Document("$unset", new Document(DIGITAL_ID, EMPTY));
            collection.updateOne(eq("_id", document.get("_id")), update);
            updatedCounter.incrementAndGet();
        });

        logger.info("In collection {}, script removeDigitalId removed {} digital id fields.", collection.getNamespace().getCollectionName(), updatedCounter.get());
    }
}
