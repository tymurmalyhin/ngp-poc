package com.catalina.omni.core.domain.solrresponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import lombok.Getter;

import java.util.List;

@Getter
public class SolrBody<T extends SolrDoc> {

    @JsonProperty("docs")
    private List<T> docs;
    @Expose
    private Long numFound;
    @Expose
    private Long start;
    @Expose
    private Double maxScore;

    public SolrBody() {
        // Empty default constructor
    }
}
