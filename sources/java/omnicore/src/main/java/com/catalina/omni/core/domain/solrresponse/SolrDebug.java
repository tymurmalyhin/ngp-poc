package com.catalina.omni.core.domain.solrresponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import lombok.Getter;

@Getter
public class SolrDebug {

    @Expose
    @JsonProperty("rawquerystring")
    private String rawQueryString;
    @Expose
    @JsonProperty("querystring")
    private String queryString;
    @Expose
    @JsonProperty("parsedquery")
    private String parsedQuery;
    @Expose
    @JsonProperty("parsedquery_toString")
    private String parsedQueryToString;
    @Expose
    @JsonProperty("QParser")
    private String qParser;

    public SolrDebug() {
        // Empty default constructor
    }
}