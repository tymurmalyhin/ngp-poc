package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.domain.omniattribution.LineItem;

import java.util.List;

public interface LineItemRepoCustom {
    List<LineItem> findAllFromCurrentCampaignWithSuppressionOrRetargetSegments() ;
}
