package com.catalina.omni.core.domain.omniattribution;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kirk on 4/23/18.
 */
@Document
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class LineItem {
    @Id
    private String id;
    private String name;
    private String campaignId;
    private String channelScreenType;
    private BiddingStrategy biddingStrategy;
    private BigDecimal budget;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    @CreatedDate
    private LocalDateTime createdDate;
    private List<FrequencyCap> frequencyCaps;
    private String countryCode;
    private Format format;
    private String tag;
    private int sortOrder;
    private String promotionId;
    private List<Creative> creative = new ArrayList<>();
    private String lastUpdatedBy;
    private boolean suppressSegment;
    private boolean retargetingSegment;
    private String suppressionSegmentKey;
    private String retargetingSegmentKey;
    private String platformName;
}
