package com.catalina.omni.core.hystrix.capability;

import com.catalina.omni.core.hystrix.matcher.MatchableByEqualsIgnoreId;
import com.catalina.omni.core.hystrix.matcher.MatchableById;
import java.io.Serializable;

/**
 * Interface that domain class should implement to support upsert aggregation(save or update)
 * */
public interface UpsertAggregationCapable<T extends Serializable>
    extends MatchableById<T>, MatchableByEqualsIgnoreId<T> {

}
