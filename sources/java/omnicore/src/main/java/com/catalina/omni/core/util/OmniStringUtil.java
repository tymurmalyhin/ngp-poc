package com.catalina.omni.core.util;

import com.catalina.omni.core.domain.enums.CountryEnum;
import com.catalina.omni.core.domain.integration.OmniUserOffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OmniStringUtil {

    private static final String LEADING_ZEROS_REGEX = "^0+";

    public enum CidPart {
        COUNTRY_CODE, NETWORK_ID, FSC_ID
    }

    public static String toCanonicalCid(CountryEnum countryCode, String networkId, String id) {
        return String.join("-", countryCode.name(), String.format("%4s", networkId).replace(' ', '0'), id);
    }

    public static String obtainCid(OmniUserOffer userOffer, String fscId) {
        return toCanonicalCid(CountryEnum.valueOf(userOffer.getCountryCode()), userOffer.getNetworkId(), fscId);
    }

    public static String getCidPart(String cid, CidPart cidPart) {
        return cid.split("-")[cidPart.ordinal()];
    }

    public static String removeZerosFromNetworkId(String networkId) {
        return networkId.replaceFirst(LEADING_ZEROS_REGEX, "");
    }
}
