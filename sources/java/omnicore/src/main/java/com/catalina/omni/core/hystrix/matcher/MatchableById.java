package com.catalina.omni.core.hystrix.matcher;

import java.io.Serializable;

/**
 * Interface to match results for update operation
 */
public interface MatchableById<T extends Serializable> extends Serializable, IdAware {

    boolean matchById(T other);
}
