package com.catalina.omni.core.domain.omniattribution;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class AttributionCIDMonitorRequest implements Serializable {

    /** serialVersionUID */
    private static final long serialVersionUID = 8601801028597970428L;

    private AttributionCIDMonitor attributionCIDMonitor;
    private String orderId;
    private String networkId;
    private List<String> upcs;
}
