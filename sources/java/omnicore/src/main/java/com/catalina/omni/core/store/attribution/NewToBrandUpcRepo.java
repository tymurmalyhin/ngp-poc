package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.domain.omniattribution.NewToBrandUpc;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NewToBrandUpcRepo extends MongoRepository<NewToBrandUpc, String> {
}
