package com.catalina.omni.core.domain.enums;

public  enum CountryEnum implements BaseEnum {
    USA(1);

    private int id;

    CountryEnum(int id) {
        this.id = id;
    }

    @Override
    public int getId() {
        return id;
    }

    public static CountryEnum getById(int id) {
        for (CountryEnum country : CountryEnum.values()) {
            if (country.getId() == id) {
                return country;
            }
        }
        return null;
    }
}