package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.domain.omniattribution.Attribution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;
import java.util.Set;

public class AttributionRepoImpl implements  AttributionRepoCustom {

    @Autowired
    @Qualifier("attributionTemplate")
    private MongoTemplate mongoTemplate;

    @Override
    public List<Attribution> getByLineItemsIdAndNotSuppressed(Set<String> lineItemIds, int index, int number) {
        Query query = new Query(Criteria.where("lineItems")
                .elemMatch(Criteria.where("lineItemId")
                .in(lineItemIds)
                .orOperator(Criteria.where("suppressed")
                    .exists(false), Criteria.where("suppressed")
                .is(Boolean.FALSE))));
        query = query.skip((long)index*number).limit(number);
        return mongoTemplate.find(query, Attribution.class);
    }

    @Override
    public List<Attribution> getByLineItemsIdAndNotRetargeted(Set<String> lineItemIds, int index, int number) {
        Query query = new Query(Criteria.where("lineItems")
                .elemMatch(Criteria.where("lineItemId")
                .in(lineItemIds)
                .orOperator(Criteria.where("retargeted")
                        .exists(false), Criteria.where("retargeted")
                        .is(Boolean.FALSE))));
        query = query.skip((long)index*number).limit(number);
        return mongoTemplate.find(query, Attribution.class);
    }
}
