package com.catalina.omni.core.domain.integration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document
@Getter
@Setter
public class OmniUserOfferClip {

    @Id
    private String id;
    private String omniOfferId;
    private String clientOfferId;
    private String cid;
    private LocalDateTime dateOfClip;
    private LocalDateTime clipIngestionToVault;
    private boolean realClip;

    private ClipResult.ClipResultStage state;
}
