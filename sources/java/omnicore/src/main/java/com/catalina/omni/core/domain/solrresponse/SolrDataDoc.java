package com.catalina.omni.core.domain.solrresponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class SolrDataDoc extends SolrDoc {

    @JsonProperty("data")
    private SolrData data;

    public SolrDataDoc() {
        // Empty default constructor
    }
}