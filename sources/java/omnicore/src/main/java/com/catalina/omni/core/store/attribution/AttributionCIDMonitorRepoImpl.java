package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.domain.omniattribution.AttributionCIDMonitor;
import com.catalina.omni.core.domain.omniattribution.LineItemImpressionEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AttributionCIDMonitorRepoImpl implements AttributionCIDMonitorRepoCustom {

    private static final Logger LOG = LoggerFactory.getLogger(AttributionCIDMonitorRepoImpl.class);

    @Autowired
    @Qualifier("attributionTemplate")
    private MongoTemplate mongoTemplate;

    @Autowired
    private AttributionCIDMonitorRepo repo;

    /**
     * Updates AttributionCIDMonitor directly using mongoTemplate bulk update:
     * <br/>It does:
     * <ul>
     * <li>create AttributionCIDMonitor, if it isn't exist (use update.setOnInsert)</li>
     * <li>update all necessary fields (use update.set)</li>
     * <li>add LineItemImpressionEvent to lineItems set (use update.addToSet)</li>
     * <li>update all in one DB command (to decrease rate limit on DB)</li>
     * </ul>
     * <p>
     * This implementation eliminate parallel processing issues:
     * <ul>
     * <li>duplicate AttributionCidMonitors creation in case,
     * if parallel threads concurrently detect that specific AttributionCIDMonitor doesn't exist
     * and they create it two/more times</li>
     * <li>not process or remove same LineItemImpressionEvent,
     * if parallel threads read together the same AttributionCIDMonitor in specific state,
     * every threads update its AttributionCIDMonitor only with its changes and they write it to DB,
     * only last change is valid, other are re-write</li>
     * <li>updates only one of duplicated AttributionCIDMonitor if there are duplicate in DB</li>
     * </ul>
     */
    @Override
    public AttributionCIDMonitor createOrUpdateCidMonitor(String cid, String campaignId, LocalDateTime endDate, LineItemImpressionEvent event) {
        Query query = new Query(Criteria.where("cid").is(cid).and("campaignId").is(campaignId));

        final List<AttributionCIDMonitor> matches = mongoTemplate.find(query, AttributionCIDMonitor.class);

        //this first part ensures that we don't create further duplications, if there is a duplicate record
        if (matches.isEmpty()) {
            final AttributionCIDMonitor cidMonitor = new AttributionCIDMonitor();
            cidMonitor.setCid(cid);
            cidMonitor.setCampaignId(campaignId);
            cidMonitor.setEndDate(endDate);
            cidMonitor.setLineItems(Collections.singletonList(event));
            return mongoTemplate.insert(cidMonitor);
        } else {

            final LocalDateTime nowDate = LocalDateTime.now();
            final Update update = new Update()
                    .setOnInsert("cid", cid)
                    .setOnInsert("campaignId", campaignId)
                    .setOnInsert("dateCreated", nowDate)
                    .set("endDate", endDate)
                    .set("lastUpdateDate", nowDate)
                    .addToSet("lineItems", event);

            // --- 'Direct' solution based on bulk operations ---
            final BulkOperations bulkOps = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, AttributionCIDMonitor.class);

            matches.forEach(cidMonitor -> bulkOps.upsert(new Query(Criteria.where("_id").is(cidMonitor.getId())), update));

            bulkOps.execute();
        }


        // find all AttributionCIDMonitors (for given criteria) to return + duplicity processing if needed
        query.with(new Sort(Sort.Direction.ASC, "dateCreated"));
        List<AttributionCIDMonitor> cidMonitors = repo.findAllByCampaignIdAndCid(campaignId, cid);
        if (cidMonitors.size() > 1) {
            // duplicity processing, merge of the same AttributionCIDMonitors
            // in case if DB parallel process two update commands
            // detect that specific AttributionCIDMonitor doesn't exist and they create it two or more times
            AttributionCIDMonitor mainRec = cidMonitors.get(0);
            query = new Query(Criteria.where("_id").is(mainRec.getId()));

            final Set<LineItemImpressionEvent> lineItemsOfOfDuplicates = cidMonitors.stream()
                    .skip(1)
                    .map(AttributionCIDMonitor::getLineItems)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toSet());

            final Set<String> idsOfDuplicates = cidMonitors.stream()
                    .skip(1)
                    .map(AttributionCIDMonitor::getId)
                    .collect(Collectors.toSet());

            final Update partialUpdate = new Update().addToSet("lineItems").each(lineItemsOfOfDuplicates);

            mongoTemplate.updateFirst(query, partialUpdate, AttributionCIDMonitor.class);

            /*
             * Has to be done trough bulk, regular remove by id in [...] does not work on sharded collection
             * */
            final BulkOperations bulkDelete = mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, AttributionCIDMonitor.class);
            idsOfDuplicates.forEach(id -> bulkDelete.remove(new Query(Criteria.where("_id").is(id))));
            bulkDelete.execute();

            LOG.info("AttributionCIDMonitor duplicity processed. size: {}, Final Id: {}", cidMonitors.size(), mainRec.getId());
            //to return updated record
            return mongoTemplate.findById(mainRec.getId(), AttributionCIDMonitor.class);
        }

        return cidMonitors.isEmpty() ? null : cidMonitors.get(0);
    }

}