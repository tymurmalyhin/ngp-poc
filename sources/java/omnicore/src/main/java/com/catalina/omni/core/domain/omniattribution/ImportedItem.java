package com.catalina.omni.core.domain.omniattribution;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document
@Getter
@Setter
public class ImportedItem {
    @Id
    private String id;
    private LocalDateTime importedDate;
    private String serviceName;
    private String importedId;
    private String salesForceId;
}
