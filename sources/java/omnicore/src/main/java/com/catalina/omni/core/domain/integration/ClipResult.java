package com.catalina.omni.core.domain.integration;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ClipResult {
    private boolean addLoyaltyResult;
    private Integer addLoyaltyStatusCode;
    private boolean clipResult;
    private Integer clipStatusCode;
    private String cid;
    private ClipResultStage clipResultStage;

    public enum ClipResultStage {
        /*
         * Request to clip this failed
         * */
        UNCLIPPED,

        /**
         * Request to clip this failed, but request to ingest this clip to vault failed
         */
        CLIPPED,

        /*
         * Both request to clip and to ingest to vault passed
         * */
        CLIPPED_AND_INGESTED
    }
}
