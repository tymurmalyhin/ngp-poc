package com.catalina.omni.core.domain.omniattribution;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Network {
    private int id;
    private String name;
}
