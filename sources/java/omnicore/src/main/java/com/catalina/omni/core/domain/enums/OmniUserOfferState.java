package com.catalina.omni.core.domain.enums;

public enum OmniUserOfferState {
    IN_PROGRESS, COMPLETED, ERROR
}
