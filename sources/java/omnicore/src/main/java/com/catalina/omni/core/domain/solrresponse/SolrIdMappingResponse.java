package com.catalina.omni.core.domain.solrresponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import lombok.Getter;

@Getter
public class SolrIdMappingResponse {

    @Expose
    @JsonProperty("responseHeader")
    private SolrHeader header;

    @Expose
    @JsonProperty("response")
    private SolrBody<SolrDataDoc> body;

    @Expose
    private SolrDebug debug;

    public SolrIdMappingResponse() {
        // Empty default constructor
    }
}
