package com.catalina.omni.core.configuration;

import com.catalina.omni.core.controller.SlackTestController;
import com.catalina.omni.core.slack.NotificationService;
import com.catalina.omni.core.slack.SlackNotificationServiceImpl;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties(SlackProperties.class)
@Import(SlackTestController.class)
public class SlackConfiguration {

    @Bean
    public NotificationService slackNotification() {
        return new SlackNotificationServiceImpl();
    }
}
