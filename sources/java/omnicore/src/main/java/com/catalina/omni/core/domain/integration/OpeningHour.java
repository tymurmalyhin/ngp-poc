package com.catalina.omni.core.domain.integration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OpeningHour {
    private DayTime open;
    private DayTime close;
}
