package com.catalina.omni.core.domain.solrresponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import lombok.Getter;

import java.util.LinkedHashMap;

@Getter
public class SolrHeader {
    @Expose
    private boolean zkConnected;

    @Expose
    @JsonProperty("params")
    private LinkedHashMap<String, Object> params;

    @Expose
    @JsonProperty("QTime")
    private Long qTime;

    @Expose
    private Long status;

    public SolrHeader() {
        // Empty default constructor
    }
}