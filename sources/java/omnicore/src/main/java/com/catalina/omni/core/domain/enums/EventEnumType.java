package com.catalina.omni.core.domain.enums;

public class EventEnumType extends EnumUserType<EventType> {
    @Override
    public BaseEnum getById(int id) {
        return EventType.getById(id);
    }
}
