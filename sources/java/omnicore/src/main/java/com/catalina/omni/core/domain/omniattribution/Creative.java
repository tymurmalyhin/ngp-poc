package com.catalina.omni.core.domain.omniattribution;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Creative {

    private String id;
    private String name;
    private String previewURL;
    private String description;
}
