package com.catalina.omni.core.hystrix.command.attribution.cidmonitor.save.factory;

import com.catalina.omni.core.configuration.hystrix.HystrixCommandConfiguration;
import com.catalina.omni.core.configuration.hystrix.operations.Update;
import com.catalina.omni.core.hystrix.command.attribution.cidmonitor.save.BulkUpdateOperationCollapser;
import com.catalina.omni.core.hystrix.param.save.BulkUpdateCommandParam;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

public class BulkUpdateCommandFactory {

    private final MongoTemplate mongoTemplate;
    private final HystrixCommandConfiguration commandConfiguration;

    public BulkUpdateCommandFactory(MongoTemplate mongoTemplate, HystrixCommandConfiguration commandConfiguration) {
        this.mongoTemplate = mongoTemplate;
        this.commandConfiguration = commandConfiguration;
    }

    public BulkUpdateOperationCollapser<?> createCommand(String collectionName, Class<?> entityClass, Query query,
            org.springframework.data.mongodb.core.query.Update update, boolean upsert, boolean multi) {
        final BulkUpdateCommandParam<?> param = new BulkUpdateCommandParam<>(query, update, upsert, multi, collectionName, entityClass);
        final Update config = commandConfiguration.getUpsert().get(entityClass.getSimpleName()).getUpdate();
        return new BulkUpdateOperationCollapser<>(param, config, mongoTemplate);
    }
}
