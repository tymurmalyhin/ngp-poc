package com.catalina.omni.core.domain.omniattribution;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by ziak on 8/7/18.
 */
@Document(collection = "AttributionV2")
@Getter
@Setter
public class Attribution {
    @Id
    private String id;
    private String campaignId;
    private String cid;
    private List<String> orderIds;
    private List<LineItemImpressionEvent> lineItems;

    @CreatedDate
    private LocalDateTime createdDate;

    @LastModifiedDate
    private LocalDateTime lastUpdateDate;
}
