package com.catalina.omni.core.slack;

import com.catalina.omni.core.configuration.SlackProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

import static java.util.Locale.getDefault;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;

@Service
public class SlackNotificationServiceImpl implements NotificationService {

    private static final String BASE_SLACK_USERNAME = "slack.username";

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private SlackProperties slackPropertiesConfiguration;
    @Autowired
    private MessageSource messageSource;

    @Override
    public void sendMessage(String message) {
        sendMessage(message, null);
    }

    @Override
    public void sendMessage(String message, String source) {
        if (!slackPropertiesConfiguration.isSlackNotificationsTurnOn()) {
            return;
        }

        String userName = translateSourceToUserName(source);
        // in case of error in source-to-user-name translating, get default user but display source as msg prefix
        if (userName == null && source != null) {
            userName = translateSourceToUserName(null);
            message = "[" + source + "]: " + message;
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("token", slackPropertiesConfiguration.getSlackToken());
        map.add("channel", slackPropertiesConfiguration.getSlackChannel());
        map.add("username", userName);
        map.add("text", message);

        restTemplate.postForObject(slackPropertiesConfiguration.getSlackWebHookURL(), new HttpEntity<>(map, headers), HashMap.class);

    }

    /**
     * Translates source to slack user name
     * @param source
     * @return translated slack user name; null otherwise
     */
    private String translateSourceToUserName(String source) {
        String messageCode = isNotBlank(source) ?
                BASE_SLACK_USERNAME + "." + source.toLowerCase() : BASE_SLACK_USERNAME;

        String userName = messageSource.getMessage(messageCode, null, getDefault());
        return isNotBlank(userName) ? userName : null;
    }


}
