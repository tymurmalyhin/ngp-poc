package com.catalina.omni.core.store.attribution;

import com.catalina.omni.core.domain.omniattribution.Campaign;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by kirk on 4/24/18.
 */

@Repository
@JaversSpringDataAuditable
public interface CampaignRepo extends MongoRepository<Campaign, String> {
    List<Campaign> findByUpcsIn(List<String> upcs);
    Page<Campaign> findByAdvertiserId(Pageable pageable, String advertiserId);
    List<Campaign> findByAdvertiserId(String advertiserId);
    Page<Campaign> findByNameLikeIgnoreCase(Pageable pageable, String name);
    Page<Campaign> findByAdvertiserIdAndNameLikeIgnoreCase(Pageable pageable, String advertiserId, String name);
    @Query(value = "{ lineItems: { $elemMatch: { _id: { $eq: ?0 } } } }")
    List<Campaign> findByLineItemsId(String name);
    @Query(value = "{ lineItems: { $elemMatch: { _id: { $in: ?0 } } } }")
    List<Campaign> findByLineItemsIdIn(List<String> ids);
    @Query(value = "{ lineItems: { $elemMatch: { name: { $eq: ?0 } } } }")
    List<Campaign> findByLineItemsName(String name);
    Stream<Campaign> findByLineItemsPromotionId(String promotionId);
    @Query(value = "{lineItems: {$elemMatch: { $or : [ { $and :[ {suppressionSegmentKey: { $exists: false} },  { suppressSegment: true}]},{ $and: [ { retargetingSegmentKey: {$exists: false}}, {retargetingSegment: true}] }]}}}" )
    List<Campaign> findByLineItemWithCreationSegmentsOnButWithoutThem();
    Stream<Campaign> findByLineItemsPromotionIdAndEndDateAfter(String promotionId, LocalDateTime now);
    List<Campaign> findByStartDateLessThanEqualAndEndDateGreaterThanEqual(LocalDateTime start, LocalDateTime end);
}