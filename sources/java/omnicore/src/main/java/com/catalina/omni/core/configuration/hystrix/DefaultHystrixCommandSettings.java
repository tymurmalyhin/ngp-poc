package com.catalina.omni.core.configuration.hystrix;

/**
 * Whole workflow of the command - https://raw.githubusercontent.com/wiki/Netflix/Hystrix/images/collapser-flow-1280.png
 */
public abstract class DefaultHystrixCommandSettings {

    /**
     * Maximum number of requests that can be fit to the batch. The more complex the query is, the
     * lower this value should be.
     *
     * Example:
     * <li>Find by primary index - value should be in thousands</li>
     * <li>Find by secondary index - value should be in tens/hundreds</li>
     * <li>Find by multiple secondary indexes - value should be in tens </li>
     */
    private Integer maxRequestsInBatch;

    /**
     * This determines at what interval will be the request queue drained for requests. This should
     * be configured in combination with maxRequestsInBatch to prevent request queue filling up to
     * much. The formula is basically maxRequestsInBatch * (1000/timeWindowSize) equals maximum
     * requests throughput per second
     */
    private Integer timeWindowSize;

    /**
     * Percentage of requests per observed time-window that have to fail in order for circuit to
     * open. When dealing with database we should use lower values as it is basically not possible
     * for use to invoke invalid request due to use spring repos, therefore all the failures will be
     * of destructive fashion(stuff like connection failures)
     */
    private Integer circuitBreakerErrorThresholdPercentage;

    /**
     * Time window that circuit should wait till closing again
     */
    private Integer sleepWindowInMilliseconds;

    public Integer getMaxRequestsInBatch() {
        return maxRequestsInBatch;
    }

    public void setMaxRequestsInBatch(Integer maxRequestsInBatch) {
        this.maxRequestsInBatch = maxRequestsInBatch;
    }

    public Integer getTimeWindowSize() {
        return timeWindowSize;
    }

    public void setTimeWindowSize(Integer timeWindowSize) {
        this.timeWindowSize = timeWindowSize;
    }

    public Integer getCircuitBreakerErrorThresholdPercentage() {
        return circuitBreakerErrorThresholdPercentage;
    }

    public void setCircuitBreakerErrorThresholdPercentage(
            Integer circuitBreakerErrorThresholdPercentage) {
        this.circuitBreakerErrorThresholdPercentage = circuitBreakerErrorThresholdPercentage;
    }

    public Integer getSleepWindowInMilliseconds() {
        return sleepWindowInMilliseconds;
    }

    public void setSleepWindowInMilliseconds(Integer sleepWindowInMilliseconds) {
        this.sleepWindowInMilliseconds = sleepWindowInMilliseconds;
    }
}
