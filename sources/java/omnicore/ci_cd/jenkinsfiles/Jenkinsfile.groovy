#!/usr/bin/env groovy

SCM_BRANCH = SCM_BRANCH.minus("origin/")

print("SCM_BRANCH: ${SCM_BRANCH}")

node {

    stage('Clone sources') {
        git(
                url: 'git@bitbucket.org:cameosaas/omnicore.git',
                credentialsId: 'bitbucket-id',
                branch: SCM_BRANCH
        )
    }

    stage('Build Jar File') {
        withMaven(maven: 'M3', jdk: 'jdk8') {
            sh('mvn clean install')
        }
    }
}
