var omniattributionMongoDb = db.getSiblingDB('omniattribution')

function MongoIndexHolder() {
    this.collectionMap = new Map()
    this.keys = []
}

function IndexRequest(collectionName) {
    this.indexes = []
    this.collectionName = collectionName
}

MongoIndexHolder.prototype.getCollection = function(name) {
    if (this.collectionMap.get(name) !== null) {
        return this.collectionMap.get(name)
    }
    var indexRequest = new IndexRequest(name)
    this.keys.push(name)
    this.collectionMap.put(name, indexRequest)
    return indexRequest
}

IndexRequest.prototype.dropIndex = function(indexName) {
    printjson(omniattributionMongoDb.getCollection(this.collectionName).dropIndex(indexName))
}


var omniattribution = new MongoIndexHolder()

omniattribution.getCollection('AttributionV2').dropIndex('cid_1')
omniattribution.getCollection('AttributionV2').dropIndex('fscId_1')

