function MongoIndexHolder() {
    this.collectionMap = new Map()
    this.keys = []
}

function IndexRequest() {
    this.indexes = []
}

MongoIndexHolder.prototype.getCollection = function(name) {
    if (this.collectionMap.get(name) !== null) {
        return this.collectionMap.get(name)
    }
    var indexRequest = new IndexRequest()
    this.keys.push(name)
    this.collectionMap.put(name, indexRequest)
    return indexRequest
}

IndexRequest.prototype.createIndex = function(key, options) {
    this.indexes.push([key, options])
}


var omniattribution = new MongoIndexHolder()


//AttributionCIDMonitorV2
omniattribution.getCollection('AttributionCIDMonitorV2').createIndex({"campaignId":1, "cid":1},{"background":true,"name":"attribution_cid-monitor_idx1"})
omniattribution.getCollection('AttributionCIDMonitorV2').createIndex({"endDate":1},{"expireAfterSeconds":0,"name":"endDate"})
// com.catalina.omni.core.domain.omniattribution.LineItemImpressionEvent
omniattribution.getCollection('AttributionCIDMonitorV2').createIndex({"lineItems.impressionDate":1},{"background":true,"name":"lineItems.impressionDate"})
omniattribution.getCollection('AttributionCIDMonitorV2').createIndex({"lineItems.tealiumEventId":1},{"background":true,"name":"lineItems.tealiumEventId"})


//AttributionV2
omniattribution.getCollection('AttributionV2').createIndex({"cid":1,"campaignId":1},{"name":"attribution_idx"})

//com.catalina.omni.core.domain.omniattribution.LineItemImpressionEvent
omniattribution.getCollection('AttributionV2').createIndex({"lineItems.impressionDate":1},{"name":"lineItems.impressionDate"})
omniattribution.getCollection('AttributionV2').createIndex({"lineItems.tealiumEventId":1},{"background":true,"name":"lineItems.tealiumEventId"})
// Missing index - inside db but not mapped by any entity
omniattribution.getCollection('AttributionV2').createIndex({"cid":1},{"name":"cid"})
omniattribution.getCollection('AttributionV2').createIndex({"fscId":1},{"name":"fscId"})

//StatisticsTealium
omniattribution.getCollection('StatisticsTealium').createIndex({"day":1,"campaignId":1,"type":1,"typeValue":1},{"background":true,"name":"statistics_tealium_idx1","unique": true})

function printTextAndJson(text, object) {
    print(text + JSON.stringify(object, null, 2))
}

var omniattributionMongoDb = db.getSiblingDB('omniattribution')

function checkIndexes(collectionName, expectedIndexNames) {
    var indexes = omniattributionMongoDb[collectionName].getIndexes()

    for (index in indexes) {
        var indexName = indexes[index].name

        if (!expectedIndexNames.includes(indexes[index].name)) {
            print('WARN: Unknown index found at collection ' + collectionName + 'index name is ' + indexName)
        }

    }
}

function verifyCollection(collectionName, indexDefinitions) {
    print('Going to check collection ' + collectionName)

    var validIndexNames = []
    for (obj in indexDefinitions) {
        var indexName = indexDefinitions[obj][1].name
        validIndexNames.push(indexName)
    }

    checkIndexes(collectionName, validIndexNames)

    for (obj in indexDefinitions) {
        var keys = indexDefinitions[obj][0]
        var options = indexDefinitions[obj][1]
        var result = omniattributionMongoDb.getCollection(collectionName).createIndex(keys, options)

        if (result.ok === 1) {
            if (result.note === "all indexes already exist") {
                print('Same index ' + options.name + ' already exists.')
            }  else {
                print('New index ' + options.name + ' created.')
            }
        }  else {
            printTextAndJson(options.name + ' result = ' , result)
        }

    }

    print('Processing finished for collection ' + collectionName)
}

for (i in omniattribution.keys) {
    var collectionName = omniattribution.keys[i]
    verifyCollection(collectionName, omniattribution.collectionMap.get(collectionName).indexes)
}
