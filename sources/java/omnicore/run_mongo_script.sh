echo 'Please choose environment to run mongo indices.'
echo '--------------------'
echo '1. SQA'
echo '2. PROD '
echo '--------------------'
read env


if [ $env != "1" ] && [ $env != "2" ]; then
        echo 'Invalid input exiting..'
    exit
fi


if [ $env = "2" ]; then
    echo 'You are going to run script on production. Are you sure ? Please type YES.'
    read agrement

    if [ $agrement != "YES" ]; then
        echo 'Invalid input exiting..'
        exit
    fi
fi


echo 'Please choose db'
echo '--------------------'
echo '1. OmniAttribution'
echo '2. OmniMfd '
echo '--------------------'
read dbName

name=default
if [ $dbName = "1" ]; then
        name='mongo-indices-attribution.js';
    elif  [ $dbName = "2" ]; then
        name='mongo-indices-omnimfd.js';
    else
        echo 'Invalid input. Exiting script';
    exit
fi

mongoPath=default
if [ $env = "1" ]; then
        mongoPath='mongodb://username:password@10.166.103.210:32100,10.166.103.210:32101,10.166.103.210:32102/?authSource=admin&authMechanism=SCRAM-SHA-1&replicaSet=rs0&readPreference=secondaryPreferred';
    elif  [ $env = "2" ]; then
        mongoPath='ADD PRODUCTION ADDRESS';
    else
        echo 'Invalid input. Exiting script';
        exit
fi

mongo $mongoPath $name




