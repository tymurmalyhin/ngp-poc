function MongoIndexHolder() {
    this.collectionMap = new Map()
    this.keys = []
}

function IndexRequest() {
    this.indexes = []
}

MongoIndexHolder.prototype.getCollection = function(name) {
    if (this.collectionMap.get(name) !== null) {
        return this.collectionMap.get(name)
    }
    var indexRequest = new IndexRequest()
    this.keys.push(name)
    this.collectionMap.put(name, indexRequest)
    return indexRequest
}

IndexRequest.prototype.createIndex = function(key, options) {
     this.indexes.push([key, options])
}


var omnimfd = new MongoIndexHolder()


//touchpointToken - com.catalina.omni.core.domain.integration.TouchpointToken
omnimfd.getCollection('touchpointToken').createIndex({"country": 1, "networkId": 1}, {"name": "touchpoint_token_idx", "background": true})
omnimfd.getCollection('touchpointToken').createIndex({"country": 1}, {"name": "country", "background": true})
omnimfd.getCollection('touchpointToken').createIndex({"networkId": 1}, {"name": "networkId", "background": true})

//storeDetail - com.catalina.omni.integration.domain.StoreDetail
omnimfd.getCollection('storeDetail').createIndex({"location": "2dsphere"}, {"name": "location_2dsphere", "background": true})
omnimfd.getCollection('storeDetail').createIndex({"location": 1}, {"name": "location", "background": true})
omnimfd.getCollection('storeDetail').createIndex({"location": "hashed"}, {"name": "location_hashed", "background": true})

//store - com.catalina.omni.core.domain.integration.Store
omnimfd.getCollection('store').createIndex({"location": "2dsphere"}, {"name": "location_2dsphere", "background": true})
omnimfd.getCollection('store').createIndex({"location": 1}, {"name": "location", "background": true})
omnimfd.getCollection('store').createIndex({"location": "hashed"}, {"name": "location_hashed", "background": true})

//New Index
omnimfd.getCollection('store').createIndex({"updated": 1}, {"name": "updated", "background": true})

//shedLock

//Drop cosmo index
//omnimfd.getCollection('omniUserOfferV2').dropIndex('_ts')

//omniUserOfferV2 - com.catalina.omni.core.domain.integration.OmniUserOffer
omnimfd.getCollection('omniUserOfferV2').createIndex({
    "countryCode": 1,
    "networkId": 1,
    "digitalId": 1
}, {"name": "omni_user_offer_idx", "background": true})
omnimfd.getCollection('omniUserOfferV2').createIndex({"countryCode": 1}, {"name": "countryCode", "background": true})
omnimfd.getCollection('omniUserOfferV2').createIndex({"networkId": 1}, {"name": "networkId", "background": true})
omnimfd.getCollection('omniUserOfferV2').createIndex({"digitalId": 1}, {"name": "digitalId", "background": true})
omnimfd.getCollection('omniUserOfferV2').createIndex({"fscId": 1}, {"name": "fscId", "background": true})

//omniUserOfferV2 - entity com.catalina.omni.core.domain.integration.OmniOffer
omnimfd.getCollection('omniUserOfferV2').createIndex({
    "omniOffer.state": 1,
    "omniOffer.networkId": 1,
    "omniOffer.endDate": 1,
    "omniOffer.startDate": 1
}, {"name": "omniOffer.omni_offer_idx", "background": true})
omnimfd.getCollection('omniUserOfferV2').createIndex({"created": 1}, {"expireAfterSeconds": 3600, "name": "created", "background": true})
omnimfd.getCollection('omniUserOfferV2').createIndex({"omniOffer.state": 1}, {"name": "omniOffer.state", "background": true})
omnimfd.getCollection('omniUserOfferV2').createIndex({"omniOffer.startDate": 1}, {"name": "omniOffer.startDate", "background": true})
omnimfd.getCollection('omniUserOfferV2').createIndex({"omniOffer.endDate": 1}, {"name": "omniOffer.endDate", "background": true})
omnimfd.getCollection('omniUserOfferV2').createIndex({"omniOffer.networkId": 1}, {"name": "omniOffer.networkId", "background": true})


// New indexes
omnimfd.getCollection('omniUserOfferV2').createIndex({
    "countryCode": 1,
    "networkId": 1,
    "digitalId": 1,
    "omniOffer.offerType": 1
}, {"name": "omni_user_offer_idx_2", "background": true})


omnimfd.getCollection('omniUserOfferV2').createIndex({
    "countryCode": 1,
    "networkId": 1,
    "fscId": 1,
    "omniOffer.offerType": 1
}, {"name": "omni_user_offer_idx_3", "background": true})

omnimfd.getCollection('omniUserOfferV2').createIndex({"omniOffer.offerType": 1}, {"name": "omniOffer.offerType", "background": true})
omnimfd.getCollection('omniUserOfferV2').createIndex({"omniOffer.rank": -1}, {"name": "omniOffer.rank", "background": true})

//OmniUserOfferClip - com.catalina.omni.core.domain.integration.OmniUserOfferClip
omnimfd.getCollection('omniUserOfferClip').createIndex({
    "omniOfferId": 1,
    "cid": 1,
    "clipIngestionToVault": 1
}, {"name": "user_offer_clip_ingest_idx", "background": true})
omnimfd.getCollection('omniUserOfferClip').createIndex({"omniOfferId": 1}, {"name": "omniOfferId", "background": true})
omnimfd.getCollection('omniUserOfferClip').createIndex({"cid": 1}, {"name": "cid", "background": true})
omnimfd.getCollection('omniUserOfferClip').createIndex({"clipIngestionToVault": 1}, {"name": "clipIngestionToVault", "background": true})


//New index
omnimfd.getCollection('omniUserOfferClip').createIndex({"clientOfferId": 1, "cid": 1}, {"name": "client_offer_id_cid_idx", "background": true})
omnimfd.getCollection('omniUserOfferClip').createIndex({"state": 1}, {"name": "state", "background": true})

//Not inside repo - drop or not ??
omnimfd.getCollection('omniUserOfferClip').createIndex({"fscIds": 1}, {"name": "fscIds", "background": true})

//omniUser - com.catalina.omni.core.domain.integration.OmniUser
omnimfd.getCollection('omniUser').createIndex({
    "countryCode": 1,
    "networkId": 1,
    "fscId": 1},
    {"name": "omni_user_idx", "background": true})
omnimfd.getCollection('omniUser').createIndex({
    "countryCode": 1,
    "networkId": 1,
    "digitalId": 1
}, {"name": "omni_user_idx_2", "background": true})
omnimfd.getCollection('omniUser').createIndex({"countryCode": 1}, {"name": "countryCode", "background": true})
omnimfd.getCollection('omniUser').createIndex({"digitalId": 1}, {"name": "digitalId", "background": true})
omnimfd.getCollection('omniUser').createIndex({"fscId": 1}, {"name": "fscId", "background": true})
omnimfd.getCollection('omniUser').createIndex({"networkId": 1}, {"name": "networkId", "background": true})


//omniOfferTargeted - com.catalina.omni.core.domain.integration.OmniOfferTargeted
omnimfd.getCollection('omniOfferTargeted').createIndex({
    "state": 1,
    "networkId": 1,
    "endDate": 1,
    "startDate": 1
}, {"name": "omni_offer_targeted_idx", "background": true})


omnimfd.getCollection('omniOfferTargeted').createIndex({"networkId": 1}, {"name": "networkId", "background": true})
omnimfd.getCollection('omniOfferTargeted').createIndex({"state": 1}, {"name": "state", "background": true})
omnimfd.getCollection('omniOfferTargeted').createIndex({"startDate": 1}, {"name": "startDate", "background": true})
omnimfd.getCollection('omniOfferTargeted').createIndex({"endDate": 1}, {"name": "endDate", "background": true})

//New index
omnimfd.getCollection('omniOfferTargeted').createIndex({
    "networkId": 1,
    "clientOfferId": 1
}, {"name": "omni_offer_targeted_idx_2", "background": true})


//omniOffer - com.catalina.omni.core.domain.integration.OmniOffer
omnimfd.getCollection('omniOffer').createIndex({
    "state": 1,
    "networkId": 1,
    "endDate": 1,
    "startDate": 1
}, {"name": "omni_offer_idx", "background": true})
omnimfd.getCollection('omniOffer').createIndex({"networkId": 1}, {"name": "networkId", "background": true})
omnimfd.getCollection('omniOffer').createIndex({"state": 1}, {"name": "state", "background": true})
omnimfd.getCollection('omniOffer').createIndex({"startDate": 1}, {"name": "startDate", "background": true})
omnimfd.getCollection('omniOffer').createIndex({"endDate": 1}, {"name": "endDate", "background": true})


//

function printTextAndJson(text, object) {
    print(text + JSON.stringify(object, null, 2))
}

var omnimfdMongoDb = db.getSiblingDB('omnimfd')

function checkIndexes(collectionName, expectedIndexNames) {
    var indexes = omnimfdMongoDb[collectionName].getIndexes()

    for (index in indexes) {
        var indexName = indexes[index].name

        if (!expectedIndexNames.includes(indexes[index].name)) {
            print('WARN: Unknown index found at collection ' + collectionName + 'index name is ' + indexName)
        }

    }
}

function verifyCollection(collectionName, indexDefinitions) {
    print('Going to check collection ' + collectionName)

    var validIndexNames = []
    for (obj in indexDefinitions) {
        var indexName = indexDefinitions[obj][1].name
        validIndexNames.push(indexName)
    }

    checkIndexes(collectionName, validIndexNames)

    for (obj in indexDefinitions) {
        var keys = indexDefinitions[obj][0]
        var options = indexDefinitions[obj][1]
        var result = omnimfdMongoDb.getCollection(collectionName).createIndex(keys, options)

        if (result.ok === 1) {
            if (result.note === "all indexes already exist") {
                print('Same index ' + options.name + ' already exists.')
            }  else {
                print('New index ' + options.name + ' created.')
            }
        }  else {
            printTextAndJson(options.name + ' result = ' , result)
        }

    }

    print('Processing finished for collection ' + collectionName)
}

for (i in omnimfd.keys) {
    var collectionName = omnimfd.keys[i]
    verifyCollection(collectionName, omnimfd.collectionMap.get(collectionName).indexes)
}