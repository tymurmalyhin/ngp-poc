FROM java:7-jdk AS build

MAINTAINER tymur <oleksandr.druzenko@globallogic.com>
LABEL authors="oleksandr.druzenko@globallogic.com"

ENV KAFKA_VERSION kafka-0.7.2-incubating-src
ENV NEXUS_URL http://d2cits3ogc9htg.cloudfront.net/software
ENV KAFKA_HOME=/usr/local/kafka

WORKDIR /usr/local

RUN set -ex; \
    curl ${NEXUS_URL}/apache/kafka/${KAFKA_VERSION}.tgz | tar xvz \
    && mkdir -p /usr/local/${KAFKA_VERSION}/project/boot/scala-2.7.7

ADD org.scala-tools.sbt.tar.gz /usr/local/${KAFKA_VERSION}/project/boot/scala-2.7.7/

RUN set -ex; \
    cd /usr/local/${KAFKA_VERSION} \
    && /usr/local/${KAFKA_VERSION}/sbt update \
    && /usr/local/${KAFKA_VERSION}/sbt package \
    && ln -s /usr/local/${KAFKA_VERSION} ${KAFKA_HOME}

EXPOSE 9094 9092

COPY config/* ${KAFKA_HOME}/config/

COPY bootstrap.sh /etc/bootstrap.sh
RUN chmod a+x /etc/bootstrap.sh

WORKDIR $KAFKA_HOME

CMD ["/etc/bootstrap.sh"]
