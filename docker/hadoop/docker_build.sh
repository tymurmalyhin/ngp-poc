
docker build --tag catalina/hadoop-base:3.1.1 ./base
docker build --tag catalina/hadoop-namenode:3.1.1 ./namenode
docker build --tag catalina/hadoop-datanode:3.1.1 ./datanode
docker build --tag catalina/hadoop-resourcemanager:3.1.1 ./resourcemanager
docker build --tag catalina/hadoop-nodemanager:3.1.1 ./nodemanager
docker build --tag catalina/hadoop-historyserver:3.1.1 ./historyserver


# rem call docker build -t bde2020/hadoop-submit:$(current_branch) ./submit
