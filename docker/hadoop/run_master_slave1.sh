docker service create \
 --name hadoop-master \
 --hostname hadoop-master \
 --network nadoop \
 --replicas 1 \
 --detach=true \
 --endpoint-mode dnsrr \
  tymur/ubuntu_h:1

docker service create \
 --name hadoop-slave1 \
 --hostname hadoop-slave1 \
 --network nadoop \
 --replicas 1 \
 --detach=true \
 --endpoint-mode dnsrr \
  tymur/ubuntu_h:1
  
docker service create \
 --name hadoop-slave2 \
 --hostname hadoop-slave2 \
 --network nadoop \
 --replicas 1 \
 --detach=true \
 --endpoint-mode dnsrr \
  tymur/ubuntu_h:1  
  
docker service create \
 --name hadoop-slave3 \
 --hostname hadoop-slave3 \
 --network nadoop \
 --replicas 1 \
 --detach=true \
 --endpoint-mode dnsrr \
  tymur/ubuntu_h:1  