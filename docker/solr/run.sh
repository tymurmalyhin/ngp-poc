
sudo docker run -d --rm --network=hadoop_hadoop-net --ip=172.16.238.50 -p 8983:8983 --name solr \
     --add-host="zookepper:172.16.238.30" \
     catalina/solr:8.1.1
