
sudo docker run --rm -d --network=hadoop_hadoop-net --ip=172.16.238.40 --name atlas -p 21000:21000 \
     --add-host="solr:172.16.238.20" --add-host="zookepper:172.16.238.30" --add-host="hbase:172.16.238.20"\
     catalina/atlas:2.0.0

