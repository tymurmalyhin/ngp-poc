
#rem docker run --rm -d --name zookeeper wurstmeister/zookeeper:lates

# the instance for kafka 0.7.2 and Hbase and atlas
docker run --rm -d --network=hadoop_hadoop-net --ip=172.16.238.30  --name zookeeper -p 2181:2181 catalina/zookeeper:3.4.14

# the instance for kafka 2.1.1
docker run --rm -d --network=hadoop_hadoop-net --ip=172.16.238.31  --name zookeeper_211 -p 12181:2181 catalina/zookeeper:3.4.14


