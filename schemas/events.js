{
	"type": "record",
	"name": "Event",
	"namespace": "com.catalina.platform.avro",
	"fields": [{
			"name": "cid",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The id of the consumer this event is correlated with",
			"default": null
		}, {
			"name": "device_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "This anonymously identifies a particular user, device, or browser instance. For the web, this is generally stored as a first-party cookie with a two-year expiration. For mobile apps, this is randomly generated for each particular instance of an application install. The value of this field should be a random UUID (version 4) as described in http://www.ietf.org/rfc/rfc4122.txt",
			"default": null
		}, {
			"name": "time",
			"type": ["null", "long"],
			"doc": "The time (UTC) in milliseconds since epoch. In Java new Date().getTime()",
			"default": null
		}, {
			"name": "source_ids",
			"type": ["null", {
					"type": "array",
					"items": {
						"type": "string",
						"avro.java.string": "String"
					}
				}
			],
			"doc": "The unique advertisement identifiers correlated with this event (offer, ad, mclu, etc)",
			"default": null
		}, {
			"name": "type",
			"type": {
				"type": "enum",
				"name": "EventType",
				"doc": "The type of event",
				"symbols": ["redemption", "impression", "click", "checkout", "media_delivery", "visit", "control", "system", "conversion", "non_conversion", "pin", "threshold", "pageview", "screenview", "item", "social", "exception", "print", "cart", "wallet", "goal", "detection", "notifications", "download", "clip"]
			},
			"doc": "The type of event that was triggered from redemption of a coupon, to an impression, to a generic tracking event",
			"default": "system"
		}, {
			"name": "system_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The unique system identifier that was responsible for generating this event",
			"default": null
		}, {
			"name": "media_type",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The type of media delivered or correlated with this particular event, for example 'ad' or 'coupon'",
			"default": null
		}, {
			"name": "model",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The type of scoring model or distribution model correlated to this particular event",
			"default": null
		}, {
			"name": "channel_type",
			"type": {
				"type": "enum",
				"name": "ChannelType",
				"symbols": ["mobile", "store", "web", "mobile_web", "tv", "nltv", "video", "email", "app", "kiosk", "direct", "other"]
			},
			"doc": "The channel which is correlated with this event",
			"default": "other"
		}, {
			"name": "session_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The unique session identifier used to correlate multiple events together as part of an overall click-stream, web-session, or transaction. Includes attribution and conversion modeling where impression can be delivered days ago and conversion is associated to the original session",
			"default": null
		}, {
			"name": "site_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The unique identifier for where this event was triggered (for store this is the store #, for websites, the affiliate or web property id)",
			"default": null
		}, {
			"name": "network_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The unique identifier for where this event was triggered (for store this is chain #, for websites, the network or affiliate id)",
			"default": null
		}, {
			"name": "lane_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The unique identifier for the lane which generated this event, only applicable if a store event",
			"default": null
		}, {
			"name": "latitude",
			"type": ["null", "float"],
			"doc": "The latitude where the user was when this event was generated in the format: 27.9710,82.4650",
			"default": null
		}, {
			"name": "longitude",
			"type": ["null", "float"],
			"doc": "The latitude where the user was when this event was generated in the format: 27.9710,82.4650",
			"default": null
		}, {
			"name": "shopper_type",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The type of shopper and/or classification of the shopper at the time the event was triggered (for example; 'new', 'abusive', 'control')",
			"default": null
		}, {
			"name": "campaign_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "A campaign id associate to the media distributed",
			"default": null
		}, {
			"name": "campaign_name",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Specifies the campaign name. Example (direct)",
			"default": null
		}, {
			"name": "campaign_keyword",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Specifies the campaign keyword.",
			"default": null
		}, {
			"name": "advertiser_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The advertiser that was associate to this event due to its media being generated, this could be retailer if retailer provided content or a manufacturer # given a manufacturer program",
			"default": null
		}, {
			"name": "ad_group_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Optional ad group identifier for the collection of media shown to a user (aka: award)",
			"default": null
		}, {
			"name": "ad_creative_set_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The ID of the creative set used when displaying to the user a collection of ids (aka: mclu)",
			"default": null
		}, {
			"name": "conversion_total_purchased_amount",
			"type": ["null", "double"],
			"doc": "A number representing the value that placed on the conversion. It could reflect a currency value, quantity, weighted value, or dollar value",
			"default": null,
			"aliases": ["purchased_amount"]
		}, {
			"name": "conversion_total_purchased_qty",
			"type": ["null", "int"],
			"doc": "A number representing the overall conversion quantity (units sold based on conversion)",
			"default": null,
			"aliases": ["purchased_qty"]
		}, {
			"name": "conversion_purchased_upcs",
			"type": ["null", {
					"type": "array",
					"items": {
						"type": "string",
						"avro.java.string": "String"
					}
				}
			],
			"doc": "The upcs associated with the conversion event and attributed to the conversion quantity and dollar value",
			"default": null,
			"aliases": ["purchased_upcs"]
		}, {
			"name": "event_value",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Specifies the event value. Values must be non-negative.",
			"default": null
		}, {
			"name": "event_label",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Specifies the event label.",
			"default": null
		}, {
			"name": "event_action",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Specifies the event action. Must not be empty.",
			"default": null
		}, {
			"name": "event_category",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Specifies the event category. Must not be empty.",
			"default": null
		}, {
			"name": "exception_fatal",
			"type": ["null", "boolean"],
			"doc": "Specifies whether the exception was fatal.",
			"default": null
		}, {
			"name": "ttl",
			"type": ["null", "int"],
			"doc": "TTL length in seconds, will automatically delete rows once the expiration time is reached.",
			"default": null,
			"order": "ignore"
		}, {
			"name": "social_network",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Specifies the social network, for example Facebook or Google Plus.",
			"default": null
		}, {
			"name": "social_action",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Specifies the social interaction action. For example on Google Plus when a user clicks the +1 button, the social action is 'plus'.",
			"default": null
		}, {
			"name": "social_action_target",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Specifies the target of a social interaction. This value is typically a URL but can be any text.",
			"default": null
		}, {
			"name": "doc_host",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Specifies the hostname from which content was hosted.",
			"default": null
		}, {
			"name": "doc_path",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The path portion of the page URL. Should begin with '/'.",
			"default": null
		}, {
			"name": "doc_title",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The title of the page / document.",
			"default": null
		}, {
			"name": "doc_referrer",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Specifies which referral source brought traffic to a website. This value is also used to compute the traffic source. The format of this value is a URL.",
			"default": null
		}, {
			"name": "mobile_app_name",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Specifies the application name",
			"default": null
		}, {
			"name": "mobile_app_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Application identifier.",
			"default": null
		}, {
			"name": "mobile_app_version",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "Specifies the application version.",
			"default": null
		}, {
			"name": "category_level_1",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The 1st level in a category taxonomy. For example in the taxonomy GROCERY > PET FOOD > CAT FOOD > DRY CAT FOOD this would be 'GROCERY'",
			"default": null,
			"ordering": "ignore"
		}, {
			"name": "category_level_2",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The 2nd level in a category taxonomy. For example in the taxonomy GROCERY > PET FOOD > CAT FOOD > DRY CAT FOOD this would be 'PET FOOD'",
			"default": null,
			"ordering": "ignore"
		}, {
			"name": "category_level_3",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The 3rd level in a category taxonomy. For example in the taxonomy GROCERY > PET FOOD > CAT FOOD > DRY CAT FOOD this would be 'CAT FOOD'",
			"default": null,
			"ordering": "ignore"
		}, {
			"name": "category_level_4",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The 4th level in a category taxonomy. For example in the taxonomy GROCERY > PET FOOD > CAT FOOD > DRY CAT FOOD this would be 'DRY CAT FOOD'",
			"default": null,
			"ordering": "ignore"
		}, {
			"name": "user_ip",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The IP address of the user (if applicable to channel) used to inject lat/long if not provided through direct interaction (GPS/Phone vs. Browser)",
			"default": null
		}, {
			"name": "id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The unique ID of this event for correlation and duplicate detection",
			"default": null,
			"order": "ignore"
		}, {
			"name": "country_code",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The country code where this event is generated against",
			"default": null
		}, {
			"name": "custom_attributes",
			"type": ["null", {
					"type": "map",
					"values": {
						"type": "string",
						"avro.java.string": "String"
					},
					"avro.java.string": "String"
				}
			],
			"doc": "Custom dimensions or properties for use with reporting or analytics",
			"default": null,
			"order": "ignore"
		}, {
			"name": "user_traits",
			"type": ["null", {
					"type": "array",
					"items": {
						"type": "string",
						"avro.java.string": "String"
					}
				}
			],
			"doc": "The traits for the specified user, which allow us to track user engagement by various dynamic classifications of the consumer throughout time. For example heavy, medium, loyal, abandoner, new visitor, etc",
			"default": null,
			"order": "ignore"
		}, {
			"name": "location_admin_zone_0",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The location administrative level 0 zone that this event was triggered in (see: diva-gis). This is always country level. This differs from country code in that country code is the country that owns the network, not where the user is engaging (for example network is owned by USA but touchpoint happens in Japan)",
			"default": null,
			"order": "ignore"
		}, {
			"name": "location_admin_zone_1",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The location administrative level 1 zone that this event was triggered in (see: diva-gis). For USA this is the state. Other countries like Japan may use different admin taxonomies. For example ?(KEN)/PREFRECTURE for level 1.",
			"default": null,
			"order": "ignore"
		}, {
			"name": "location_admin_zone_2",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The location administrative level 2 zone that this event was triggered in (see: diva-gis). For USA this is county. Other countries like  Japan may use different admin taxonomies. For Japan this might be municipality ?(SHI)/CITY for level 2 depending on the size of the municipality.",
			"default": null,
			"order": "ignore"
		}, {
			"name": "user_agent",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The browser/user agent used by the user to access (in digital/mobile) channels",
			"default": null,
			"order": "ignore"
		}, {
			"name": "user_language",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The language the user had their device set to when accessing the content",
			"default": null,
			"order": "ignore"
		}, {
			"name": "category_level_1_code",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The code for the category level 1, which is, if available the identifier for the category level 1 text. Most systems should ignore this value.",
			"default": null,
			"order": "ignore"
		}, {
			"name": "category_level_2_code",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The code for the category level 2, which is, if available the identifier for the category level 2 text. Most systems should ignore this value.",
			"default": null,
			"order": "ignore"
		}, {
			"name": "threshold_balance",
			"type": ["null", "double"],
			"doc": "The last balance of the user when dealing with loyalty thresholds.",
			"default": null,
			"order": "ignore"
		}, {
			"name": "threshold_sequence",
			"type": ["null", "int"],
			"doc": "The last sequence of the user when dealing with loyalty thresholds",
			"default": null,
			"order": "ignore"
		}, {
			"name": "threshold_reset",
			"type": ["null", "boolean"],
			"doc": "A flag indicating if the threshold should be reset.",
			"default": null,
			"order": "ignore"
		}, {
			"name": "gen",
			"type": ["null", "long"],
			"doc": "The time (UTC) in milliseconds since epoch that the event was generated. This differs from ts in that it contains the timestamp when the event was received and is used for data extraction and analytics. In Java new Date().getTime()",
			"default": null
		}, {
			"name": "client_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The client ID that sponsored this advertisement",
			"default": null,
			"order": "ignore"
		}, {
			"name": "placement_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The placement that hosted this advertisement, if any applies",
			"default": null,
			"order": "ignore"
		}, {
			"name": "bid_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The identifier for the bid that won this space",
			"default": null,
			"order": "ignore"
		}, {
			"name": "exchange_id",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The identifier of the exchange that this advertisement was bid within",
			"default": null,
			"order": "ignore"
		}, {
			"name": "bid_amount",
			"type": ["null", "double"],
			"doc": "The cost/price of the bid that resulted in the win.",
			"default": null,
			"order": "ignore"
		}, {
			"name": "bid_currency",
			"type": ["null", {
					"type": "string",
					"avro.java.string": "String"
				}
			],
			"doc": "The currency of the price, assumed to be what is the country's currency code if not specified",
			"default": null,
			"order": "ignore"
		}, {
			"name": "ads",
			"type": ["null", {
					"type": "array",
					"items": {
						"type": "record",
						"name": "AdMetaData",
						"doc": "* Advertising metadata used for tracking and analytic as well as billing and general capturing.",
						"fields": [{
								"name": "id",
								"type": ["null", {
										"type": "string",
										"avro.java.string": "String"
									}
								],
								"doc": "The unique ID for this advertisement, for example `USA-BLIP-1234`.",
								"default": null
							}, {
								"name": "template_id",
								"type": ["null", {
										"type": "string",
										"avro.java.string": "String"
									}
								],
								"doc": "The template ID that was used when rendering this advertisement. If any was used.",
								"default": null
							}, {
								"name": "adunit_id",
								"type": ["null", {
										"type": "string",
										"avro.java.string": "String"
									}
								],
								"doc": "The ad unit that was used when rendering this advertisement. If any was used.",
								"default": null
							}, {
								"name": "adunit_type",
								"type": ["null", {
										"type": "string",
										"avro.java.string": "String"
									}
								],
								"doc": "The ad unit that was used when rendering this advertisement. If any was used.",
								"default": null
							}, {
								"name": "height",
								"type": ["null", "int"],
								"doc": "The base height of the advertisement. If it was rendered.",
								"default": null
							}, {
								"name": "width",
								"type": ["null", "int"],
								"doc": "The base width of the advertisement. If it was rendered.",
								"default": null
							}, {
								"name": "url",
								"type": ["null", {
										"type": "string",
										"avro.java.string": "String"
									}
								],
								"doc": "* The URL that can be used to preview the advertisement that was rendered. If it was rendered.",
								"default": null
							}, {
								"name": "variables",
								"type": ["null", {
										"type": "map",
										"values": {
											"type": "string",
											"avro.java.string": "String"
										},
										"avro.java.string": "String"
									}
								],
								"doc": "The variables associated to this ad, for example 'pin' or other custom variables for the advertisement.",
								"default": null
							}, {
								"name": "scores",
								"type": ["null", {
										"type": "map",
										"values": "double",
										"avro.java.string": "String"
									}
								],
								"doc": "The scores associated to this advertisement, if any are applicable.",
								"default": null
							}
						]
					}
				}
			],
			"default": null
		}
	]
}
